package mindslab.mms.service;

import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface SpeechRateService {

    List<SttMetaDTO> getSpeechRateList(SearchVO searchVO);

    int getSpeechRateCount(SearchVO searchVO);

    List<SttMetaDTO> getSpeechRateExcel(SearchVO searchVO);

}
