package mindslab.mms.service;

import mindslab.mms.models.dto.DetectedKeywordDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface DetectedKeywordService {

    List<DetectedKeywordDTO> getKeywordList(SearchVO searchVO);

    int getKeywordCount(SearchVO searchVO);

    List<DetectedKeywordDTO> getKeywordDetailList(SearchVO searchVO);

    int getKeywordDetailCount(SearchVO searchVO);

    List<DetectedKeywordDTO> getGraphData(SearchVO searchVO);

    List<DetectedKeywordDTO> getKeywordExcel(SearchVO searchVO);

}
