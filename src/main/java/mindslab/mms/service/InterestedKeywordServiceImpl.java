package mindslab.mms.service;

import mindslab.mms.mapper.base.InterestedKeywordMapper;
import mindslab.mms.models.dto.DetectedKeywordDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InterestedKeywordServiceImpl implements InterestedKeywordService {

    @Autowired
    InterestedKeywordMapper interestedKeywordMapper;

    @Override
    public List<DetectedKeywordDTO> getInterestedKeywordList(SearchVO searchVO) {
        return interestedKeywordMapper.getInterestedKeywordList(searchVO);
    }

    @Override
    public int getInterestedKeywordCount(SearchVO searchVO) {
        return interestedKeywordMapper.getInterestedKeywordCount(searchVO);
    }

    @Override
    public List<DetectedKeywordDTO> getInterestedKeywordExcel(SearchVO searchVO) {
        return interestedKeywordMapper.getInterestedKeywordExcel(searchVO);
    }
}
