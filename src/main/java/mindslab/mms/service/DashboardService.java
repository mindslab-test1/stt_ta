package mindslab.mms.service;

import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface DashboardService {

    List<SttMetaDTO> getDashboardList(SearchVO searchVO);

    int getDashboardCount(SearchVO searchVO);
}
