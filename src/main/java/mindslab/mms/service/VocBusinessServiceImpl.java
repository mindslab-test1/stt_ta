package mindslab.mms.service;

import mindslab.mms.mapper.base.VocBusinessMapper;
import mindslab.mms.models.dto.VocBusinessDTO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.models.vo.VocBusinessVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class VocBusinessServiceImpl implements VocBusinessService{

    @Autowired
    VocBusinessMapper vocBusinessMapper;

    @Override
    public List<VocBusinessDTO> getVocBusinessList(SearchVO searchVO) {
        return vocBusinessMapper.getVocBusinessList(searchVO);
    }

    @Override
    public int getVocBusinessCount(SearchVO searchVO) {
        return vocBusinessMapper.getVocBusinessCount(searchVO);
    }

    @Override
    public boolean addVocBusiness(VocBusinessVO vocBusinessVO) {
        return vocBusinessMapper.addVocBusiness(vocBusinessVO);
    }

    @Override
    public boolean deleteVocBusiness(VocBusinessVO vocBusinessVO) {
        return vocBusinessMapper.deleteVocBusiness(vocBusinessVO);
    }

    @Override
    public boolean updateVocBusiness(VocBusinessVO vocBusinessVO) {
        return vocBusinessMapper.updateVocBusiness(vocBusinessVO);
    }

    @Override
    public int checkDupVocBusiness(VocBusinessVO vocBusinessVO) {
        return vocBusinessMapper.checkDupVocBusiness(vocBusinessVO);
    }
}
