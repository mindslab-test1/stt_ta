package mindslab.mms.service;

import mindslab.mms.mapper.base.AccountMapper;
import mindslab.mms.models.dto.AccountDTO;
import mindslab.mms.models.dto.AuthorityDTO;
import mindslab.mms.models.vo.AccountVO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    AccountMapper accountMapper;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        AccountDTO account = accountMapper.getAccount(userId);
        account.setAuthorities(getAuthorities(userId));

        UserDetails userDetails = new UserDetails() {

            @Override
            public boolean isEnabled() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public String getUsername() {
                return account.getUsername();
            }

            @Override
            public String getPassword() {
                return account.getPassword();
            }

            @Override
            public Collection getAuthorities() {
                return account.getAuthorities();
            }
        };
        return account;
    }

    public Collection<GrantedAuthority> getAuthorities(String username) {
        List<String> string_authorities = accountMapper.getAuthorities(username);
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String authority : string_authorities) {
            authorities.add(new SimpleGrantedAuthority(authority));
        }
        return authorities;
    }

    public AccountDTO save(AccountDTO account, String role) {
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setAccountNonExpired(true);
        account.setAccountNonLocked(true);
        account.setCredentialsNonExpired(true);
        account.setEnabled(true);

        AuthorityDTO authority = new AuthorityDTO();
        authority.setUsername(account.getUsername());
        authority.setAuthority(role);

        accountMapper.insertUser(account);
        accountMapper.insertUserAuthority(authority);
        return account;
    }

    public AccountDTO getAccount(String userId) {
        return accountMapper.getAccount(userId);
    }

    @Override
    public List<AccountDTO> getUserList(SearchVO searchVO) {
        return accountMapper.getUserList(searchVO);
    }

    @Override
    public int getUserCount(SearchVO searchVO) {
        return accountMapper.getUserCount(searchVO);
    }

    @Transactional
    public boolean addUser(AccountVO accountVO) {
        accountVO.setPassword(passwordEncoder.encode(accountVO.getPassword()));
        accountVO.setAccountNonExpired(true);
        accountVO.setAccountNonLocked(true);
        accountVO.setCredentialsNonExpired(true);
        accountVO.setEnabled(true);

        AuthorityDTO authority = new AuthorityDTO();
        authority.setUsername(accountVO.getUsername());

        // 중기청에서 새로생성되는 유저는 단일권한을 갖는다.
        authority.setAuthority("ROLE_USER");

        try {
            accountMapper.addUser(accountVO);
            accountMapper.insertUserAuthority(authority);
        } catch(RuntimeException ex) {
            return false;
        }
        return true;
    }
    public boolean updateUser(AccountVO accountVO) {
        accountVO.setPassword(passwordEncoder.encode(accountVO.getPassword()));
        return accountMapper.updateUser(accountVO);
    }

    @Override
    public int checkDupUser(AccountVO accountVO) {
        return accountMapper.checkDupUser(accountVO);
    }

    @Transactional
    public boolean deleteUser(AccountVO accountVO) {
        List<String> usernameList = accountMapper.getAuthorityList(accountVO);
        accountVO.setUsernameList(usernameList);
        accountMapper.deleteUser(accountVO);
        return accountMapper.deleteAuthorityList(accountVO);
    }

}
