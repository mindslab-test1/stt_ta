package mindslab.mms.service;

import mindslab.mms.models.dto.KeywordDTO;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.dto.SttResultDTO;
import mindslab.mms.models.vo.KeywordVO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface ModalService {

    List<SttResultDTO> getSttResult(SearchVO searchVO);

    SttMetaDTO getSttMetaData(String recMetaSer);

    List<KeywordDTO> getKeywordManagementList();

    boolean addKeyword(KeywordVO keywordVO);

    boolean deleteKeyword(KeywordVO keywordVO);

    int checkDuplicateKeyword(KeywordVO keywordVO);

    int getLongcallConfig();

    boolean updateLongcallConfig(int second);
}
