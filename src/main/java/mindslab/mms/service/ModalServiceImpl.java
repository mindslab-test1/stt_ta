package mindslab.mms.service;

import mindslab.mms.mapper.base.ModalMapper;
import mindslab.mms.models.dto.KeywordDTO;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.dto.SttResultDTO;
import mindslab.mms.models.vo.KeywordVO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ModalServiceImpl implements ModalService {

    @Autowired
    ModalMapper modalMapper;

    @Override
    public List<SttResultDTO> getSttResult(SearchVO searchVO) {
        return modalMapper.getSttResult(searchVO);
    }

    @Override
    public SttMetaDTO getSttMetaData(String recMetaSer) {
        return modalMapper.getSttMetaData(recMetaSer);
    }

    @Override
    public List<KeywordDTO> getKeywordManagementList() {
        return modalMapper.getKeywordManagementList();
    }

    @Override
    public boolean addKeyword(KeywordVO keywordVO) {
        return modalMapper.addKeyword(keywordVO);
    }

    @Override
    public boolean deleteKeyword(KeywordVO keywordVO) {
        return modalMapper.deleteKeyword(keywordVO);
    }

    @Override
    public int checkDuplicateKeyword(KeywordVO keywordVO) {
        return modalMapper.checkDuplicateKeyword(keywordVO);
    }

    @Override
    public int getLongcallConfig() {
        return modalMapper.getLongcallConfig();
    }

    @Override
    public boolean updateLongcallConfig(int second) {
        return modalMapper.updateLongcallConfig(second);
    }
}
