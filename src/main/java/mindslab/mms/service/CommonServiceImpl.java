package mindslab.mms.service;
import mindslab.mms.mapper.aws.AwsMapper;
import mindslab.mms.mapper.base.CommonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonServiceImpl implements CommonService{
    @Autowired
    CommonMapper commonMapper;

    @Autowired
    AwsMapper awsMapper;

    @Override
    public String getMp3FilePath(String recMetaSer) {
        return commonMapper.getMp3FilePath(recMetaSer);
    }

    @Override
    public List<String> getTeamList() {
        return commonMapper.getTeamList();
    }

    @Override
    public List<String> getOracleTeamList() {
        return awsMapper.getOracleTeamList();
    }
}
