package mindslab.mms.service;

import mindslab.mms.mapper.base.SpeechRateMapper;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SpeechRateServiceImpl implements SpeechRateService {

    @Autowired
    SpeechRateMapper speechRateMapper;

    @Override
    public List<SttMetaDTO> getSpeechRateList(SearchVO searchVO) {
        return speechRateMapper.getSpeechRateList(searchVO);
    }

    @Override
    public int getSpeechRateCount(SearchVO searchVO) {
        return speechRateMapper.getSpeechRateCount(searchVO);
    }

    @Override
    public List<SttMetaDTO> getSpeechRateExcel(SearchVO searchVO) {
        return speechRateMapper.getSpeechRateExcel(searchVO);
    }
}
