package mindslab.mms.service;

import mindslab.mms.mapper.aws.AwsMapper;
import mindslab.mms.models.dto.OracleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AwsServiceImpl implements AwsService {

	@Autowired
	AwsMapper awsMapper;

	public int awsTest() {
		return awsMapper.awsTest();
	}

	@Override
	public List<OracleDTO> getOracleCounselData() {
		return awsMapper.getOracleCounselData();
	}
}
