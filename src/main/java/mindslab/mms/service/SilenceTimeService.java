package mindslab.mms.service;

import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface SilenceTimeService {

    List<SttMetaDTO> getSilenceTimeList(SearchVO searchVO);

    int getSilenceTimeCount(SearchVO searchVO);

    List<SttMetaDTO> getSilenceTimeExcel(SearchVO searchVO);

}
