package mindslab.mms.service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommonService {
    String getMp3FilePath(String recMetaSer);

    List<String> getTeamList();

    List<String> getOracleTeamList();
}
