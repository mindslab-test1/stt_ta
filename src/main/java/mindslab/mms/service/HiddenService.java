package mindslab.mms.service;

import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;


@Service
public interface HiddenService {

    List<HashMap> hiddenSelect(SearchVO searchVO);

    int hiddenInsert(SearchVO searchVO);

    int hiddenUpdate(SearchVO searchVO);

    int hiddenDelete(SearchVO searchVO);

    List<SttMetaDTO> getVOCData(SearchVO searchVO);

    int addMissingData(SearchVO searchVO);
}
