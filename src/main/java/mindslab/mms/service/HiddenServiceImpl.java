package mindslab.mms.service;

import mindslab.mms.mapper.base.HiddenMapper;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;


@Service
public class HiddenServiceImpl implements HiddenService {

    @Autowired
    HiddenMapper hiddenMapper;

    public List<HashMap> hiddenSelect(SearchVO searchVO) {
        return hiddenMapper.hiddenSelect(searchVO);
    }

    public int hiddenInsert(SearchVO searchVO) {
        return hiddenMapper.hiddenInsert(searchVO);
    }

    public int hiddenUpdate(SearchVO searchVO) {
        return hiddenMapper.hiddenUpdate(searchVO);
    }

    public int hiddenDelete(SearchVO searchVO) {
        return hiddenMapper.hiddenDelete(searchVO);
    }

    @Override
    public List<SttMetaDTO> getVOCData(SearchVO searchVO) {
        return hiddenMapper.getVOCData(searchVO);
    }

    @Override
    public int addMissingData(SearchVO searchVO) {
        return hiddenMapper.addMissingData(searchVO);
    }
}
