package mindslab.mms.service;

import mindslab.mms.models.dto.AccountDTO;
import mindslab.mms.models.vo.AccountVO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;


@Service
public interface AccountService extends UserDetailsService {

    @Override
    UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException;

    Collection<GrantedAuthority> getAuthorities(String username);

    AccountDTO save(AccountDTO account, String role);

    AccountDTO getAccount(String userId);

    List<AccountDTO> getUserList(SearchVO searchVO);

    int getUserCount(SearchVO searchVO);

    boolean addUser(AccountVO accountVO);

    boolean updateUser(AccountVO accountVO);

    int checkDupUser(AccountVO accountVO);

    boolean deleteUser(AccountVO accountVO);
}
