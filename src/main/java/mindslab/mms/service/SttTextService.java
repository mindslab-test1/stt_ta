package mindslab.mms.service;

import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface SttTextService {

    List<SttMetaDTO> getSttTextList(SearchVO searchVO);

    int getSttTextCount(SearchVO searchVO);

    List<SttMetaDTO> getSttTextExcel(SearchVO searchVO);
}
