package mindslab.mms.service;

import mindslab.mms.mapper.base.SilenceTimeMapper;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SilenceTimeServiceImpl implements SilenceTimeService {

    @Autowired
    SilenceTimeMapper silenceTimeMapper;

    @Override
    public List<SttMetaDTO> getSilenceTimeList(SearchVO searchVO) {
        return silenceTimeMapper.getSilenceTimeList(searchVO);
    }

    @Override
    public int getSilenceTimeCount(SearchVO searchVO) {
        return silenceTimeMapper.getSilenceTimeCount(searchVO);
    }

    @Override
    public List<SttMetaDTO> getSilenceTimeExcel(SearchVO searchVO) {
        return silenceTimeMapper.getSilenceTimeExcel(searchVO);
    }
}
