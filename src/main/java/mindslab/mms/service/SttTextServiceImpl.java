package mindslab.mms.service;

import mindslab.mms.mapper.base.SttTextMapper;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SttTextServiceImpl implements SttTextService {

    @Autowired
    SttTextMapper sttTextMapper;

    @Override
    public List<SttMetaDTO> getSttTextList(SearchVO searchVO) {
        return sttTextMapper.getSttTextList(searchVO);
    }

    @Override
    public int getSttTextCount(SearchVO searchVO) {
        return sttTextMapper.getSttTextCount(searchVO);
    }

    @Override
    public List<SttMetaDTO> getSttTextExcel(SearchVO searchVO) {
        return sttTextMapper.getSttTextExcel(searchVO);
    }
}
