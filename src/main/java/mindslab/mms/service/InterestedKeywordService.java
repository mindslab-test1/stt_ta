package mindslab.mms.service;

import mindslab.mms.models.dto.DetectedKeywordDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface InterestedKeywordService {

    List<DetectedKeywordDTO> getInterestedKeywordList(SearchVO searchVO);

    int getInterestedKeywordCount(SearchVO searchVO);

    List<DetectedKeywordDTO> getInterestedKeywordExcel(SearchVO searchVO);

}
