package mindslab.mms.service;

import mindslab.mms.mapper.base.AgentManagementMapper;
import mindslab.mms.models.dto.AgentDTO;
import mindslab.mms.models.vo.AgentVO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AgentManagementServiceImpl implements AgentManagementService{

    @Autowired
    AgentManagementMapper agentManagementMapper;

    @Override
    public List<AgentDTO> getAgentList(SearchVO searchVO) {
        return agentManagementMapper.getAgentList(searchVO);
    }

    @Override
    public int getAgentCount(SearchVO searchVO) {
        return agentManagementMapper.getAgentCount(searchVO);
    }

    @Override
    public boolean addAgent(AgentVO agentVO) {
        return agentManagementMapper.addAgent(agentVO);
    }

    @Override
    public boolean deleteAgent(AgentVO agentVO) {
        return agentManagementMapper.deleteAgent(agentVO);
    }

    @Override
    public boolean updateAgent(AgentVO agentVO) {
        return agentManagementMapper.updateAgent(agentVO);
    }

    @Override
    public int checkDupAgent(AgentVO agentVO) {
        return agentManagementMapper.checkDupAgent(agentVO);
    }
}
