package mindslab.mms.service;

import mindslab.mms.mapper.base.DashboardMapper;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DashboardServiceImpl implements DashboardService {

    @Autowired
    DashboardMapper dashboardMapper;

    @Override
    public List<SttMetaDTO> getDashboardList(SearchVO searchVO) {
        return dashboardMapper.getDashboardList(searchVO);
    }

    @Override
    public int getDashboardCount(SearchVO searchVO) {
        return dashboardMapper.getDashboardCount(searchVO);
    }
}
