package mindslab.mms.service;

import mindslab.mms.models.dto.AgentDTO;
import mindslab.mms.models.vo.AgentVO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface AgentManagementService{

    List<AgentDTO> getAgentList(SearchVO searchVO);

    int getAgentCount(SearchVO searchVO);

    boolean addAgent(AgentVO agentVO);

    boolean deleteAgent(AgentVO agentVO);

    boolean updateAgent(AgentVO agentVO);

    int checkDupAgent(AgentVO agentVO);
}
