package mindslab.mms.service;

import mindslab.mms.models.dto.OracleDTO;

import java.util.List;

public interface AwsService {

	int awsTest();

	List<OracleDTO> getOracleCounselData();
}
