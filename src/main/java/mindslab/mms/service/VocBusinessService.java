package mindslab.mms.service;

import mindslab.mms.models.dto.VocBusinessDTO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.models.vo.VocBusinessVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface VocBusinessService {

    List<VocBusinessDTO> getVocBusinessList(SearchVO searchVO);

    int getVocBusinessCount(SearchVO searchVO);

    boolean addVocBusiness(VocBusinessVO vocBusinessVO);

    boolean deleteVocBusiness(VocBusinessVO vocBusinessVO);

    boolean updateVocBusiness(VocBusinessVO vocBusinessVO);

    int checkDupVocBusiness(VocBusinessVO vocBusinessVO);
}
