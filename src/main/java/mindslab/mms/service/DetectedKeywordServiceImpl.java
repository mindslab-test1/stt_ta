package mindslab.mms.service;

import mindslab.mms.mapper.base.DetectedKeywordMapper;
import mindslab.mms.models.dto.DetectedKeywordDTO;
import mindslab.mms.models.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DetectedKeywordServiceImpl implements DetectedKeywordService {

    @Autowired
    DetectedKeywordMapper detectedKeywordMapper;

    @Override
    public List<DetectedKeywordDTO> getKeywordList(SearchVO searchVO) {
        return detectedKeywordMapper.getKeywordList(searchVO);
    }

    @Override
    public int getKeywordCount(SearchVO searchVO) {
        return detectedKeywordMapper.getKeywordCount(searchVO);
    }

    @Override
    public List<DetectedKeywordDTO> getKeywordDetailList(SearchVO searchVO) {
        return detectedKeywordMapper.getKeywordDetailList(searchVO);
    }

    @Override
    public int getKeywordDetailCount(SearchVO searchVO) {
        return detectedKeywordMapper.getKeywordDetailCount(searchVO);
    }

    @Override
    public List<DetectedKeywordDTO> getGraphData(SearchVO searchVO) {
        return detectedKeywordMapper.getGraphData(searchVO);
    }

    @Override
    public List<DetectedKeywordDTO> getKeywordExcel(SearchVO searchVO) {
        return detectedKeywordMapper.getKeywordExcel(searchVO);
    }
}
