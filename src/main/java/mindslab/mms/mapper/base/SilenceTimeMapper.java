package mindslab.mms.mapper.base;

import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SilenceTimeMapper {
    List<SttMetaDTO> getSilenceTimeList(SearchVO searchVO);

    int getSilenceTimeCount(SearchVO searchVO);

    List<SttMetaDTO> getSilenceTimeExcel(SearchVO searchVO);

}
