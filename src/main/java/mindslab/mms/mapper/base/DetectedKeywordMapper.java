package mindslab.mms.mapper.base;

import mindslab.mms.models.dto.DetectedKeywordDTO;
import mindslab.mms.models.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface DetectedKeywordMapper {
    List<DetectedKeywordDTO> getKeywordList(SearchVO searchVO);

    int getKeywordCount(SearchVO searchVO);

    List<DetectedKeywordDTO> getKeywordDetailList(SearchVO searchVO);

    int getKeywordDetailCount(SearchVO searchVO);

    List<DetectedKeywordDTO> getGraphData(SearchVO searchVO);

    List<DetectedKeywordDTO> getKeywordExcel(SearchVO searchVO);
}
