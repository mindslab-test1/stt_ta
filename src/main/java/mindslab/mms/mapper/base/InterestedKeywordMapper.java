package mindslab.mms.mapper.base;

import mindslab.mms.models.dto.DetectedKeywordDTO;
import mindslab.mms.models.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface InterestedKeywordMapper {
    List<DetectedKeywordDTO> getInterestedKeywordList(SearchVO searchVO);

    int getInterestedKeywordCount(SearchVO searchVO);

    List<DetectedKeywordDTO> getInterestedKeywordExcel(SearchVO searchVO);

}
