package mindslab.mms.mapper.base;

import mindslab.mms.models.dto.KeywordDTO;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.dto.SttResultDTO;
import mindslab.mms.models.vo.KeywordVO;
import mindslab.mms.models.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.javassist.compiler.ast.Keyword;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ModalMapper {
    List<SttResultDTO> getSttResult(SearchVO searchVO);

    SttMetaDTO getSttMetaData(String recMetaSer);

    List<KeywordDTO> getKeywordManagementList();

    int checkDuplicateKeyword(KeywordVO keywordVO);

    boolean addKeyword(KeywordVO keywordVO);

    boolean deleteKeyword(KeywordVO keywordVO);

    int getLongcallConfig();

    boolean updateLongcallConfig(int second);
}
