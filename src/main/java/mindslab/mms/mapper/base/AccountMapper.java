package mindslab.mms.mapper.base;

import mindslab.mms.models.dto.AccountDTO;
import mindslab.mms.models.dto.AuthorityDTO;
import mindslab.mms.models.vo.AccountVO;
import mindslab.mms.models.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AccountMapper {
    AccountDTO getAccount(String username);

    List<AccountDTO> getUserList(SearchVO searchVO);

    int getUserCount(SearchVO searchVO);

    List<String> getAuthorities(String username);

    void insertUser(AccountDTO account);

    void insertUserAuthority(AuthorityDTO authority);

    List getAllAccounts();

    boolean addUser(AccountVO accountVO);

    boolean updateUser(AccountVO accountVO);

    int checkDupUser(AccountVO accountVO);

    boolean deleteUser(AccountVO accountVO);

    boolean deleteAuthorityList(AccountVO accountVO);

    List<String> getAuthorityList(AccountVO accountVO);
}
