package mindslab.mms.mapper.base;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CommonMapper {
    String getMp3FilePath(String recMetaSer);

    List<String> getTeamList();
}
