package mindslab.mms.mapper.base;

import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SpeechRateMapper {
    List<SttMetaDTO> getSpeechRateList(SearchVO searchVO);

    int getSpeechRateCount(SearchVO searchVO);

    List<SttMetaDTO> getSpeechRateExcel(SearchVO searchVO);

}
