package mindslab.mms.mapper.base;

import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SttTextMapper {
    List<SttMetaDTO> getSttTextList(SearchVO searchVO);

    int getSttTextCount(SearchVO searchVO);

    List<SttMetaDTO> getSttTextExcel(SearchVO searchVO);
}
