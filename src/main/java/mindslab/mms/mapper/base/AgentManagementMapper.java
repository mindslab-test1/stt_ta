package mindslab.mms.mapper.base;

import mindslab.mms.models.dto.AgentDTO;
import mindslab.mms.models.vo.AgentVO;
import mindslab.mms.models.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AgentManagementMapper {
    List<AgentDTO> getAgentList(SearchVO searchVO);

    int getAgentCount(SearchVO searchVO);

    boolean addAgent(AgentVO agentVO);

    boolean deleteAgent(AgentVO agentVO);

    boolean updateAgent(AgentVO agentVO);

    int checkDupAgent(AgentVO agentVO);
}
