package mindslab.mms.mapper.base;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Mapper
public interface HiddenMapper {

   List<HashMap> hiddenSelect(SearchVO searchVO);

   int hiddenInsert(SearchVO searchVO);

   int hiddenUpdate(SearchVO searchVO);

   int hiddenDelete(SearchVO searchVO);

   List<SttMetaDTO> getVOCData(SearchVO searchVO);

   int addMissingData(SearchVO searchVO);
}
