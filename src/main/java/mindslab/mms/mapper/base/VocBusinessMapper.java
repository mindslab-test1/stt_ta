package mindslab.mms.mapper.base;

import mindslab.mms.models.dto.VocBusinessDTO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.models.vo.VocBusinessVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface VocBusinessMapper {
    List<VocBusinessDTO> getVocBusinessList(SearchVO searchVO);

    int getVocBusinessCount(SearchVO searchVO);

    boolean addVocBusiness(VocBusinessVO vocBusinessVO);

    boolean deleteVocBusiness(VocBusinessVO vocBusinessVO);

    boolean updateVocBusiness(VocBusinessVO vocBusinessVO);

    int checkDupVocBusiness(VocBusinessVO vocBusinessVO);
}
