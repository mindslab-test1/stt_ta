package mindslab.mms.mapper.aws;

import mindslab.mms.models.dto.OracleDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AwsMapper {
    int awsTest();

    List<String> getOracleTeamList();

    List<OracleDTO> getOracleCounselData();
}
