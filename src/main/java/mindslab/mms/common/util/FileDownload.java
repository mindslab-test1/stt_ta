package mindslab.mms.common.util;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class FileDownload {

    public void fileDownload(String filePath, String filename, HttpServletResponse response){
        try {
            File f = new File(filePath+filename);
            if (!f.exists()) {
                System.out.println("error! file is not exist!"); // 존재하지 않는 파일 다운로드
                return;
            }
            filename = new String(filename.getBytes("euc-kr"), "8859_1");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment;filename="+ filename);

            // 파일 내용을 클라이언트에 전송
            byte[] b = new byte[1024];
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(f));
            OutputStream os = response.getOutputStream();

            int n;
            while ((n = bis.read(b, 0, b.length)) != -1) { // 파일을 모두 읽으면 -1리턴
                os.write(b, 0, n);
            }
            os.flush();
            os.close();
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

