package mindslab.mms.common;

import mindslab.mms.models.dto.AccountDTO;
import mindslab.mms.models.dto.AuthenticaionDTO;
import mindslab.mms.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("authProvider")
public class AuthProvider implements AuthenticationProvider {

    @Autowired
    private AccountService accountService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String id = authentication.getName();
        String password = authentication.getCredentials().toString();
        AccountDTO user = accountService.getAccount(id);

        if (null == user || !passwordEncoder.matches(password, user.getPassword())) {
            return null;
        }

        List<GrantedAuthority> grantedAuthorityList = new ArrayList(accountService.getAuthorities(id));

        return new AuthenticaionDTO(id, password, grantedAuthorityList, user);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
