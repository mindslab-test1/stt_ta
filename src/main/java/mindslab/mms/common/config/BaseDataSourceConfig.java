package mindslab.mms.common.config;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@MapperScan(value="mindslab.mms.mapper.base", sqlSessionFactoryRef="sqlSessionFactory")
public class BaseDataSourceConfig {
    @Autowired
    private Environment env;

    private final String MYBATIS_CONFIG = "classpath:mybatis/mybatis-config.xml";
    private final String MYBATIS_MAPPER = "classpath:mybatis/mapper/base/**/*.xml";

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.base.datasource")
    public DataSource mysql1DataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.base.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.base.datasource.url"));
        dataSource.setUsername(env.getProperty("spring.base.datasource.username"));
        dataSource.setPassword(env.getProperty("spring.base.datasource.password"));

        return dataSource;
    }


    @Bean
    @Primary
    public SqlSessionFactory sqlSessionFactory(@Autowired @Qualifier("mysql1DataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

        factoryBean.setConfigLocation(resolver.getResource(MYBATIS_CONFIG));
        factoryBean.setMapperLocations(resolver.getResources(MYBATIS_MAPPER));

        return factoryBean.getObject();
    }

    @Bean
    public SqlSession sqlSession(@Autowired @Qualifier("sqlSessionFactory") SqlSessionFactory factory) {
        return new SqlSessionTemplate(factory);
    }
}
