package mindslab.mms.models.vo;

import lombok.Data;

import java.util.List;

@Data
public class AgentVO {
    private List<String> agentIdList; //for delete

    private String id;
    private String agentIp;
    private String agentDn;
    private String agentId;
    private String agentName;
    private String agentDepart;
    private String agentAvailable;
    private String updateId;
}
