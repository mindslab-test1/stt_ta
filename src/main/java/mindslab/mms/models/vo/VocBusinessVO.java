package mindslab.mms.models.vo;

import lombok.Data;

import java.util.List;

@Data
public class VocBusinessVO {
    private List<String> vocBusinessIdList; //for delete

    private String id;
    private String businessName;
    private String branchName;
    private String businessOffice;
    private String businessBureau;
    private String businessDepartment;
    private String policyArea;

    private String updateId;
}
