package mindslab.mms.models.vo;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

@Data
public class AccountVO {
    private List<String> userIdList; //for delete
    private List<String> usernameList; //for delete
    private Long id;
    private String username;
    private String password;
    private String name;
    private String userDepart;
    private String createDate;
    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
    private boolean isEnabled;
}
