package mindslab.mms.models.vo;

import lombok.Data;

import java.util.List;

@Data
public class KeywordVO {
    private List<String> keywordList; //for delete

    private String wordCodeValue;
    private String upsertId;
}
