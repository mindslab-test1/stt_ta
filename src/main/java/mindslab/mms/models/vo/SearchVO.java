package mindslab.mms.models.vo;

import lombok.Data;
import mindslab.mms.models.dto.OracleDTO;

import java.util.List;

@Data
public class SearchVO {
    private String recMetaSer;
    private int page;
    private int countPerPage;
    private String wordCodeValue;

    // filter
    private String schRecTime;
    private String schAgentIp;
    private String schAgentDn;
    private String schName;
    private String schDepart;
    private String schSpeaker;
    private String schBusinessName;
    private String schAgentId;
    private String schAgentNumber;
    private String schAgentName;
    private String schCustomerNumber;
    private String schStartDate;
    private String schEndDate;
    private String schStartTime;
    private String schEndTime;
    private String schWordCodeValue;
    private String schTalkSpeed;
    private String schSilenceTime;
    private String schModalSpeaker;

    private String query;
    private List<String> callIdList;
    private List<OracleDTO> oracleDTOList;
}
