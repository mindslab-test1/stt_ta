package mindslab.mms.models.dto;

import lombok.Data;

@Data
public class AgentDTO {
    private Long id;
    private String agent_ip;
    private String agent_dn;
    private String agent_id;
    private String agent_name;
    private String agent_depart;
    private String agent_available;
    private String update_time;
    private String update_id;
}
