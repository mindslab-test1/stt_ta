package mindslab.mms.models.dto;

import lombok.Data;

@Data
public class SttMetaDTO {
    private String rec_meta_ser;
    private String agent_id;
    private String agent_name;
    private String agent_number;
    private String agent_part1;
    private String customer_number;
    private String rec_direction;
    private Integer rec_time;
    private String max_silence_time;
    private String avg_tlk_speed;
    private String rec_start_time;
    private String rec_end_time;
    private String create_date;
    private String create_time;

    private String longcall_occur;
    private String call_time;
    private String speaker;
    private String agent_info;
    private String word_code_value;
    private String start_time;
    private String end_time;
}
