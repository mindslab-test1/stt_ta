package mindslab.mms.models.dto;

import lombok.Data;

@Data
public class DetectedKeywordDTO {
    private String rec_meta_ser;
    private String detected_seq;
    private String create_time;
    private String word_code_value;
    private String stt_sentence;
    private String create_date;

    private String speaker;
    private String count;
    private String agent_part1;
    private String agent_name;
    private String rec_direction;
    private String customer_number;
    private String agent_info;
    private String agent_number;
}
