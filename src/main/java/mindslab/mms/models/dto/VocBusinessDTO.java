package mindslab.mms.models.dto;

import lombok.Data;

@Data
public class VocBusinessDTO {
    private Long id;
    private String business_name;
    private String branch_name;
    private String business_office;
    private String business_bureau;
    private String business_department;
    private String policy_area;
    private String update_time;
    private String update_id;
}
