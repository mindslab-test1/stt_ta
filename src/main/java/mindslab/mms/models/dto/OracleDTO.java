package mindslab.mms.models.dto;

import lombok.Data;

@Data
public class OracleDTO {
    private String call_id;
    private String start_time;
    private String end_time;
    private String call_time;
    private String user_id;
    private String call_name;
    private String cid;
    private String extension;
    private String call_code;
}
