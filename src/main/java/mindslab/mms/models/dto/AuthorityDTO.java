package mindslab.mms.models.dto;

import lombok.Data;

@Data
public class AuthorityDTO {
    private String username;
    private String authority;
}
