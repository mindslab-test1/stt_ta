package mindslab.mms.models.dto;

        import lombok.Data;

@Data
public class KeywordDTO {
    private String word_code_value;
    private String upsert_id;
}
