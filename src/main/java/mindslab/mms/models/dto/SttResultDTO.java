package mindslab.mms.models.dto;

import lombok.Data;

@Data
public class SttResultDTO {
    private String rec_meta_ser;
    private String create_date;
    private String create_time;
    private String update_time;
    private String stt_seq;
    private Integer stt_start_point;
    private Integer stt_end_point;
    private String stt_tlk_code;
    private String stt_tlk_speed;
    private String silence_time;
    private String stt_sentence;

    private String speaker;
}
