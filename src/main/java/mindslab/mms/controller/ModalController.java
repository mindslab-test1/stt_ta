package mindslab.mms.controller;

import mindslab.mms.common.util.FileDownload;
import mindslab.mms.models.dto.KeywordDTO;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.dto.SttResultDTO;
import mindslab.mms.models.vo.KeywordVO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.ModalService;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ModalController {

    @Value("${excel.path}")
    private String excelPath;

    @Autowired
    private ModalService modalService;

    @RequestMapping(value="/getSttResult", method = {RequestMethod.GET})
    public List<SttResultDTO> getSttResult(SearchVO searchVO) {
        List<SttResultDTO> result = modalService.getSttResult(searchVO);
        return result;
    }

    @RequestMapping(value="/getSttMetaData", method = {RequestMethod.GET})
    public SttMetaDTO getSttMetaData(String recMetaSer) {
        SttMetaDTO result = modalService.getSttMetaData(recMetaSer);
        return result;
    }

    @RequestMapping(value="/getKeywordManagementList", method = {RequestMethod.GET})
    public List<KeywordDTO> getKeywordManagementList() {
        List<KeywordDTO> result = modalService.getKeywordManagementList();
        return result;
    }

    @RequestMapping(value="/addKeyword", method = {RequestMethod.POST})
    public String addKeyword(KeywordVO keywordVO) {
        int dupYn = modalService.checkDuplicateKeyword(keywordVO);
        if(dupYn > 0) {
            return "DUP";
        }

       boolean result = modalService.addKeyword(keywordVO);
       if(result) {
           return"SUCC";
       } else {
           return "FAIL";
       }
    }

    @RequestMapping(value="/deleteKeyword", method = {RequestMethod.POST})
    public boolean deleteKeyword(KeywordVO keywordVO) {
        boolean result = modalService.deleteKeyword(keywordVO);
        return result;
    }

    @RequestMapping(value = "/getSttExcel", method = {RequestMethod.GET})
    public void getSttTextExcel(SearchVO searchVO, HttpServletResponse response) {
        List<SttResultDTO> result = modalService.getSttResult(searchVO);
        setExcelData(result, response);
    }

    public void setExcelData(List<SttResultDTO> sttResultList, HttpServletResponse res) {
        String fileName="";
        // Sheet 생성
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet1");
        XSSFRow row = null;
        XSSFCell cell = null;
        int rowCount = 0;
        int cellCount = 0;

        // 첫번째 로우 폰트 설정
        Font headFont = workbook.createFont();
        headFont.setFontHeightInPoints((short) 11);
        headFont.setFontName("돋움");

        // 첫번째 로우 셀 스타일 설정
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.LEFT);
        headStyle.setFont(headFont);

        // 바디 폰트 설정
        Font bodyFont = workbook.createFont();
        bodyFont.setFontHeightInPoints((short) 9);
        bodyFont.setFontName("돋움");

        // 바디 스타일 설정
        CellStyle bodyStyle = workbook.createCellStyle();
        bodyStyle.setFont(bodyFont);
        bodyStyle.setWrapText(true);

        // 헤더 생성
        row = sheet.createRow(rowCount++);
        String[] headers = {"No","시작시간", "종료시간", "화자","속도","묵음시간","STT 텍스트"};
        for(int i=0; i<headers.length; i++){
            cell = row.createCell(cellCount++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headers[i]);
        }

        // 데이터 생성
        for (SttResultDTO sttResult : sttResultList) {
            row = sheet.createRow(rowCount++);
            cellCount = 0;
            int nCount = 0;

            if(cellCount == 0) {
                fileName = sttResult.getRec_meta_ser();
            }

            // 바디 셀에 데이터 입력, 스타일 적용
            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(rowCount-1);   // No

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttResult.getStt_start_point());  // 시작시간

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttResult.getStt_end_point());  // 종료시간

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttResult.getSpeaker());  // 화자

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttResult.getStt_tlk_speed());  // 속도

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttResult.getSilence_time());    // 묵음시

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttResult.getStt_sentence());  // 자재명
        }

        // 셀 와이드 설정
        for (int i=0; i<headers.length; i++){
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, Math.min(255*256, (sheet.getColumnWidth(i)) + 512));
        }

        // 엑셀생성 경로설정 및 자원종료
        String filePath = excelPath;
        String filename =  fileName +".xlsx";
        try {
            // 엑셀 파일 생성
            File fDir = new File(filePath);
            if (!fDir.exists()) { // 디렉토리 없으면 생성
                fDir.mkdirs();
            }

            FileOutputStream fout = new FileOutputStream(filePath + filename);
            workbook.write(fout);
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileDownload fd = new FileDownload();
        fd.fileDownload(filePath, filename, res);
    }

    @RequestMapping(value="/getLongcallConfig", method = {RequestMethod.GET})
    public int getLongcallConfig() {
        int result = modalService.getLongcallConfig();
        return result;
    }

    @RequestMapping(value="/updateLongcallConfig", method = {RequestMethod.POST})
    public boolean updateLongcallConfig(int longcallConfig) {
        boolean result = modalService.updateLongcallConfig(longcallConfig);
        return result;
    }
}
