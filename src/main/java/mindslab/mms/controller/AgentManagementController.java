package mindslab.mms.controller;

import mindslab.mms.models.dto.AgentDTO;
import mindslab.mms.models.vo.AgentVO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.AgentManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class AgentManagementController {

    @Autowired
    private AgentManagementService agentManagementService;

    @RequestMapping(value="/getAgentList", method = {RequestMethod.GET})
    public List<AgentDTO> getAgentList(SearchVO searchVO) {
        List<AgentDTO> result = agentManagementService.getAgentList(searchVO);
        return result;
    }

    @RequestMapping(value="/getAgentCount", method = {RequestMethod.GET})
    public int getAgentCount(SearchVO searchVO) {
        int result = agentManagementService.getAgentCount(searchVO);
        return result;
    }

    @RequestMapping(value="/addAgent", method = {RequestMethod.POST})
    public String addAgent(AgentVO agentVO) {
        int dupYn = agentManagementService.checkDupAgent(agentVO);
        if(dupYn > 0) {
            return "DUP";
        }

        boolean result = agentManagementService.addAgent(agentVO);
        if(result) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }

    @RequestMapping(value="/deleteAgent", method = {RequestMethod.POST})
    public String deleteAgent(AgentVO agentVO) {
        boolean result = agentManagementService.deleteAgent(agentVO);
        if(result) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }

    @RequestMapping(value="/updateAgent", method = {RequestMethod.POST})
    public String updateAgent(AgentVO agentVO) {
        /*int dupYn = agentManagementService.checkDupAgent(agentVO);
        if(dupYn > 0) {
            return "DUP";
        }*/

        boolean result = agentManagementService.updateAgent(agentVO);
        if(result) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }
}
