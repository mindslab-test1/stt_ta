package mindslab.mms.controller;

import mindslab.mms.models.vo.SearchVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RouterController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String intro() {
        return "/main";
    }

    @RequestMapping(value = "/login", method = {RequestMethod.GET})
    public String goLogin() {
        return "/login";
    }

    @RequestMapping(value = "/main", method = {RequestMethod.GET})
    public String goMain() {
        return "/main";
    }

    @RequestMapping(value = "/dashboard", method = {RequestMethod.GET})
    public String goDashboard() {
        return "content/dashboard";
    }

    @RequestMapping(value = "/searchSttText", method = {RequestMethod.GET})
    public String goSearchSttText() {
        return "content/search-stt-text";
    }

    @RequestMapping(value = "/searchBannedTerm", method = {RequestMethod.GET})
    public String goSearchBannedTerm() {
        return "content/search-banned-term";
    }

    @RequestMapping(value = "/interestedKeyword", method = {RequestMethod.GET})
    public String goInterestedKeyword(SearchVO searchVO, Model model) {
        model.addAttribute("searchVO", searchVO);
        return "content/interested-keyword";
    }

    @RequestMapping(value = "/speechRateManagement", method = {RequestMethod.GET})
    public String goSpeechRateManagement() {
        return "content/speech-rate-management";
    }

    @RequestMapping(value = "/searchSilentCall", method = {RequestMethod.GET})
    public String goSearchSilentCall() {
        return "content/search-silent-call";
    }

    @RequestMapping(value = "/highFrequencyKeyword", method = {RequestMethod.GET})
    public String goHighFrequencyKeyword() {
        return "content/high-frequency-keyword";
    }

    @RequestMapping(value = "/VOCBusinessNameManagement", method = {RequestMethod.GET})
    public String goVOCBusinessNameManagement() {
        return "content/VOC-business-name-management";
    }

    @RequestMapping(value = "/userManagement", method = {RequestMethod.GET})
    public String gouUserManagement() {
        return "content/user-management";
    }

    @RequestMapping(value = "/agentManagement", method = {RequestMethod.GET})
    public String gouAgentManagement() {
        return "content/agent-management";
    }

    @RequestMapping(value = "/sttText", method = {RequestMethod.GET})
    public String goSttText(String recMetaSer, Model model) {
        model.addAttribute("recMetaSer", recMetaSer);
        return "modal/stt-text";
    }
    @RequestMapping(value = "/realtimeStt", method = {RequestMethod.GET})
    public String goRealtimeStt(String recMetaSer, Model model) {
        model.addAttribute("recMetaSer", recMetaSer);
        return "modal/realtime-stt";
    }

    @RequestMapping(value = "/keywordManagement", method = {RequestMethod.GET})
    public String goKeywordManagement() {
        return "modal/keyword-management";
    }

    @RequestMapping(value = "/configManagement", method = {RequestMethod.GET})
    public String goConfigManagement() {
        return "modal/config-management";
    }

    @RequestMapping(value = "/changePassword", method = {RequestMethod.GET})
    public String changePassword() {
        return "modal/change-password";
    }

    @RequestMapping(value = "/hidden", method = RequestMethod.GET)
    public String getHiddenPage() {
        return "common/hidden";
    }
}
