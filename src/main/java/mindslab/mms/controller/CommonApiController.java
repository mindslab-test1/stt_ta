package mindslab.mms.controller;

import mindslab.mms.common.util.AudioClass;
import mindslab.mms.common.util.MultipartFileSender;
import mindslab.mms.models.dto.OracleDTO;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.AwsService;
import mindslab.mms.service.CommonService;
import mindslab.mms.service.HiddenService;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping(value = "/api")
public class CommonApiController {
    @Autowired
    private CommonService commonService;

    @Autowired
    private AwsService awsService;

    @Autowired
    private HiddenService hiddenService;

    @Value("${solr.url}")
    private String solrUrl;

    @RequestMapping(value = "/getWav", method = {RequestMethod.GET})
    public void getWav(String recMetaSer, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filePath = commonService.getMp3FilePath(recMetaSer);
        //filePath = "/home/mindslab/test.mp3";

        File getFile = new File(filePath);
        MultipartFileSender.fromFile(getFile).with(request).with(response).serveResource();
    }

    @RequestMapping(value = "/audioDownload", method = {RequestMethod.GET})
    public void getWav2(String recMetaSer, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filePath = commonService.getMp3FilePath(recMetaSer);
        //filePath = "/home/mindslab/test.mp3";

        String fileName = recMetaSer + ".mp3";
        if(filePath == null) {
            return;
        }
        AudioClass ad = new AudioClass();
        ad.audioDown(filePath, fileName, request, response);
    }

    @RequestMapping(value = "/getTeamList", method = {RequestMethod.GET})
    public List<String> getTeamList() {
        return commonService.getTeamList();
    }

    /*@RequestMapping(value = "/getTeamList", method = {RequestMethod.GET})
    public List<String> getOracleTeamList() {
        return commonService.getOracleTeamList();
    }*/

    @RequestMapping(value = "/getSolrData", method = {RequestMethod.GET})
    public List<HashMap<String, ?>> getSolrData(SearchVO searchVO) throws ParseException {
        SimpleDateFormat utcDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        utcDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        String startDateStr = searchVO.getSchStartDate() + "T" + searchVO.getSchStartTime() + ":00" + "+09:00";
        String endDateStr = searchVO.getSchEndDate() + "T" + searchVO.getSchEndTime() + ":00" + "+09:00";

        Date startUtcDate = utcDateFormat.parse(startDateStr);
        Date endUtcDate = utcDateFormat.parse(endDateStr);

        String startUtcDateStr = utcDateFormat.format(startUtcDate);
        String endUtcDateStr = utcDateFormat.format(endUtcDate);

        SolrClient solr = new HttpSolrClient.Builder(solrUrl).build();
        SolrQuery query = new SolrQuery();

        String param = "create_time:" + "[" + startUtcDateStr + " TO " + endUtcDateStr + "]";

        if (!StringUtils.isEmpty(searchVO.getSchSpeaker())) {
            param += " AND stt_tlk_code:" + searchVO.getSchSpeaker();
        }
        query.setQuery(param);
        query.setRows(0);
        query.setFacet(true);
        query.addFacetField("stt_sentence");
        QueryResponse rsp = null;
        try {
            rsp = solr.query(query);
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<FacetField> ffList = rsp.getFacetFields();
        List result = new ArrayList();
        for (FacetField ff : ffList) {
            List<FacetField.Count> fcList = ff.getValues();
            int count = 0;
            for (FacetField.Count fc : fcList) {
                if (fc.getCount() == 0) {
                    continue;
                }
                HashMap row = new HashMap();
                row.put("no", ++count);
                row.put("text", fc.getName());
                row.put("size", fc.getCount());
                result.add(row);
            }
        }
        return result;
    }

    @RequestMapping(value = "/fillVoc", method = {RequestMethod.GET})
    public void fillVoc() {
        // 상담AP 서버에서 금일 민원콜 조회
        // List<OracleDTO> oracleDTOList = awsService.getOracleCounselData();

        List<OracleDTO> oracleDTOList = new ArrayList();
        OracleDTO testOracleDTO = new OracleDTO();
        testOracleDTO.setCall_id("5678");
        testOracleDTO.setCall_time("65");
        testOracleDTO.setStart_time("183030");
        testOracleDTO.setEnd_time("183030");
        oracleDTOList.add(testOracleDTO);

        OracleDTO testOracleDTO2 = new OracleDTO();
        testOracleDTO2.setCall_id("20160801_08EA9E82-63D4-48AD-9875-888B37D50EAD_8002");
        testOracleDTO2.setStart_time("183030");
        testOracleDTO2.setEnd_time("183030");
        oracleDTOList.add(testOracleDTO2);

        //call_id 리스트 만든 후 tb_stt_meta 조회
        List<String> callIdList = new ArrayList();
        for(int i=0; i<oracleDTOList.size(); i++) {
            String callId = oracleDTOList.get(i).getCall_id();
            callIdList.add(callId);
        }
        SearchVO searchVO = new SearchVO();
        searchVO.setCallIdList(callIdList);

        List<SttMetaDTO> sttMetaDTOList = hiddenService.getVOCData(searchVO);

        // ap서버 데이터와 tb_stt_meta 데이터 비교후 존재하지 않는 데이터 별도의 리스트 생성
        List<OracleDTO> missingList = new ArrayList();
        for(int j=0; j<oracleDTOList.size(); j++) {
            String recMetaSer = oracleDTOList.get(j).getCall_id();
            int idx = -1;
            for(int k=0; k<sttMetaDTOList.size(); k++) {
                if (sttMetaDTOList.get(k).getRec_meta_ser().equals(recMetaSer)) {
                    idx = -1;
                    break;
                } else {
                    idx = j;
                }
            }
            if(idx != -1) {
                OracleDTO oracleDTO = oracleDTOList.get(idx);
                String startTime = oracleDTO.getStart_time();
                String endTime = oracleDTO.getEnd_time();
                startTime = startTime.substring(0,2) + ":" + startTime.substring(2,4) + ":" + startTime.substring(4);
                endTime = endTime.substring(0,2) + ":" + endTime.substring(2,4) +  ":" + endTime.substring(4);
                oracleDTO.setStart_time(startTime);
                oracleDTO.setEnd_time(endTime);

                missingList.add(oracleDTO);
            }
        }

        SearchVO searchVO2 = new SearchVO();
        searchVO2.setOracleDTOList(missingList);
        hiddenService.addMissingData(searchVO2);
    }

    @RequestMapping(value = "/awsTest", method = {RequestMethod.GET})
    public void awsTest() {
        int result = awsService.awsTest();
    }
}
