package mindslab.mms.controller;

import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    @RequestMapping(value="/getDashboardList", method = {RequestMethod.GET})
    public List<SttMetaDTO> getDashboardList(SearchVO searchVO) {
        List<SttMetaDTO> result = dashboardService.getDashboardList(searchVO);
        return result;
    }

    @RequestMapping(value="/getDashboardCount", method = {RequestMethod.GET})
    public int getDashboardCount(SearchVO searchVO) {
        int result = dashboardService.getDashboardCount(searchVO);
        return result;
    }
}
