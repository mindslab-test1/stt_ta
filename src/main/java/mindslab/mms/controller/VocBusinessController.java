package mindslab.mms.controller;

import mindslab.mms.models.dto.AgentDTO;
import mindslab.mms.models.dto.VocBusinessDTO;
import mindslab.mms.models.vo.AgentVO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.models.vo.VocBusinessVO;
import mindslab.mms.service.AgentManagementService;
import mindslab.mms.service.VocBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class VocBusinessController {

    @Autowired
    private VocBusinessService vocBusinessService;

    @RequestMapping(value="/getVocBusinessList", method = {RequestMethod.GET})
    public List<VocBusinessDTO> getVocBusinessList(SearchVO searchVO) {
        List<VocBusinessDTO> result = vocBusinessService.getVocBusinessList(searchVO);
        return result;
    }

    @RequestMapping(value="/getVocBusinessCount", method = {RequestMethod.GET})
    public int getVocBusinessCount(SearchVO searchVO) {
        int result = vocBusinessService.getVocBusinessCount(searchVO);
        return result;
    }

    @RequestMapping(value="/addVocBusiness", method = {RequestMethod.POST})
    public String addVocBusiness(VocBusinessVO vocBusinessVO) {
        int dupYn = vocBusinessService.checkDupVocBusiness(vocBusinessVO);
        if(dupYn > 0) {
            return "DUP";
        }

        boolean result = vocBusinessService.addVocBusiness(vocBusinessVO);
        if(result) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }

    @RequestMapping(value="/deleteVocBusiness", method = {RequestMethod.POST})
    public String deleteVocBusiness(VocBusinessVO vocBusinessVO) {
        boolean result = vocBusinessService.deleteVocBusiness(vocBusinessVO);
        if(result) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }

    @RequestMapping(value="/updateVocBusiness", method = {RequestMethod.POST})
    public String updateVocBusiness(VocBusinessVO vocBusinessVO) {
        /*int dupYn = vocBusinessService.checkDupVocBusiness(vocBusinessVO);
        if(dupYn > 0) {
            return "DUP";
        }*/
        boolean result = vocBusinessService.updateVocBusiness(vocBusinessVO);
        if(result) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }
}
