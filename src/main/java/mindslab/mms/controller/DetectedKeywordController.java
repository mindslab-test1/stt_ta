package mindslab.mms.controller;

import mindslab.mms.common.util.FileDownload;
import mindslab.mms.models.dto.DetectedKeywordDTO;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.DetectedKeywordService;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class DetectedKeywordController {

    @Value("${excel.path}")
    private String excelPath;

    @Autowired
    private DetectedKeywordService detectedKeywordService;

    @RequestMapping(value="/getKeywordList", method = {RequestMethod.GET})
    public List<DetectedKeywordDTO> getKeywordList(SearchVO searchVO) {
        List<DetectedKeywordDTO> result = detectedKeywordService.getKeywordList(searchVO);
        return result;
    }

    @RequestMapping(value="/getKeywordCount", method = {RequestMethod.GET})
    public int getKeywordCount(SearchVO searchVO) {
        int result = detectedKeywordService.getKeywordCount(searchVO);
        return result;
    }

    @RequestMapping(value="/getKeywordDetailList", method = {RequestMethod.GET})
    public List<DetectedKeywordDTO> getKeywordDetailList(SearchVO searchVO) {
        List<DetectedKeywordDTO> result = detectedKeywordService.getKeywordDetailList(searchVO);
        return result;
    }

    @RequestMapping(value="/getGraphData", method = {RequestMethod.GET})
    public List<DetectedKeywordDTO> getGraphData(SearchVO searchVO) {
        List<DetectedKeywordDTO> result = detectedKeywordService.getGraphData(searchVO);
        return result;
    }

    @RequestMapping(value="/getKeywordExcel", method = {RequestMethod.GET})
    public void getKeywordExcel(SearchVO searchVO, HttpServletResponse response) throws Exception {
        List<DetectedKeywordDTO> result1 = detectedKeywordService.getKeywordExcel(searchVO);
        List<DetectedKeywordDTO> result2 = detectedKeywordService.getGraphData(searchVO);
        List<DetectedKeywordDTO> result3 = detectedKeywordService.getKeywordDetailList(searchVO);

        setExcelData(result1, result2, result3, response);
    }

    public void setExcelData(List<DetectedKeywordDTO> detectedKeywordDTOList, List<DetectedKeywordDTO> detectedKeywordDTOList2,
                             List<DetectedKeywordDTO> detectedKeywordDTOList3, HttpServletResponse res) throws Exception {

        // Sheet 생성
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet1");
        XSSFSheet sheet2 = workbook.createSheet("sheet2");
        XSSFSheet sheet3 = workbook.createSheet("sheet3");
        XSSFRow row = null;
        XSSFRow row2 = null;
        XSSFRow row3 = null;
        XSSFCell cell = null;


        int rowCount = 0;
        int cellCount = 0;

        // 첫번째 로우 폰트 설정
        Font headFont = workbook.createFont();
        headFont.setFontHeightInPoints((short) 11);
        headFont.setFontName("돋움");

        // 첫번째 로우 셀 스타일 설정
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.LEFT);
        headStyle.setFont(headFont);

        // 바디 폰트 설정
        Font bodyFont = workbook.createFont();
        bodyFont.setFontHeightInPoints((short) 9);
        bodyFont.setFontName("돋움");

        // 바디 스타일 설정
        CellStyle bodyStyle = workbook.createCellStyle();
        bodyStyle.setFont(bodyFont);
        bodyStyle.setWrapText(true);

        // 헤더 생성
        row = sheet.createRow(rowCount++);
        String[] headers = {"No", "키워드", "COUNT"};
        for(int i=0; i<headers.length; i++){
            cell = row.createCell(cellCount++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headers[i]);
        }

        // 데이터 생성
        for (DetectedKeywordDTO detectedKeywordDTO : detectedKeywordDTOList) {
            row = sheet.createRow(rowCount++);
            cellCount = 0;
            int nCount = 0;

            // 바디 셀에 데이터 입력, 스타일 적용
            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(rowCount-1);   // No

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getWord_code_value());  // 키워드

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getCount());  // Count
        }

        // 셀 와이드 설정
        for (int i=0; i<headers.length; i++){
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, Math.min(255*256, (sheet.getColumnWidth(i)) + 512));
        }

        //sheet2 start
        rowCount = 0;
        cellCount = 0;

        // 헤더 생성
        row2 = sheet2.createRow(rowCount++);
        String[] headers2 = {"No", "날짜", "COUNT"};
        for(int i=0; i<headers2.length; i++){
            cell = row2.createCell(cellCount++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headers2[i]);
        }

        // 데이터 생성
        for (DetectedKeywordDTO detectedKeywordDTO : detectedKeywordDTOList2) {
            row2 = sheet2.createRow(rowCount++);
            cellCount = 0;
            int nCount = 0;

            // 바디 셀에 데이터 입력, 스타일 적용
            cell = row2.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(rowCount-1);   // No

            cell = row2.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getCreate_date());  // 날짜


            cell = row2.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getCount());  // Count
        }

        // 셀 와이드 설정
        for (int i=0; i<headers2.length; i++){
            sheet2.autoSizeColumn(i, true);
            sheet2.setColumnWidth(i, Math.min(255*256, (sheet2.getColumnWidth(i)) + 512));
        }

        //sheet3 start
        rowCount = 0;
        cellCount = 0;

        // 헤더 생성
        row3 = sheet3.createRow(rowCount++);
        String[] headers3 = {"No", "키워드", "소속팀", "상담사", "고객번호", "발화자", "원문"};
        for(int i=0; i<headers3.length; i++){
            cell = row3.createCell(cellCount++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headers3[i]);
        }

        // 데이터 생성
        for (DetectedKeywordDTO detectedKeywordDTO : detectedKeywordDTOList3) {
            row3 = sheet3.createRow(rowCount++);
            cellCount = 0;
            int nCount = 0;

            // 바디 셀에 데이터 입력, 스타일 적용
            cell = row3.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(rowCount-1);   // No

            cell = row3.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getWord_code_value());  // 키워드


            cell = row3.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getAgent_part1());  // 소속팀

            cell = row3.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getAgent_info());  // 상담사


            cell = row3.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getCustomer_number());  // 고객번호

            cell = row3.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getSpeaker());  // 발화자


            cell = row3.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(detectedKeywordDTO.getStt_sentence());  // 원문
        }

        // 셀 와이드 설정
        for (int i=0; i<headers3.length; i++){
            sheet3.autoSizeColumn(i, true);
            sheet3.setColumnWidth(i, Math.min(255*256, (sheet3.getColumnWidth(i)) + 512));
        }

        // 엑셀생성 경로설정 및 자원종료
        String filePath = excelPath;
        String filename =  "키워드탐지콜조회" +".xlsx";
        try {
            // 엑셀 파일 생성
            File fDir = new File(filePath);
            if (!fDir.exists()) { // 디렉토리 없으면 생성
                fDir.mkdirs();
            }

            FileOutputStream fout = new FileOutputStream(filePath + filename);
            workbook.write(fout);
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileDownload fd = new FileDownload();
        fd.fileDownload(filePath, filename, res);
    }
}
