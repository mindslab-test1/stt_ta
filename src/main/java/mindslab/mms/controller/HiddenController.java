package mindslab.mms.controller;

import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.HiddenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class HiddenController {

    @Autowired
    private HiddenService hiddenService;

    @RequestMapping(value = "/executeQuery", method = {RequestMethod.POST})
    public Object executeQuery(String type, String query) {
        SearchVO searchVO = new SearchVO();
        searchVO.setQuery(query);
        try {
            if (type.equals("select")) {
                return hiddenService.hiddenSelect(searchVO);
            } else if (type.equals("insert")) {
                return hiddenService.hiddenInsert(searchVO);
            } else if (type.equals("update")) {
                return hiddenService.hiddenUpdate(searchVO);
            } else if (type.equals("delete")) {
                return hiddenService.hiddenDelete(searchVO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "type을 확인해주세요";
    }
}
