package mindslab.mms.controller;

import mindslab.mms.common.util.FileDownload;
import mindslab.mms.models.dto.DetectedKeywordDTO;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.SilenceTimeService;
import mindslab.mms.service.SpeechRateService;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class SilenceTimeController {

    @Value("${excel.path}")
    private String excelPath;

    @Autowired
    private SilenceTimeService silenceTimeService;

    @RequestMapping(value="/getSilenceTimeList", method = {RequestMethod.GET})
    public List<SttMetaDTO> getSilenceTimeList(SearchVO searchVO) {
        List<SttMetaDTO> result = silenceTimeService.getSilenceTimeList(searchVO);
        return result;
    }

    @RequestMapping(value="/getSilenceTimeCount", method = {RequestMethod.GET})
    public int getSilenceTimeCount(SearchVO searchVO) {
        int result = silenceTimeService.getSilenceTimeCount(searchVO);
        return result;
    }

    @RequestMapping(value="/getSilenceTimeExcel", method = {RequestMethod.GET})
    public void getSilenceTimeExcel(SearchVO searchVO, HttpServletResponse response) {
        List<SttMetaDTO> result = silenceTimeService.getSilenceTimeExcel(searchVO);
        setExcelData(result, response);
    }

    public void setExcelData(List<SttMetaDTO> sttMetaDTOList, HttpServletResponse res) {

        // Sheet 생성
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet1");
        XSSFRow row = null;
        XSSFCell cell = null;
        int rowCount = 0;
        int cellCount = 0;

        // 첫번째 로우 폰트 설정
        Font headFont = workbook.createFont();
        headFont.setFontHeightInPoints((short) 11);
        headFont.setFontName("돋움");

        // 첫번째 로우 셀 스타일 설정
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.LEFT);
        headStyle.setFont(headFont);

        // 바디 폰트 설정
        Font bodyFont = workbook.createFont();
        bodyFont.setFontHeightInPoints((short) 9);
        bodyFont.setFontName("돋움");

        // 바디 스타일 설정
        CellStyle bodyStyle = workbook.createCellStyle();
        bodyStyle.setFont(bodyFont);
        bodyStyle.setWrapText(true);

        // 헤더 생성
        row = sheet.createRow(rowCount++);
        String[] headers = {"No", "소속팀", "상담사", "내선번호", "고객번호", "상담일시", "묵음시간(초)"};
        for(int i=0; i<headers.length; i++){
            cell = row.createCell(cellCount++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headers[i]);
        }

        // 데이터 생성
        for (SttMetaDTO sttMetaDTO : sttMetaDTOList) {
            row = sheet.createRow(rowCount++);
            cellCount = 0;
            int nCount = 0;

            // 바디 셀에 데이터 입력, 스타일 적용
            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(rowCount-1);   // No

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMetaDTO.getAgent_part1());  // 소속팀

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMetaDTO.getAgent_info());  // 상담사

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMetaDTO.getAgent_number());  // 내선번호

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMetaDTO.getCustomer_number());  // 고객번호

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMetaDTO.getCreate_time());    // 상담일시

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMetaDTO.getMax_silence_time());  //묵음시간
        }

        // 셀 와이드 설정
        for (int i=0; i<headers.length; i++){
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, Math.min(255*256, (sheet.getColumnWidth(i)) + 512));
        }

        // 엑셀생성 경로설정 및 자원종료
        String filePath = excelPath;
        String filename =  "묵음탐지콜조회" +".xlsx";
        try {
            // 엑셀 파일 생성
            File fDir = new File(filePath);
            if (!fDir.exists()) { // 디렉토리 없으면 생성
                fDir.mkdirs();
            }

            FileOutputStream fout = new FileOutputStream(filePath + filename);
            workbook.write(fout);
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileDownload fd = new FileDownload();
        fd.fileDownload(filePath, filename, res);
    }
}
