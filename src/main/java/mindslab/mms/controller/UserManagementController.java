package mindslab.mms.controller;

import mindslab.mms.models.dto.AccountDTO;
import mindslab.mms.models.vo.AccountVO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class UserManagementController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value="/getUserList", method = {RequestMethod.GET})
    public List<AccountDTO> getUserList(SearchVO searchVO) {
        List<AccountDTO> result = accountService.getUserList(searchVO);
        return result;
    }

    @RequestMapping(value="/getUserCount", method = {RequestMethod.GET})
    public int getUserCount(SearchVO searchVO) {
        int result = accountService.getUserCount(searchVO);
        return result;
    }

    @RequestMapping(value="/addUser", method = {RequestMethod.POST})
    public String addUser(AccountVO accountVO) {
        int dupYn = accountService.checkDupUser(accountVO);
        if(dupYn > 0) {
            return "DUP";
        }

        boolean result = accountService.addUser(accountVO);
        if(result) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }

    @RequestMapping(value="/updateUser", method = {RequestMethod.POST})
    public String updateUser(AccountVO accountVO) {
        boolean result = accountService.updateUser(accountVO);
        if(result) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }

    @RequestMapping(value="/deleteUser", method = {RequestMethod.POST})
    public String deleteUser(AccountVO accountVO) {
        boolean result = accountService.deleteUser(accountVO);
        if(result) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }

    @RequestMapping(value="/getUserInfo", method = RequestMethod.GET)
    public AccountDTO getUserInfo() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userId = auth.getName();

        AccountDTO result = accountService.getAccount(userId);
        return result;
    }
}
