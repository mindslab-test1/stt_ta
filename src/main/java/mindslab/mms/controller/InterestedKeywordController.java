package mindslab.mms.controller;

import mindslab.mms.common.util.FileDownload;
import mindslab.mms.models.dto.DetectedKeywordDTO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.InterestedKeywordService;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping(value = "/api")
public class InterestedKeywordController {

    @Autowired
    private InterestedKeywordService interestedKeywordService;

    @Value("${solr.url}")
    private String solrUrl;

    @Value("${excel.path}")
    private String excelPath;

    @RequestMapping(value="/getInterestedKeywordList", method = {RequestMethod.GET})
    public List<HashMap<String, Object>> getInterestedKeywordList(SearchVO searchVO) throws ParseException {
        SolrClient solr = new HttpSolrClient.Builder(solrUrl).build();

        SolrQuery query = new SolrQuery();

        if(searchVO.getCountPerPage() != 0) {
            query.setRows(searchVO.getCountPerPage());
            query.setStart(searchVO.getCountPerPage() * (searchVO.getPage() - 1));
        } else {
            query.setRows(Integer.MAX_VALUE);
        }
        SimpleDateFormat utcDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        utcDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        String startDateStr = searchVO.getSchStartDate() + "T" + searchVO.getSchStartTime() + ":00" + "+09:00";
        String endDateStr = searchVO.getSchEndDate() + "T" + searchVO.getSchEndTime() + ":00" + "+09:00";

        Date startUtcDate = utcDateFormat.parse(startDateStr);
        Date endUtcDate = utcDateFormat.parse(endDateStr);

        String startUtcDateStr = utcDateFormat.format(startUtcDate);
        String endUtcDateStr = utcDateFormat.format(endUtcDate);

        String param = "create_time:" + "[" + startUtcDateStr + " TO " + endUtcDateStr + "]";
        if(!StringUtils.isEmpty(searchVO.getSchAgentId())) {
            param += " AND agent_id:" +searchVO.getSchAgentId();
        }
        if(!StringUtils.isEmpty(searchVO.getSchAgentNumber())) {
            param += " AND agent_number:" +searchVO.getSchAgentNumber();
        }
        if(!StringUtils.isEmpty(searchVO.getSchSpeaker())){
            param += " AND stt_tlk_code:" +searchVO.getSchSpeaker();
        }
        if(!StringUtils.isEmpty(searchVO.getSchCustomerNumber())) {
            param += " AND customer_number:" +searchVO.getSchCustomerNumber();
        }
        if(!StringUtils.isEmpty(searchVO.getSchDepart())) {
            param += " AND agent_part1:" +searchVO.getSchDepart();
        }
        if(!StringUtils.isEmpty(searchVO.getSchWordCodeValue())) {
            param += " AND stt_sentence:" +searchVO.getSchWordCodeValue();
        }

        query.setQuery(param);
        query.setSort("create_time", SolrQuery.ORDER.desc);
        QueryResponse rsp = null;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ArrayList result = new ArrayList();
        int numFound = 0;

        try {
            rsp = solr.query(query);
            SolrDocumentList solrDocumentList = rsp.getResults();

            //set numFound
            numFound = (int) solrDocumentList.getNumFound();
            HashMap<String, Object> firstRow = new HashMap<>();
            firstRow.put("numFound", numFound);
            result.add(firstRow);

            for(SolrDocument solrDocument: solrDocumentList) {
                Collection<String> fieldNames = solrDocument.getFieldNames();
                HashMap<String, Object> solrRow = new HashMap<>();
                for (String key: fieldNames) {
                    if(key.equals("create_time")) {
                        Date date = (Date)solrDocument.getFieldValue(key);
                        solrRow.put(key, format.format(date));
                        continue;
                    }
                    solrRow.put(key, solrDocument.getFieldValue(key));
                }
                result.add(solrRow);
            }
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value="/getInterestedKeywordCount", method = {RequestMethod.GET})
    public int getInterestedKeywordCount(SearchVO searchVO) {
        int result = interestedKeywordService.getInterestedKeywordCount(searchVO);
        return result;
    }

    @RequestMapping(value="/getInterestedKeywordExcel", method = {RequestMethod.GET})
    public void getInterestedKeywordExcel(SearchVO searchVO, HttpServletResponse response) throws ParseException {
        //List<DetectedKeywordDTO> result = interestedKeywordService.getInterestedKeywordExcel(searchVO);
        List<HashMap<String, Object>> result = getInterestedKeywordList(searchVO);
        setExcelData(result, response);
    }

    public void setExcelData(List<HashMap<String, Object>> detectedKeywordDTOList, HttpServletResponse res) {

        // Sheet 생성
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet1");
        XSSFRow row = null;
        XSSFCell cell = null;
        int rowCount = 0;
        int cellCount = 0;
        boolean skipFlag = false;

        // 첫번째 로우 폰트 설정
        Font headFont = workbook.createFont();
        headFont.setFontHeightInPoints((short) 11);
        headFont.setFontName("돋움");

        // 첫번째 로우 셀 스타일 설정
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.LEFT);
        headStyle.setFont(headFont);

        // 바디 폰트 설정
        Font bodyFont = workbook.createFont();
        bodyFont.setFontHeightInPoints((short) 9);
        bodyFont.setFontName("돋움");

        // 바디 스타일 설정
        CellStyle bodyStyle = workbook.createCellStyle();
        bodyStyle.setFont(bodyFont);
        bodyStyle.setWrapText(true);

        // 헤더 생성
        row = sheet.createRow(rowCount++);
        String[] headers = {"No", "소속팀","상담사", "내선번호", "발화자", "고객번호","상담일시","원문"};
        for(int i=0; i<headers.length; i++){
            cell = row.createCell(cellCount++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headers[i]);
        }

        // 데이터 생성
        for (HashMap<String, Object> detectedKeywordDTO : detectedKeywordDTOList) {
            if(!skipFlag) {
                skipFlag = true;
                continue;
            }
            row = sheet.createRow(rowCount++);
            cellCount = 0;

            // 바디 셀에 데이터 입력, 스타일 적용
            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(rowCount-1);   // No

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue((String) detectedKeywordDTO.get("agent_part1"));  // 소속팀

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue((String) detectedKeywordDTO.get("agent_info"));  // 상담사

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue((String) detectedKeywordDTO.get("agent_number"));  // 내선번호

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            if(detectedKeywordDTO.get("stt_tlk_code").equals("A")){
                cell.setCellValue("고객");  // 발화자
            } else if(detectedKeywordDTO.get("stt_tlk_code").equals("C")) {
                cell.setCellValue("상담사");
            } else {
                cell.setCellValue("");
            }

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue((String) detectedKeywordDTO.get("customer_number"));  // 고객번호

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue((String) detectedKeywordDTO.get("create_time"));    // 상담일시

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue((String) detectedKeywordDTO.get("stt_sentence"));  // 원문
        }

        // 셀 와이드 설정
        for (int i=0; i<headers.length; i++){
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, Math.min(255*256, (sheet.getColumnWidth(i)) + 512));
        }

        // 엑셀생성 경로설정 및 자원종료
        String filePath = excelPath;
        String filename =  "관심키워드" +".xlsx";
        try {
            // 엑셀 파일 생성
            File fDir = new File(filePath);
            if (!fDir.exists()) { // 디렉토리 없으면 생성
                fDir.mkdirs();
            }

            FileOutputStream fout = new FileOutputStream(filePath + filename);
            workbook.write(fout);
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileDownload fd = new FileDownload();
        fd.fileDownload(filePath, filename, res);
    }
}
