package mindslab.mms.controller;

import mindslab.mms.common.util.FileDownload;
import mindslab.mms.models.dto.SttMetaDTO;
import mindslab.mms.models.dto.SttResultDTO;
import mindslab.mms.models.vo.SearchVO;
import mindslab.mms.service.SttTextService;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class SttTextController {
    @Value("${excel.path}")
    private String excelPath;

    @Autowired
    private SttTextService sttTextService;

    @RequestMapping(value="/getSttTextList", method = {RequestMethod.GET})
    public List<SttMetaDTO> getSttTextList(SearchVO searchVO) {
        List<SttMetaDTO> result = sttTextService.getSttTextList(searchVO);
        return result;
    }

    @RequestMapping(value="/getSttTextCount", method = {RequestMethod.GET})
    public int getSttTextCount(SearchVO searchVO) {
        int result = sttTextService.getSttTextCount(searchVO);
        return result;
    }

    @RequestMapping(value="/getSttTextExcel", method = {RequestMethod.GET})
    public void getSttTextExcel(SearchVO searchVO, HttpServletResponse response) {
        List<SttMetaDTO> result = sttTextService.getSttTextExcel(searchVO);
        setExcelData(result, response);
    }

    public void setExcelData(List<SttMetaDTO> sttMetaList, HttpServletResponse res) {

        // Sheet 생성
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet1");
        XSSFRow row = null;
        XSSFCell cell = null;
        int rowCount = 0;
        int cellCount = 0;

        // 첫번째 로우 폰트 설정
        Font headFont = workbook.createFont();
        headFont.setFontHeightInPoints((short) 11);
        headFont.setFontName("돋움");

        // 첫번째 로우 셀 스타일 설정
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.LEFT);
        headStyle.setFont(headFont);

        // 바디 폰트 설정
        Font bodyFont = workbook.createFont();
        bodyFont.setFontHeightInPoints((short) 9);
        bodyFont.setFontName("돋움");

        // 바디 스타일 설정
        CellStyle bodyStyle = workbook.createCellStyle();
        bodyStyle.setFont(bodyFont);
        bodyStyle.setWrapText(true);

        // 헤더 생성
        row = sheet.createRow(rowCount++);
        String[] headers = {"No", "소속팀","상담사", "내선번호", "고객번호","상담일자","상담시간","통화길이(초)"};
        for(int i=0; i<headers.length; i++){
            cell = row.createCell(cellCount++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headers[i]);
        }

        // 데이터 생성
        for (SttMetaDTO sttMeta : sttMetaList) {
            row = sheet.createRow(rowCount++);
            cellCount = 0;
            int nCount = 0;

            // 바디 셀에 데이터 입력, 스타일 적용
            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(rowCount-1);   // No

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMeta.getAgent_part1());  // 소속팀

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMeta.getAgent_info());  // 상담사

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMeta.getAgent_number());  // 내선번호

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMeta.getCustomer_number());  // 고객번호

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMeta.getCreate_date());    // 상담일자

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);
            cell.setCellValue(sttMeta.getCall_time());  // 상담시간

            cell = row.createCell(cellCount++);
            cell.setCellStyle(bodyStyle);

            Integer recTime = sttMeta.getRec_time();
            if(recTime == null) {
                recTime = 0;
            }
            cell.setCellValue(recTime);  // 통화길이(초)
        }

        // 셀 와이드 설정
        for (int i=0; i<headers.length; i++){
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, Math.min(255*256, (sheet.getColumnWidth(i)) + 512));
        }

        // 엑셀생성 경로설정 및 자원종료
        String filePath = excelPath;
        String filename =  "STT텍스트조회" +".xlsx";
        try {
            // 엑셀 파일 생성
            File fDir = new File(filePath);
            if (!fDir.exists()) { // 디렉토리 없으면 생성
                fDir.mkdirs();
            }

            FileOutputStream fout = new FileOutputStream(filePath + filename);
            workbook.write(fout);
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileDownload fd = new FileDownload();
        fd.fileDownload(filePath, filename, res);
    }
}
