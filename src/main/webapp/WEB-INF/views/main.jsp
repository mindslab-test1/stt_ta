<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="/resources/images/ico_favicon_64x64.png" />
    <link rel="shortcut icon" type="image/x-icon" href="/resources/images/ico_favicon_64x64.ico"/>

    <title>1357 중소기업 통합콜센터</title>

    <link rel="stylesheet" type="text/css" href="/resources/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/all.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/font.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/common.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/datepicker.css" />

    <script type="text/javascript" src="/resources/js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="/resources/js/jquery-ui-custom.min.js"></script>
    <script type="text/javascript" src="/resources/js/grid.locale-en.js"></script>
    <script type="text/javascript" src="/resources/js/jquery.jqGrid.js"></script>
    <script type="text/javascript" src="/resources/js/chart-2.4.0.min.js"></script>
    <script type="text/javascript" src="/resources/js/common.js"></script>
    <script type="text/javascript" src="/resources/js/vue.2.6.10.js"></script>
    <script type="text/javascript" src="/resources/js/d3.v3.min.js"></script>
    <script type="text/javascript" src="/resources/js/d3.layout.cloud.js"></script>
    <script type="text/javascript" src="/resources/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/resources/js/bootstrap-datepicker.js"></script>

    <!-- //resources -->
</head>

<body>
    <!-- .page loading -->
    <div id="page_ldWrap" class="page_loading">
        <div class="loading_itemBox">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>


    <!-- wrap -->
    <div id="wrap">
        <%@ include file="common/topToolBar.jsp" %>

        <!-- #container -->
        <div id="container">
            <%@ include file="common/leftMenu.jsp" %>

            <!-- .contents -->
            <div class="contents">
            </div>
            <!-- //.contents -->
        </div>
        <!-- //#container -->
    </div>
    <!-- //wrap -->

    <div id="saveSuccessDialog" class="dialog"></div>
    <div id="saveFailDialog" class="dialog"></div>
    <div id="deleteSuccessDialog" class="dialog"></div>
    <div id="deleteFailDialog" class="dialog"></div>
    <div id="duplicateDialog" class="dialog"></div>
    <div id="saveConfirmDialog" class="dialog"></div>
    <div id="deleteConfirmDialog" class="dialog"></div>
</body>

<script type="text/javascript">
    var userName = '';
    var userId = '';
    var userDepart = '';
    var id = '';

    var contentVue = null;
    var inter = null;
    $(document).ready(function(){
        loadingInit();
        //notificationInit();
        pageLoading('dashboard');
    });

    function loadingInit() {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    }

    function pageLoading(id) {
        $('#' + id).addClass('active');
        $.ajax({
            type : "GET",
            url : id,
            dataType : "text",
            error : function(msg) {
                console.log(msg);
            },
            success : function(data) {
                if(contentVue){
                    contentVue.$destroy();
                }
                if(inter) {
                    clearInterval(inter);
                }
                $('.contents').html(data);

            }

        });
    }

    function getUrl(suffix) {
        var protocol = location.protocol + '//';
        var host = location.host;
        return protocol + host + suffix;
    }

    function changeTest() {
        if (Notification.permission === "granted") {
            var option = {
                body: '금칙어(배우자)가 탐지되었습니다.'
            }
            var noti = new Notification('금칙어 탐지', option);
        }
    }

    function setUserData(user) {
        this.userId = user['username'];
        this.userName = user['name'];
        this.userDepart = user['user_depart'];
        this.id = user['id'];
    }

    function getUserData() {
        var user = {
            userId: this.userId,
            userName: this.userName,
            userDepart: this.userDepart,
            id: this.id
        }
        return user;
    }

    function convertDateToStr(date) {
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();

        month = month < 10 ? "0" + month : month;
        day = day < 10 ? "0" + day : day;

        return year + '-' + month + '-' + day;
    }

    function addZero(num) {
        if(num < 10) {
            return '0' + num;
        } else {
            return num;
        }
    }
</script>