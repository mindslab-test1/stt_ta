<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<!-- icon_favicon -->
	<link rel="apple-touch-icon-precomposed" href="/resources/images/ico_favicon_64x64.png">
	<link rel="shortcut icon" type="image/x-icon" href="/resources/images/ico_favicon_64x64.ico"/>

	<title>1357 중소기업 통합콜센터</title>
	<!-- resources -->
	<link rel="stylesheet" type="text/css" href="/resources/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/resources/css/font.css" />
	<link rel="stylesheet" type="text/css" href="/resources/css/login.css" />
<%--	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--%>

	<script type="text/javascript" src="/resources/js/jquery-1.12.4.js"></script>
	<script type="text/javascript" src="/resources/js/jquery.cookie.js"></script>
	<!-- //resources -->
</head>

<body>
<!--[if lt IE 9]>
<div class="legacy_browser">
	<div class="legacyBox">
		<div class="tit">
			<h1><img src="/resources/images/logo_mindslab_w.png" alt="MINDsLab"></h1>
			<button type="button" class="btn_legacy_close">닫기</button>
		</div>
		<div class="txt">
			사용중인 브라우저는 지원이 중단된 브라우저입니다.<br>
			원활한 온라인 서비스를 위해 브라우저를 <a href="http://windows.microsoft.com/ko-kr/internet-explorer/ie-11-worldwide-languages" target="_blank">최신 버전</a>으로 업그레이드 해주세요.
		</div>
	</div>
</div>
<![endif]-->

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</div>
</div>
<!-- //.page loading -->


<div class="main_tit">
	<img src="/resources/images/logo_h.png" alt="중소벤처기업부">
</div>


<form class="loginWrap" id="valueForm" name="valueForm">
	<fieldset>
		<legend>Login</legend>
		<div class="loginBox">
			<div class="fl"><img src="/resources/images/ico_login.png" alt="login"></div>

			<div class="fr">
                <span>
					<!-- [D] ID값 에러일 경우 input에 클래스 error를 선언해주세요. -->
					<input type="text" autocomplete="off" name="username" id="ipt_id" class="ipt_txt voc_txt" title="아이디" placeholder="User Id" autocomplete="false" onkeypress="enterCheck()">
				</span>
				<span>
					<!-- [D] pw값 에러일 경우 input에 클래스 error를 선언해주세요. -->
					<input type="password" name="password" id="ipt_pw" class="ipt_txt voc_txt" title="비밀번호" placeholder="Password" autocomplete="false" onkeypress="enterCheck()">
				</span>
				<span class="checks">
                	<input type="checkbox" id="saveId" name="saveId" value="1" checked="checked">
                    <label for="saveId" class="check_voc">아이디 저장</label>
                </span>
				<span>
					<!-- [D] 에러일 경우 .txt_error 스크립트로 구현 -->
					<span class="txt_error">아이디 또는 비밀번호를 다시 확인하세요.<br>등록되지 않은 아이디이거나, 비밀번호를 잘못 입력하셨습니다.</span>
                	<em class="disbBox"></em>
                	<input type="button" name="btn_login" id="btn_login" class="btn_login disabled" title="Login" value="Login" disabled onclick="goLogin()">
                </span>
<%--				<a class="btn_forgot">비밀번호를 잊어 버렸습니까?</a>--%>
			</div>
		</div>
		<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"/>
	</fieldset>
</form>
<!-- //loginBox -->
<div class="copyRight"><span>MINDsLab &copy; 2019</span></div>

<script type="text/javascript">
	$(window).load(function() {
		$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
			$(this).remove();
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function (){
		// input 초기화
		$('.ipt_txt').each(function(){
			$(this).val('');
			$('#ipt_id').focus();
		});

		// 입력값 체크 (버튼 활성화)
		$('.ipt_txt').on('change keyup paste click', function(e) {
			var idValLth = $('#ipt_id').val().length;
			var pwValLth = $('#ipt_pw').val().length;

			if ( idValLth > 0 && pwValLth > 0) {
				$('.btn_login').removeClass('disabled');
				$('.btn_login').removeAttr('disabled');
				$('.disbBox').remove();
			} else {
				$('.btn_login').addClass('disabled');
				$('.btn_login').attr('disabled');
			}
		});

		// 브라우저 안내메세지 닫기
		$('.legacy_browser .legacyBox .tit .btn_legacy_close').on('click', function(e) {
			$('.legacy_browser').hide();
		});

		init();
	});

	function init() {
		var ckVal = $.cookie('saveId');

		if( $('#saveId').is(":checked") && ckVal.length > 0 ){
			$('#ipt_id').val(ckVal);
			$('#ipt_pw').focus();
		}else{
			$('#ipt_id').focus();
		}
	}

	//Login
	function goLogin(){
		var id = $('#ipt_id').val();
		var pw = $('#ipt_pw').val();

		// validation check

		if( $('#saveId').is(":checked") ){
			$.cookie('saveId', id, {expire:7});
		}

		valueForm.method = "POST";
		valueForm.action = "/loginProcess"
		valueForm.submit();
	}

	//엔터키 처리
	function enterCheck(event){
		if( window.event.keyCode == 13 ){
			goLogin();
		}else{
			return false;
		}
	}
</script>
</body>
</html>


