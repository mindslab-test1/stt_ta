<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- #header -->
<div id="header">
<%--    <h1><a target="_self" href=""><img src="/resources/images/logo_h.png" alt="중소벤처기업부"></a></h1>--%>

    <!-- .etcmenu -->
    <div class="etcmenu">
        <button type="button" class="btn_config_mng" v-on:click="openConfigManagement()">통화시간 설정</button>
        <button type="button" class="btn_keyword_srch" v-on:click="openKeywordManagement()">탐지 키워드 관리</button>
        <div class="userBox">
            <dl>
                <dt class="ico_user">User</dt>
                <dd>
                    <a target="_self" href="#none" id="userInfo">admin/관리자</a>
                </dd>
                <ul class="lst">
                    <li class="ico_pw"><a target="_self" href="#" v-on:click="openChangePassword()">비밀번호 변경</a></li>
                    <li class="ico_logout"><a target="_self" href="/logout">로그아웃</a></li>
                </ul>
            </dl>
        </div>
    </div>
    <!-- //.etcmenu -->
</div>
<!-- //#header -->

<script type="text/javascript">
    $(document).ready(function(){
        var toolBarVue;

        toolBarVue = new Vue({
            el: '#header',
            data: {
            },
            methods: {
                openKeywordManagement: function() {
                    $.ajax({
                        type : "GET",
                        url : "keywordManagement",
                        error : function(msg) {
                            console.log(msg);
                        },
                        success : function(data) {
                            $('#keywordManagement').html(data);
                            $('.keywordSrch').show();
                        }
                    });
                },
                openConfigManagement: function() {
                    $.ajax({
                        type : "GET",
                        url : "configManagement",
                        error : function(msg) {
                            console.log(msg);
                        },
                        success : function(data) {
                            $('#configManagement').html(data);
                            $('.configMng').show();
                        }
                    });
                },
                openChangePassword: function() {
                    $.ajax({
                        type : "GET",
                        url : "changePassword",
                        error : function(msg) {
                            console.log(msg);
                        },
                        success : function(data) {
                            $('#changePasswordModal').html(data);
                            $('#changePassword').show();
                        }
                    });
                },
                getUserInfo: function() {
                    var self = this;
                    var suffix = '/api/getUserInfo';
                    var url = getUrl(suffix);
                    var data = {};
                    $.get(url, data, function (result) {
                        $('#userInfo').html(result['username'] + '/' + result['name']);
                        setUserData(result);
                    });
                }
            },
            mounted: function () {
                this.getUserInfo();
            }
        });
    })
</script>