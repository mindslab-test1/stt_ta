<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="v-on" uri="http://www.springframework.org/tags/form" %>

<div class="paging_wrap" id="paging">
	<div class="paging">
		<span class="select_type single">
			<select id="pageInitPerPage" name="pageInitPerPage" v-model="pagingData['countPerPage']" v-on:change="goPage(0)">
				<option value="10" selected>10</option>
				<option value="15" selected>15</option>
				<option value="30">30</option>
				<option value="50">50</option>
			</select>
		</span>


		<a href="#" v-on:click="goPage(1)" class="paging_first"><i class="fas fa-step-backward"></i><span class="hide">처음 페이지로 이동</span></a>
		<a href="#" v-on:click="goPage(pagingData['currentPage']-1)" class="paging_prev"><i class="fas fa-caret-left"></i><span class="hide">이전 페이지로 이동</span></a>

		<span class="list">
			<span v-for="page in pagingData['pageList']">
				<span v-if="page==pagingData['currentPage']">
					<strong>{{page}}</strong>
				</span>
				<span v-else>
					<a href="#" v-on:click="goPage(page)">{{page}}</a>
				</span>
			</span>
		</span>

		<a href="#" v-on:click="goPage(pagingData['currentPage']+1)" class="paging_next"><i class="fas fa-caret-right"></i><span class="hide">다음 페이지로 이동</span></a>
		<a href="#" v-on:click="goPage(pagingData['totalPage'])" class="paging_last"><i class="fas fa-step-forward"></i><span class="hide">마지막 페이지로 이동</span></a>
	</div>
</div>


<script type="text/javascript">

</script>





