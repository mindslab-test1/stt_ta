<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript" src="/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/resources/js/vue.2.6.10.js"></script>

<div id="content">
    <div class="titArea">
        <h3>Hidden Menu</h3>
    </div>

    <%--select--%>
    <div style="display: flex; padding:10px;">
        <span style="width: 50px; height: 30px; padding: 10px;">Select</span>
        <textarea id="select" style="height:150px; width: 80%;"></textarea>
        <button v-on:click="execute('select')">Execute</button>
    </div>

    <%--insert--%>
    <div style="display: flex; padding:10px;">
        <span style="width: 50px; height: 30px; padding: 10px;">Insert</span>
        <textarea id="insert" style="height:150px; width: 80%;"></textarea>
        <button v-on:click="execute('insert')">Execute</button>
    </div>

    <%--update--%>
    <div style="display: flex; padding:10px;">
        <span style="width: 50px; height: 30px; padding: 10px;">Update</span>
        <textarea id="update" style="height:150px; width: 80%;"></textarea>
        <button v-on:click="execute('update')">Execute</button>
    </div>

    <%--delete--%>
    <div style="display: flex; padding:10px;">
        <span style="width: 50px; height: 30px; padding: 10px;">Delete</span>
        <textarea id="delete" style="height:150px; width: 80%;"></textarea>
        <button v-on:click="execute('delete')">Execute</button>
    </div>
</div>

<script type="text/javascript">
    new Vue({
        el: '#content',
        data: {},
        methods: {
            execute: function (type) {
                let query = $('#' + type).val();
                if(!query) {
                    return false;
                }
                console.log('type:: ' + type);
                console.log('query:: ' + query);

                $.ajax({
                    url : "/api/executeQuery",
                    traditional : true,
                    data : {
                        type: type,
                        query: query
                    },
                    type : "POST",
                    beforeSend : function(xhr) {
                        /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                        xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                    },
                }).done(function(data){
                    console.log(data);
                }).fail(function(data){
                    console.log(data);
                });
            }
        }
    });

</script>

