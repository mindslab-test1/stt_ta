<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- .snb -->
<div class="snb">
    <ul class="nav">
        <sec:authorize access="hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')">
            <li id='dashboard' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">assessment</i>대시보드</a></li>
            <li id='searchSttText' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">dashboard</i>STT 텍스트 조회</a></li>
            <li id='searchBannedTerm' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">settings_phone</i>키워드 탐지 콜 조회</a></li>
            <li id='interestedKeyword' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">bookmark_border</i>관심 키워드</a></li>
            <li id='speechRateManagement' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">scatter_plot</i>상담사 발화속도 관리</a></li>
            <li id='searchSilentCall' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">how_to_reg</i>묵음 탐지 콜 조회</a></li>
<%--            <li id='highFrequencyKeyword' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">show_chart</i>고빈도 키워드</a></li>--%>
            <li id='agentManagement' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">sms</i>상담사 관리</a></li>
<%--            <li id='VOCBusinessNameManagement' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">folder_shared</i>VOC 사업명 관리</a></li>--%>
        </sec:authorize>
        <sec:authorize access="hasAnyAuthority('ROLE_ADMIN')">
            <li id='userManagement' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">supervised_user_circle</i>사용자 관리</a></li>
        </sec:authorize>
        <%--<sec:authorize access="isAnonymous()">
            <li id='userManagement' onclick="pageLoading(this.id)"><a href="#"><i class="material-icons">supervised_user_circle</i>사용자 관리</a></li>
        </sec:authorize>--%>
    </ul>
</div>
<!-- //.snb -->