<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="/resources/js/common.js"></script>

<div id="content">

    <!-- .titArea -->
    <div class="titArea">
        <h3>실시간 Dashboard</h3>
    </div>
    <!-- //.titArea -->

    <!-- .srchArea -->
    <div class="srchArea">
        <dl class="dlBox">
            <dt>소속팀</dt>
            <dd>
                <div class="selectbox">
                    <label for="schDepart">전체</label>
                    <select id="schDepart">
                        <option value="" selected>전체</option>
                    </select>
                </div>
            </dd>
            <dt>발화자</dt>
            <dd>
                <div class="selectbox">
                    <label for="schSpeaker">전체</label>
                    <select id="schSpeaker" >
                        <option value="" selected>전체</option>
                        <option value="A">상담사</option>
                        <option value="C">고객</option>
                    </select>
                </div>
            </dd>
        </dl>
        <div class="dlBox">
            <dt>상담사 이름</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt ipt_wide" placeholder="" id="schAgentName" v-on:keyup.13="search()">
                </div>
            </dd>
        </div>
    </div>
    <!-- //.srchArea -->
    <div class="btnBox">
        <button type="submit" class="btnS_basic" v-on:click="search()"><img src="resources/images/ico_srch_w.png" alt="조회">조회</button>
    </div>
    <!-- .content -->
    <div class="content">
        <!-- .stn -->
        <div id="demo" class="stn">
            <demo-grid :key-arr="keyArr" :data="data" :header="header" :paging-data="pagingData"></demo-grid>
        </div>
        <!-- //.stn -->
        <!-- page_area -->
        <div class="page_area">
            <%@ include file="../common/paging.jsp" %>
        </div>
        <!-- //page_area -->
        <div id="modal"></div>
        <div id="keywordManagement"></div>
        <div id="configManagement"></div>
        <div id="changePasswordModal"></div>
    </div>
    <!-- //.content -->
</div>
<!-- component template -->

<script type="text/x-template" id="dashboard-table">
    <div>
        <table class="tbl_lst">
            <colgroup>
                <col width="60">
                <col width="10%">
                <col width="10%">
                <col width="15%">
                <col>
                <col>
                <col width="15%">
                <col width="7%">
                <col width="7%">
            </colgroup>
            <thead>
                <th v-for="column in header">
                    {{column}}
                </th>
            </thead>
        </table>
        <div class="tblBoxScroll" style="max-height:620px;">
            <table class="tbl_lst">
                <colgroup>
                    <col width="60">
                    <col width="10%">
                    <col width="10%">
                    <col width="15%">
                    <col>
                    <col>
                    <col width="15%">
                    <col width="7%">
                    <col width="7%">
                </colgroup>
                <tbody>
                    <tr v-if="data.length === 0">
                        <td colspan="9">데이터 없음</td>
                    </tr>
                     <tr v-else v-for="(row, idx) in data" v-bind:class="{longCall: row['longcall_occur' ] == 'Y'}">
                         <td>{{(pagingData['currentPage'] - 1) * pagingData['countPerPage'] + idx + 1}}</td>
                        <td v-for="key in keyArr">
                            {{row[key]}}
                        </td>
                        <td v-on:click="realtimeSttModal(row['rec_meta_ser'])"><span class=""><i class="material-icons">zoom_in</i></span></td>
                        <td v-if="row['rec_end_time']" v-on:click="sttTextModal(row['rec_meta_ser'])"><span class="lyr_view"><i class="material-icons">volume_up</i></span></td>
                        <td v-else="row['rec_end_time']"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<script type="text/javascript">

    $(document).ready(function(){
        Vue.component('demo-grid', {
            template: '#dashboard-table',
            props: {
                data: Array,
                header: Array,
                keyArr: Array,
                pagingData: Object
            },
            methods: {
                realtimeSttModal: function(recMetaSer) {
                    $.ajax({
                        type : "GET",
                        url : "realtimeStt",
                        dataType : "text",
                        data: {recMetaSer: recMetaSer},
                        error : function(msg) {
                            console.log(msg);
                        },
                        success : function(data) {
                            $('#modal').html(data);
                        }
                    });
                },
                sttTextModal: function(recMetaSer) {
                    $.ajax({
                        type : "GET",
                        url : "sttText",
                        dataType : "text",
                        data: {recMetaSer: recMetaSer},
                        error : function(msg) {
                            console.log(msg);
                        },
                        success : function(data) {
                            $('#modal').html(data);
                        }
                    });
                }
            }
        });

        contentVue = new Vue({
            el: '#content',
            data: {
                filterSpeaker: '',
                filterDepart: '',
                filterAgentName: '',
                ws: null,
                header: ['No', '소속', '상담사', '고객번호', '발화자', '탐지어', '상담일시', '보기', '청취'],
                keyArr: ['agent_part1', 'agent_info', 'customer_number', 'speaker', 'word_code_value', 'rec_start_time'],
                data: [],
                pagingData: {
                    //초기값 currentPage=1, countPerPage=15
                    totalPage: 0,
                    currentPage: 1,
                    countPerPage: 15,
                    pageList: false
                },
            },
            methods: {
                search: function() {
                    this.filterSpeaker = $('#schSpeaker').val();
                    this.filterDepart = $('#schDepart').val();
                    this.filterAgentName = $('#schAgentName').val();

                    this.getDashboardList(1);
                    this.getDashboardCount();
                },
                getTeamList: function() {
                    var self = this;
                    var suffix = '/api/getTeamList';
                    var url = getUrl(suffix);
                    var data = {};

                    $.get(url, data, function (result) {
                        if (result) {
                            result.forEach(function(item){
                                $('#schDepart').append('<option>' + item + '</option>');
                            });
                        }
                    });
                },
                goPage: function(page){
                    if(page <= 0){
                        page = 1;
                    } else if(page > this.pagingData.totalPage) {
                        page = this.pagingData.totalPage;
                    }
                    this.pagingData.currentPage = page;
                    this.getDashboardList(page);
                    this.getDashboardCount();
                },
                getDashboardList: function(page) {
                    var self = this;
                    var suffix = '/api/getDashboardList';
                    var url = getUrl(suffix);

                    var data = {
                        schSpeaker: self.filterSpeaker,
                        schDepart: self.filterDepart,
                        schAgentName: self.filterAgentName,
                        page: page,
                        countPerPage: this.pagingData.countPerPage
                    };

                    $.get(url, data, function (result) {
                        if (result) {
                            self.data = result;
                        } else {
                            self.data = [];
                        }
                    });
                },
                getDashboardCount: function() {
                    var self = this;
                    var suffix = '/api/getDashboardCount';
                    var url = getUrl(suffix);
                    var data = {
                        schSpeaker: self.filterSpeaker,
                        schDepart: self.filterDepart,
                        schAgentName: self.filterAgentName
                    };
                    $.get(url, data, function (result) {
                        var countPerPage = self.pagingData.countPerPage;
                        var currentPage = self.pagingData.currentPage;
                        var totalPage = Math.ceil(result/countPerPage);
                        var pageList = [];

                        self.pagingData.totalPage = totalPage;

                        var startIndex = Math.floor((currentPage-1)/10)*10; //올림
                        for(var i=startIndex; i<totalPage; i++) {
                            pageList.push(i+1);

                            if(pageList.length >= 10) {
                                break;
                            }
                        }
                        self.pagingData.pageList = pageList;
                    });
                },
                openWebSocket: function() {
                    var self = this;
                    this.ws = new WebSocket('ws://211.253.149.199:8093/websocket');
                    this.ws.onopen = function () {
                        var sendMsg = '{"TYPE":"subscribe"}';
                        self.ws.send(sendMsg);
                    };

                    this.ws.onmessage = function (e) {
                        var rcvData = JSON.parse(e.data);
                        console.log(rcvData);

                        if(rcvData['TYPE'] === 'START') { // new call start
                            if(self.filterDepart !== '' && self.filterDepart !== rcvData['AGENT_TEAM']) {
                                console.log('skip!');
                                return false;
                            } else if(self.filterSpeaker !== '' && self.filterSpeaker !== rcvData['TALKER']) {
                                console.log('skip2!');
                                return false;
                            } else if(self.filterAgentName !== '' && self.filterAgentName !== rcvData['AGENT_NAME']){
                                console.log('skip3!');
                                return false;
                            }
                            var row = {};
                            row['rec_meta_ser'] = rcvData['CALL_ID'];
                            row['agent_name'] = rcvData['AGENT_NAME'];
                            row['agent_id'] = rcvData['AGENT_ID'];
                            row['agent_info'] = rcvData['AGENT_ID'] + '/' + rcvData['AGENT_NAME'];
                            row['agent_part1'] = rcvData['AGENT_TEAM'];
                            row['customer_number'] = rcvData['CUSTOMER_NUMBER'];
                            row['rec_start_time'] = rcvData['START_TIME'];
                            self.data.pop();
                            self.data.unshift(row);
                            self.getDashboardCount();
                        } else if(rcvData['TYPE'] === 'STOP') { // call end
                            var recMetaSer = rcvData['CALL_ID'];
                            var data = self.data;
                            data.forEach(function(item, idx) {
                                if(item['rec_meta_ser'] === recMetaSer) {
                                    var row = data[idx];
                                    row['rec_end_time'] = true;
                                }
                            });
                        } else if(rcvData['TYPE'] === 'DETECTED') {
                            var recMetaSer = rcvData['CALL_ID'];
                            var data = self.data;
                            data.forEach(function(item, idx) {
                                if(item['rec_meta_ser'] === recMetaSer) {
                                    var row = data[idx];
                                    if(self.filterSpeaker !== '' && self.filterSpeaker !== rcvData['TALKER']) {
                                        return false;
                                    }
                                    if(row['speaker']) {
                                        if(!row['word_code_value'].includes(rcvData['KEYWORD'])) { //기존에 검출된 키워드인지 검색
                                            row['word_code_value'] = row['word_code_value'] + ',' + rcvData['KEYWORD'];
                                        }
                                        var speaker = row['speaker'];
                                        var newSpeaker = rcvData['TALKER'];
                                        if(speaker === '상담사,고객') {
                                            return ;
                                        } else if(speaker === '상담사' && newSpeaker === 'C') {
                                            row['speaker'] = '상담사,고객';
                                        } else if(speaker === '고객' && newSpeaker === 'A') {
                                            row['speaker'] = '상담사,고객';
                                        } else {
                                            // 기존 컬럼이 상담사인데 상담사가 들어온경우, 고객인데 고객이 들어온 경우
                                            return ;
                                        }
                                    } else {
                                        // 새로 들어오는 경우
                                        row['word_code_value'] = rcvData['KEYWORD'];
                                        if(rcvData['TALKER'] === 'A') {
                                            row['speaker'] = '상담사'
                                        } else { // rcvData['TALKER'] === 'C'
                                            row['speaker'] = '고객'
                                        }
                                    }
                                }
                            });
                        }  else if(rcvData['TYPE'] === 'LONG_CALL') {
                            var recMetaSer = rcvData['CALL_ID'];
                            var data = self.data;
                            data.forEach(function(item, idx) {
                                if(item['rec_meta_ser'] === recMetaSer) {
                                    var row = data[idx];
                                    row['longcall_occur'] = 'Y';
                                }
                            });
                        }
                    };
                }
            },
            mounted: function () {
                $.getScript('/resources/js/common.js');
                this.getDashboardList(1);
                this.getDashboardCount();
                this.getTeamList();
                this.openWebSocket();
            },
            destroyed: function() {
                if(this.ws) {
                    this.ws.close();
                }
            }
        });
    });
</script>