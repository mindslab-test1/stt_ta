<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="content">
    <!-- .titArea -->
    <div class="titArea">
        <h3>키워드 탐지 콜 조회</h3>
    </div>
    <!-- //.titArea -->
    <!-- .srchArea -->
    <div class="srchArea">
        <div class="dlBox">
            <dt>상담 일자</dt>
            <dd>
                <div class="dateBox">
                    <input type="text" autocomplete="off" id="schStartDate" class="ipt_date" placeholder="시작일" autocomplete="off">
                    <span class="hyphen">-</span>
                    <input type="text" autocomplete="off" id="schEndDate" class="ipt_date" placeholder="종료일" autocomplete="off">
                </div>
            </dd>
            <dt>상담사 사번</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt ipt_wide" placeholder="" id="schAgentId" v-on:keyup.13="search()">
                </div>
            </dd>
        </div>
        <div class="dlBox">
            <dt>소속팀</dt>
            <dd>
                <div class="selectbox">
                    <label for="schDepart">전체</label>
                    <select id="schDepart">
                        <option value="" selected>전체</option>
                    </select>
                </div>
            </dd>
            <dt>발화자</dt>
            <dd>
                <div class="selectbox">
                    <label for="schSpeaker">전체</label>
                    <select id="schSpeaker" >
                        <option value="" selected>전체</option>
                        <option value="A">상담사</option>
                        <option value="C">고객</option>
                    </select>
                </div>
            </dd>
        </div>
        <div class="dlBox">
            <dt>고객번호</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt ipt_wide" placeholder="" id="schCustomerNumber" v-on:keyup.13="search()">
                </div>
            </dd>
        </div>
    </div>
    <div class="btnBox">
        <button type="submit" class="btnS_basic" v-on:click="search()"><img src="resources/images/ico_srch_w.png" alt="조회">조회</button>
        <button type="button" class="btnS_basic" v-on:click="downloadExcel()"><img src="resources/images/ico_download_w.png" alt="다운로드">다운로드</button>
    </div>
    <!-- .content -->
    <div class="content">
        <div class="visual_stn">
            <div :class="{tblpg_a:isActive, tblpg_b:!isActive}">
                <div :class="{tbl_box:isActive, tbl_box_b:!isActive}">
                    <table-grid :key-arr="keyArr" :data="data" :header="header" :paging-data="pagingData"></table-grid>

                    <!-- 페이징 -->
                    <div class="page_area">
                        <%@ include file="../common/paging.jsp" %>
                    </div>
                </div>
            </div>
            <div :class="{graph_fr:isActive, graph_fr_b:!isActive}">
                <div  id="chart_div" style="height:270px;">
                    <canvas id="line-chart"></canvas>
                </div>
            </div>
        </div>

        <!-- .stn -->
        <div :class="{stn_b:isActive, stn:!isActive}">
            <div class="tbl_box_c">
                <table-grid2 :key-arr="keyArr2" :data="data2" :header="header2" :paging-data="pagingData2"></table-grid2>
            </div>
        </div>

        <div id="modal"></div>
        <div id="keywordManagement"></div>
        <div id="configManagement"></div>
        <div id="changePasswordModal"></div>
    </div>
    <!-- //.content -->
</div>
<!-- component template -->
<script type="text/x-template" id="search-banned-term-table">
    <div>
        <table class="tbl_lst">
            <colgroup>
                <col width="33%">
                <col width="34%">
                <col width="33%">
            </colgroup>
            <thead>
                <th v-for="column in header">
                    {{column}}
                </th>
            </thead>
        </table>
        <div class="tblBoxScroll" :class="{detect_a:contentVue.isActive, detect_b:!contentVue.isActive}">
            <table class="tbl_lst">
                <colgroup>
                    <col width="33%">
                    <col width="34%">
                    <col width="33%">
                </colgroup>
                <tbody>
                    <tr v-if="data.length === 0">
                        <td colspan="3">데이터 없음</td>
                    </tr>
                    <tr v-else v-for="(row, idx) in data" v-on:click="onChangeActive(row['word_code_value'])">
                        <td>{{(pagingData['currentPage'] - 1) * pagingData['countPerPage'] + idx + 1}}</td>
                        <td v-for="key in keyArr">
                            {{row[key]}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<script type="text/x-template" id="search-banned-term-table2">
    <div>
        <table class="tbl_lst">
            <colgroup>
                <col width="60">
                <col width="7%">
                <col width="10%">
                <col width="15%">
                <col width="10%">
                <col width="5%">
                <col>
                <col width="5%">
            </colgroup>
            <thead>
            <th v-for="column in header">
                {{column}}
            </th>
            </thead>
        </table>
        <div class="tblBoxScroll" style="max-height: 200px;">
            <table class="tbl_lst">
                <colgroup>
                    <col width="60">
                    <col width="7%">
                    <col width="10%">
                    <col width="15%">
                    <col width="10%">
                    <col width="5%">
                    <col>
                    <col width="5%">
                </colgroup>
                <tbody>
                    <tr v-if="data.length === 0">
                        <td colspan="7">데이터 없음</td>
                    </tr>
                    <tr v-else v-for="(row, idx) in data">
                        <td>{{(pagingData['currentPage'] - 1) * pagingData['countPerPage'] + idx + 1}}</td>
                        <td v-for="key in keyArr" :class="{stt_txt: key === 'stt_sentence'}">
                            {{row[key]}}
                        </td>
                        <td v-on:click="sttTextModal(row['rec_meta_ser'])"><span class="lyr_view"><i class="material-icons">volume_up</i></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<script type="text/javascript">

    Vue.component('table-grid', {
        template: '#search-banned-term-table',
        props: {
            data: Array,
            header: Array,
            keyArr: Array,
            pagingData: Object
        },
        methods: {
            onChangeActive: function (keyword) {
                contentVue.initChart();

                this.isActive = false;
                contentVue.isActive = false;
                contentVue.filterWordCodeValue = keyword;

                contentVue.getKeywordDetailList(keyword);
                contentVue.getChartData(keyword);
            }
        }
    });

    Vue.component('table-grid2', {
        template: '#search-banned-term-table2',
        props: {
            data: Array,
            header: Array,
            keyArr: Array,
            pagingData: Object
        },
        methods: {
            sttTextModal: function(recMetaSer) {
                $.ajax({
                    type : "GET",
                    url : "sttText",
                    dataType : "text",
                    data: {recMetaSer: recMetaSer},
                    error : function(msg) {
                        console.log(msg);
                    },
                    success : function(data) {
                        $('#modal').html(data);
                    }
                });
            }
        }
    });

    contentVue = new Vue({
        el: '#content',
        data: {
            chart: '',
            isActive: true,
            filterWordCodeValue: '',
            filterStartDate: '',
            filterEndDate: '',
            filterDepart: '',
            filterAgentId: '',
            filterSpeaker: '',
            filterCustomerNumber: '',
            pagingData: {
                //초기값 currentPage=1, countPerPage=10
                totalPage: 0,
                currentPage: 1,
                countPerPage: 10,
                pageList: false
            },
            header: ['No', '키워드', 'Count'],
            keyArr: ['word_code_value', 'count'],
            data: [],
            pagingData2: {
                //초기값 currentPage=1, countPerPage=15
                totalPage: 0,
                currentPage: 1,
                countPerPage: 15,
                pageList: false
            },
            header2: ['No', '키워드', '소속팀', '상담사', '고객번호', '발화자', '원문', '청취'],
            keyArr2: ['word_code_value',  'agent_part1', 'agent_info', 'customer_number', 'speaker', 'stt_sentence'],
            data2: []
        },
        methods: {
            search: function() {
                this.filterStartDate = $('#schStartDate').val();
                this.filterEndDate = $('#schEndDate').val();
                this.filterDepart = $('#schDepart').val();
                this.filterAgentId = $('#schAgentId').val();
                this.filterSpeaker = $('#schSpeaker').val();
                this.filterCustomerNumber = $('#schCustomerNumber').val();
                this.isActive = true;

                this.getKeywordList(1);
                this.getKeywordCount();
            },
            getTeamList: function() {
                var self = this;
                var suffix = '/api/getTeamList';
                var url = getUrl(suffix);
                var data = {};

                $.get(url, data, function (result) {
                    if (result) {
                        result.forEach(function(item){
                            $('#schDepart').append('<option>' + item + '</option>');
                        });
                    }
                });
            },
            goPage: function(page){
                if(page <= 0){
                    page = 1;
                } else if(page > this.pagingData.totalPage) {
                    page = this.pagingData.totalPage;
                }
                this.pagingData.currentPage = page;
                this.getKeywordList(page);
                this.getKeywordCount();
            },
            getKeywordCount: function() {
                var self = this;
                var suffix = '/api/getKeywordCount';
                var url = getUrl(suffix);
                var data = {
                    schStartDate: self.filterStartDate,
                    schEndDate: self.filterEndDate,
                    schDepart: self.filterDepart,
                    schAgentId: self.filterAgentId,
                    schSpeaker: self.filterSpeaker,
                    schCustomerNumber: self.filterCustomerNumber
                };
                $.get(url, data, function (result) {
                    var countPerPage = self.pagingData.countPerPage;
                    var currentPage = self.pagingData.currentPage;
                    var totalPage = Math.ceil(result/countPerPage);
                    var pageList = [];

                    self.pagingData.totalPage = totalPage;

                    var startIndex = Math.floor((currentPage-1)/10)*10; //올림
                    for(var i=startIndex; i<totalPage; i++) {
                        pageList.push(i+1);

                        if(pageList.length >= 10) {
                            break;
                        }
                    }
                    self.pagingData.pageList = pageList;
                });
            },
            getKeywordList: function(page) {
                var self = this;
                var suffix = '/api/getKeywordList';
                var url = getUrl(suffix);
                var data = {
                    schStartDate: self.filterStartDate,
                    schEndDate: self.filterEndDate,
                    schDepart: self.filterDepart,
                    schAgentId: self.filterAgentId,
                    schSpeaker: self.filterSpeaker,
                    schCustomerNumber: self.filterCustomerNumber,
                    page: page,
                    countPerPage: this.pagingData.countPerPage
                };

                $.get(url, data, function (result) {
                    if (result) {
                        self.data = result;
                    } else {
                        self.data = [];
                    }
                });
            },
            getKeywordDetailList: function(keyword) {
                var self = this;
                var suffix = '/api/getKeywordDetailList';
                var url = getUrl(suffix);
                var data = {
                    schStartDate: self.filterStartDate,
                    schEndDate: self.filterEndDate,
                    schDepart: self.filterDepart,
                    schAgentId: self.filterAgentId,
                    schSpeaker: self.filterSpeaker,
                    schCustomerNumber: self.filterCustomerNumber,
                    schWordCodeValue: keyword,
                };

                $.get(url, data, function (result) {
                    if (result) {
                        self.data2 = result;
                    } else {
                        self.data2 = [];
                    }
                });
            },
            downloadExcel : function() {
                var self = this;
                var a = document.createElement('a');
                var param = '/api/getKeywordExcel?schStartDate=' + self.filterStartDate + '&schEndDate=' + self.filterEndDate
                    + '&schDepart=' + self.filterDepart + '&schAgentId=' + self.filterAgentId
                    + '&schSpeaker=' + self.filterSpeaker
                    + '&schCustomerNumber=' + self.filterCustomerNumber + '&schWordCodeValue=' + self.filterWordCodeValue;
                var url = getUrl(param);

                a.href = url;
                a.click();
            },
            initChart: function() {
                 this.chart = new Chart(document.getElementById("line-chart"), {
                    type: 'line',
                    data: {
                        labels: [],//['2019-12-09', '2019-12-10', '2019-12-11', '2019-12-12'],
                        datasets: [{
                            data: [],//[125,200,150,141],
                            label: '',
                            borderColor: "#7764ca",
                            backgroundColor: "#7764ca",
                            fill: false
                        }]
                    },
                    options: {
                        maintainAspectRatio:false, // default value. false일 경우 포함된 div의 크기에 맞춰서 그려짐.
                        title: {
                            display:false,
                            text: '일별 탐지 건수'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        },
                        legend:{
                            display:true,
                            position: 'bottom'
                        }
                    }
                 });
            },
            getChartData: function(keyword) {
                this.chart.data.datasets[0].label = keyword;

                this.filterStartDate = $('#schStartDate').val();
                this.filterEndDate = $('#schEndDate').val();

                var fromDate = new Date(this.filterStartDate);
                var toDate = new Date(this.filterEndDate);

                var diff = Math.abs(toDate.getTime() - fromDate.getTime());
                diff = Math.ceil(diff/(1000*3600*24));

                var dateArr = [];
                for(var i=diff; i>=0; i--) {
                    dateArr.push(convertDateToStr(new Date(toDate.getFullYear(), toDate.getMonth(), toDate.getDate() - i)));
                }

                var self = this;
                var suffix = '/api/getGraphData';
                var url = getUrl(suffix);
                var data = {
                    schStartDate: self.filterStartDate,
                    schEndDate: self.filterEndDate,
                    schDepart: self.filterDepart,
                    schAgentId: self.filterAgentId,
                    schSpeaker: self.filterSpeaker,
                    schCustomerNumber: self.filterCustomerNumber,
                    schWordCodeValue: keyword
                };

                $.get(url, data, function (result) {
                    if (result) {
                       var countList = [];
                       dateArr.forEach(function(date, idx){
                           var count = 0;
                           result.forEach(function(item){
                               if(date === item['create_date']) {
                                    count = item['count'];
                               }
                           });
                           countList.push(count);

                           if(dateArr.length - 1 === idx) {
                               self.chart.data.labels = dateArr;
                               self.chart.data.datasets[0].data = countList;
                               self.chart.update();
                           }
                       });
                    }
                });
            },
            setDate: function () {
                var nowDate = new Date();
                var stDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 7);

                var startDate = convertDateToStr(stDate);
                var endDate = convertDateToStr(nowDate);

                $('#schStartDate').val(startDate);
                $('#schEndDate').val(endDate);
            }
        },
        mounted: function () {
            $.getScript('/resources/js/common.js');
            this.setDate();
            this.getTeamList();
            this.search();
        }
    });
</script>