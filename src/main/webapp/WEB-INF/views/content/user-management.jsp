<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="content">
    <!-- .titArea -->
    <div class="titArea">
        <h3>사용자 관리</h3>
    </div>
    <!-- //.titArea -->

    <!-- .srchArea -->
    <div class="srchArea">
        <div class="dlBox">
            <dt>소속팀</dt>
            <dd>
                <div class="selectbox">
                    <label for="schDepart">전체</label>
                    <select id="schDepart">
                        <option value="" selected>전체</option>
                    </select>
                </div>
            </dd>
            <dt>이름</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" id="schName" class="ipt_txt ipt_wide" placeholder="" v-on:keyup.13="search()">
                </div>
            </dd>
        </div>
    </div>
    <!-- //.srchArea -->
    <div class="btnBox">
        <button type="submit" class="btnS_basic" v-on:click="search()"><img src="resources/images/ico_srch_w.png" alt="조회">조회</button>
        <button type="submit" class="btnS_basic" v-on:click="addUser()"><img src="resources/images/ico_add_w.png" alt="추가">추가</button>
        <button type="button" class="btnS_basic" v-on:click="$('#deleteConfirmDialog').dialog('open')"><img src="resources/images/ico_check_delete_w.png" alt="삭제">삭제</button>
    </div>
    <!-- .content -->
    <div class="content">

        <!-- .stn -->
        <div id="demo" class="stn">
            <demo-grid :key-arr="keyArr" :data="data" :header="header"></demo-grid>
        </div>
        <!-- //.stn -->
        <!-- page_area -->
        <div class="page_area">
            <%@ include file="../common/paging.jsp" %>
        </div>
        <!-- //page_area -->
        <div id="modal"></div>
        <div id="keywordManagement"></div>
        <div id="configManagement"></div>
        <div id="changePasswordModal"></div>
    </div>
    <!-- //.content -->

    <!-- modal page -->
    <div class="lyrWrap">
        <div class="lyr_bg"></div>
        <div id="lyr_add" class="lyrBox03">
            <div class="lyr_top">
                <h3>사용자 추가</h3>
                <button type="button" class="btn_lyr_close" v-on:click="modalReset()">닫기</button>
            </div>
            <div class="lyr_mid">
                <div class="txtBox">
                    <table class="tbl_view">
                        <colgroup>
                            <col width="20%"><col width="40%"><col width="40%">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th scope="row">사번</th>
                            <td><input type="text" autocomplete="off" id="username" class="ipt_txt ipt_wide" placeholder=""></td>
                        </tr>
                        <tr>
                            <th scope="row">비밀번호</th>
                            <td><input type="password" id="password" class="ipt_txt ipt_wide" placeholder=""></td>
                        </tr>
                        <tr>
                            <th scope="row">이름</th>
                            <td><input type="text" autocomplete="off" id="name" class="ipt_txt ipt_wide" placeholder=""></td>
                        </tr>
                        <tr>
                            <th scope="row">소속팀</th>
                            <td>
                                <div class="selectbox">
                                    <label for="userDepart">전체</label>
                                    <select id="userDepart" class="">
                                        <option value="" selected>전체</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="lyr_btm">
                <ul class="btn_lst">
                    <li><button type="button" v-on:click="$('#saveConfirmDialog').dialog('open')" class="btn_clr">저장</button></li>
                    <li><button type="button" class="btn_clr btn_lyr_close" v-on:click="modalReset()">닫기</button></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- //.lyrWrap -->
    <!-- //modal page -->
</div>
<!-- component template -->
<script type="text/x-template" id="user-management-table">
    <div>
        <table class="tbl_lst">
            <colgroup>
                <col width="10%">
                <col width="20%">
                <col width="20%">
                <col width="20%">
                <col width="30%">
            </colgroup>
            <thead>
                <th><input type="checkbox" id="cbh" v-on:click="chkBoxAllCheck()"><label for="cbh"></label></th>
                <th v-for="column in header">
                    {{column}}
                </th>
            </thead>
        </table>
        <div class="tblBoxScroll" style="max-height:615px;">
            <table class="tbl_lst">
                <colgroup>
                    <col width="10%">
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                    <col width="30%">
                </colgroup>
                <tbody>
                    <tr v-if="data.length === 0">
                        <td colspan="5">데이터 없음</td>
                    </tr>
                    <tr v-else v-for="row in data">
                        <td onclick="event.cancelBubble=true"><input type="checkbox" name="chkBoxes" v-bind:id="row['id']"><label v-bind:for="row['id']"></label></td>
                        <td v-for="key in keyArr">
                            {{row[key]}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>


<script type="text/javascript">
    Vue.component('demo-grid', {
        template: '#user-management-table',
        props: {
            data: Array,
            header: Array,
            keyArr: Array
        },
        methods: {
            openModal: function(id) {
            },
            chkBoxAllCheck: function() {
                var flag = $('input:checkbox[id="cbh"]').is(":checked");
                if( flag ){
                    $('input[name=chkBoxes]').prop("checked", true);
                } else{
                    $('input[name=chkBoxes]').prop("checked", false);
                }
            }
        }
    });

    contentVue = new Vue({
        el: '#content',
        data: {
            pagingData: {
                //초기값 currentPage=1, countPerPage=15
                totalPage: 0,
                currentPage: 1,
                countPerPage: 15,
                pageList: false
            },
            header: ['사번', '이름', '소속', '등록일자'],
            keyArr: ['username', 'name', 'user_depart', 'create_date'],
            data: []
        },
        methods: {
            search: function() {
                contentVue.getUserList(1);
                contentVue.getUserCount();
            },
            goPage: function(page){
                if(page <= 0){
                    page = 1;
                } else if(page > this.pagingData.totalPage) {
                    page = this.pagingData.totalPage;
                }
                this.getUserCount();
                this.getUserList(page);
            },
            getTeamList: function() {
                var self = this;
                var suffix = '/api/getTeamList';
                var url = getUrl(suffix);
                var data = {};

                $.get(url, data, function (result) {
                    if (result) {
                        result.forEach(function(item){
                            $('#schDepart').append('<option>' + item + '</option>');
                            $('#userDepart').append('<option>' + item + '</option>');
                        });
                    }
                });
            },
            getUserCount: function() {
                var self = this;
                var suffix = '/api/getUserCount';
                var url = getUrl(suffix);
                var data = {
                    schName: $('#schName').val(),
                    schDepart: $('#schDepart').val(),
                };
                $.get(url, data, function (result) {
                    var countPerPage = self.pagingData.countPerPage;
                    var currentPage = self.pagingData.currentPage;
                    var totalPage = Math.ceil(result/countPerPage);
                    var pageList = [];
                    self.pagingData.totalPage = totalPage;

                    var startIndex = Math.floor((currentPage-1)/10)*10; //올림
                    for(var i=startIndex; i<totalPage; i++) {
                        pageList.push(i+1);
                    }
                    self.pagingData.pageList = pageList;
                });
            },
            getUserList: function(page) {
                var self = this;
                var suffix = '/api/getUserList';
                var url = getUrl(suffix);
                var data = {
                    schName: $('#schName').val(),
                    schDepart: $('#schDepart').val(),
                    page: page,
                    countPerPage: this.pagingData.countPerPage
                };

                $.get(url, data, function (result) {
                    if (result) {
                        self.data = result;
                        self.pagingData.currentPage = page;
                    } else {
                        self.data = [];
                    }
                });
            },
            deleteUser: function() {
                var self = this;
                var suffix = '/api/deleteUser';
                var url = getUrl(suffix);

                var userIdList = [];
                $('input:checkbox[name="chkBoxes"]:checked').each(function(){
                    userIdList.push($(this).attr('id'));
                }).promise().done(function() {
                    if(userIdList.length <= 0) {
                        return false;
                    }
                    $.ajax({
                        url : url,
                        traditional : true,
                        data : {
                            userIdList: userIdList
                        },
                        type : "POST",
                        beforeSend : function(xhr) {
                            /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                            xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                        },
                    }).done(function(data){
                        if(data === "SUCC") {
                            self.chkBoxReset();
                            self.getUserCount();
                            self.getUserList(1);
                            $('#deleteSuccessDialog').dialog('open');
                        } else {
                            console.log('error');
                            self.chkBoxReset();
                            $('#deleteFailDialog').dialog('open');
                        }
                    }).fail(function(){
                        console.log('error');
                        self.chkBoxReset();
                        $('#deleteFailDialog').dialog('open');
                    });
                });
            },
            addUser: function() {
                $('.lyrWrap').fadeIn(300);
                $('#lyr_add').show();
            },
            saveUser: function() {
                var self = this;
                var suffix = '/api/addUser';
                var url = getUrl(suffix);
                var data = {
                    username: $('#username').val(),
                    name: $('#name').val(),
                    password: $('#password').val(),
                    userDepart: $('#userDepart').val()
                };

                $.ajax({
                    url : url,
                    data : data,
                    type : "POST",
                    beforeSend : function(xhr) {
                        /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                        xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                    },
                }).done(function(result){
                    if(result === "DUP"){
                        $('#duplicateDialog').dialog('open');
                    } else if(result === "SUCC") {
                        self.getUserCount();
                        self.getUserList(1);
                        $('#saveSuccessDialog').dialog('open');
                    } else {
                        console.log('error');
                        $('#saveFailDialog').dialog('open');
                    }
                    self.modalReset();
                }).fail(function(data){
                    console.log('error');
                    $('#failDialog').dialog('open');
                    self.modalReset();
                });
            },
            chkBoxReset: function() {
                $('input:checkbox[name=chkBoxes]:checked').each(function(){
                    $(this).attr("checked", false);
                });
                $('#cbh').attr("checked", false);
            },
            modalReset: function() {
                $('#username').val('');
                $('#name').val('');
                $('#password').val('');
                $('#userDepart').val('');
                $('#userDepart').val('');
            },
            initDialog: function() {
                var duplicateDialogMsg = '해당 사번이 이미 존재합니다.';
                var duplicateDialogId = 'duplicateDialog';
                var duplicateDialogType = 'duplicate';
                this.setDialog(duplicateDialogId, duplicateDialogMsg, duplicateDialogType);

                var deleteConfirmDialogMsg = '삭제 하시겠습니까?';
                var deleteConfirmDialogId = 'deleteConfirmDialog';
                var deleteConfirmDialogType = 'deleteConfirm';
                this.setDialog(deleteConfirmDialogId, deleteConfirmDialogMsg, deleteConfirmDialogType);

                var saveConfirmDialogMsg = '저장 하시겠습니까?';
                var saveConfirmDialogId = 'saveConfirmDialog';
                var saveConfirmDialogType = 'saveConfirm';
                this.setDialog(saveConfirmDialogId, saveConfirmDialogMsg, saveConfirmDialogType);

                var saveSuccessDialogMsg = '저장 완료되었습니다.';
                var saveSuccessDialogId = 'saveSuccessDialog';
                var saveSuccessDialogType = 'success';
                this.setDialog(saveSuccessDialogId, saveSuccessDialogMsg, saveSuccessDialogType);

                var saveFailDialogMsg = '저장 실패되었습니다.';
                var saveFailDialogId = 'saveFailDialog';
                var saveFailDialogType = 'fail';
                this.setDialog(saveFailDialogId, saveFailDialogMsg, saveFailDialogType);

                var deleteSuccessDialogMsg = '삭제 완료되었습니다.';
                var deleteSuccessDialogId = 'deleteSuccessDialog';
                var deleteSuccessDialogType = 'success';
                this.setDialog(deleteSuccessDialogId, deleteSuccessDialogMsg, deleteSuccessDialogType);

                var deleteFailDialogMsg = '삭제 실패되었습니다.';
                var deleteFailDialogId = 'deleteFailDialog';
                var deleteFailDialogType = 'fail';
                this.setDialog(deleteFailDialogId, deleteFailDialogMsg, deleteFailDialogType);
            },
            setDialog: function(id, msg, type) {
                var buttons;
                var self = this;
                if (type === 'success' || type === 'fail') {
                    buttons = [
                        {
                            text: "OK",
                            click: function () {
                                $('.lyrWrap').fadeOut(300);
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if(type === 'saveConfirm') {
                    buttons= [
                        {
                            text: "OK",
                            click: function() {
                                self.saveUser();
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if(type === 'deleteConfirm') {
                    buttons= [
                        {
                            text: "OK",
                            click: function() {
                                self.deleteUser();
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if (type === 'duplicate') {
                    buttons = [
                        {
                            text: "OK",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ];
                }
                $('#'+id).html(msg);
                $('#'+id).dialog({
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    buttons: buttons
                });
            }
        },
        mounted: function () {
            $.getScript('/resources/js/common.js');
            $.getScript('/resources/js/bootstrap-datepicker.js');

            this.getUserCount();
            this.getUserList(1);
            this.getTeamList();

            this.initDialog();
        }
    });
</script>