<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="content">
    <!-- .titArea -->
    <div class="titArea">
        <h3>묵음 탐지 콜 조회</h3>

    </div>
    <!-- //.titArea -->
    <!-- .srchArea -->
    <div class="srchArea">
        <div class="dlBox">
            <dt>상담 일자</dt>
            <dd>
                <div class="dateBox">
                    <input type="text" autocomplete="off" id="schStartDate" class="ipt_date" placeholder="시작일" autocomplete="off" >
                    <span class="hyphen">-</span>
                    <input type="text" autocomplete="off" id="schEndDate" class="ipt_date" placeholder="종료일" autocomplete="off" >
                </div>
            </dd>
            <dt>소속팀</dt>
            <dd>
                <div class="selectbox">
                    <label for="schDepart">전체</label>
                    <select id="schDepart">
                        <option value="" selected>전체</option>
                    </select>
                </div>
            </dd>
        </div>
        <div class="dlBox">
            <dt>상담사 사번</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt ipt_wide" placeholder="" id="schAgentId" v-on:keyup.13="search()">
                </div>
            </dd>
            <dt>묵음 시간</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt sec_txt" placeholder="" id="schSilenceTime" v-on:keyup.13="search()">
                </div>
                <span class="small_txt">초 이상</span>
            </dd>
        </div>
    </div>
    <!-- //.srchArea -->

    <div class="btnBox">
        <button type="submit" class="btnS_basic" v-on:click="search()"><img src="resources/images/ico_srch_w.png" alt="조회">조회</button>
        <button type="button" class="btnS_basic" v-on:click="downloadExcel()"><img src="resources/images/ico_download_w.png" alt="다운로드">다운로드</button>
    </div>
    <!-- //.srchArea -->
    <!-- .content -->
    <!-- .content -->
    <div class="content">
        <!-- .stn -->
        <div id="demo" class="stn">
            <demo-grid :key-arr="keyArr" :data="data" :header="header" :paging-data="pagingData"></demo-grid>
        </div>
        <!-- //.stn -->
        <!-- page_area -->
        <div class="page_area">
            <%@ include file="../common/paging.jsp" %>
        </div>
        <!-- //page_area -->
        <div id="modal"></div>
        <div id="keywordManagement"></div>
        <div id="configManagement"></div>
        <div id="changePasswordModal"></div>
    </div>
    <!-- //.content -->
</div>

<!-- component template -->
<script type="text/x-template" id="search-silent-call-table">
    <div>
        <table class="tbl_lst">
            <colgroup>
                <col width="60">
                <col width="15%">
                <col width="10%">
                <col width="10%">
                <col width="20%">
                <col>
                <col>
                <col width="10%">
            </colgroup>
            <thead>
                <th v-for="column in header">
                    {{column}}
                </th>
            </thead>
        </table>
        <div class="tblBoxScroll" style="max-height:570px;">
            <table class="tbl_lst">
                <colgroup>
                    <col width="60">
                    <col width="15%">
                    <col width="10%">
                    <col width="10%">
                    <col width="20%">
                    <col>
                    <col>
                    <col width="10%">
                </colgroup>
                <tbody>
                    <tr v-if="data.length === 0">
                        <td colspan="8">데이터 없음</td>
                    </tr>
                    <tr v-else v-for="(row, idx) in data">
                        <td>{{(pagingData['currentPage'] - 1) * pagingData['countPerPage'] + idx + 1}}</td>
                        <td v-for="key in keyArr">
                            {{row[key]}}
                        </td>
                        <td v-on:click="sttTextModal(row['rec_meta_ser'])"><span class="lyr_view"><i class="material-icons">volume_up</i></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<script type="text/javascript">
    Vue.component('demo-grid', {
        template: '#search-silent-call-table',
        props: {
            data: Array,
            header: Array,
            keyArr: Array,
            pagingData: Object
        },
        methods: {
            sttTextModal: function(recMetaSer) {
                $.ajax({
                    type : "GET",
                    url : "sttText",
                    dataType : "text",
                    data: {recMetaSer: recMetaSer},
                    error : function(msg) {
                        console.log(msg);
                    },
                    success : function(data) {
                        $('#modal').html(data);
                    }
                });
            }
        }
    });

    contentVue = new Vue({
        el: '#content',
        data: {
            filterStartDate: '',
            filterEndDate: '',
            filterDepart: '',
            filterAgentId: '',
            filterSilenceTime: '',
            isNew: true,
            pagingData: {
                //초기값 currentPage=1, countPerPage=15
                totalPage: 0,
                currentPage: 1,
                countPerPage: 15,
                pageList: false
            },
            header: ['No', '소속팀', '상담사', '내선번호', '고객번호', '상담일시', '묵음시간(초)', '청취'],
            keyArr: ['agent_part1', 'agent_info', 'agent_number','customer_number', 'create_time', 'max_silence_time'],
            data: []
        },
        methods: {
            search: function() {
                this.filterStartDate = $('#schStartDate').val();
                this.filterEndDate = $('#schEndDate').val();
                this.filterDepart = $('#schDepart').val();
                this.filterAgentId = $('#schAgentId').val();
                this.filterSilenceTime = $('#schSilenceTime').val();

                this.getSilenceTimeList(1);
                this.getSilenceTimeCount();
            },
            getTeamList: function() {
                var self = this;
                var suffix = '/api/getTeamList';
                var url = getUrl(suffix);
                var data = {};

                $.get(url, data, function (result) {
                    if (result) {
                        result.forEach(function(item){
                            $('#schDepart').append('<option>' + item + '</option>');
                        });
                    }
                });
            },
            goPage: function(page){
                if(page <= 0){
                    page = 1;
                } else if(page > this.pagingData.totalPage) {
                    page = this.pagingData.totalPage;
                }
                this.pagingData.currentPage = page;
                this.getSilenceTimeCount();
                this.getSilenceTimeList(page);
            },
            getSilenceTimeCount: function() {
                var self = this;
                var suffix = '/api/getSilenceTimeCount';
                var url = getUrl(suffix);
                var data = {
                    schStartDate: self.filterStartDate,
                    schEndDate: self.filterEndDate,
                    schDepart: self.filterDepart,
                    schAgentId: self.filterAgentId,
                    schSilenceTime: self.filterSilenceTime
                };
                $.get(url, data, function (result) {
                    var countPerPage = self.pagingData.countPerPage;
                    var currentPage = self.pagingData.currentPage;
                    var totalPage = Math.ceil(result/countPerPage);
                    var pageList = [];

                    self.pagingData.totalPage = totalPage;

                    var startIndex = Math.floor((currentPage-1)/10)*10; //올림
                    for(var i=startIndex; i<totalPage; i++) {
                        pageList.push(i+1);

                        if(pageList.length >= 10) {
                            break;
                        }
                    }
                    self.pagingData.pageList = pageList;
                });
            },
            getSilenceTimeList: function(page) {
                var self = this;
                var suffix = '/api/getSilenceTimeList';
                var url = getUrl(suffix);
                var data = {
                    schStartDate: self.filterStartDate,
                    schEndDate: self.filterEndDate,
                    schDepart: self.filterDepart,
                    schAgentId: self.filterAgentId,
                    schSilenceTime: self.filterSilenceTime,
                    page: page,
                    countPerPage: this.pagingData.countPerPage
                };

                $.get(url, data, function (result) {
                    if (result) {
                        self.data = result;
                    } else {
                        self.data = [];
                    }
                });
            },
            downloadExcel : function() {
                var self = this;
                var a = document.createElement('a');
                var param = '/api/getSilenceTimeExcel?schStartDate=' + self.filterStartDate + '&schEndDate=' + self.filterEndDate
                    + '&schDepart=' + self.filterDepart + '&schAgentId=' + self.filterAgentId
                    + '&schSilenceTime=' + self.filterSilenceTime;
                var url = getUrl(param);

                a.href = url;
                a.click();
            },
            setDate: function () {
                var nowDate = new Date();
                var stDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 7);

                var startDate = convertDateToStr(stDate);
                var endDate = convertDateToStr(nowDate);

                $('#schStartDate').val(startDate);
                $('#schEndDate').val(endDate);
            }
        },
        mounted: function () {
            $.getScript('/resources/js/common.js');
            this.setDate();
            $('#schSilenceTime').val(5);
            this.getTeamList();
            this.search();
        }
    });
</script>