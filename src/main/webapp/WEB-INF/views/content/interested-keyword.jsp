<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="content">
    <!-- .titArea -->
    <div class="titArea">
        <h3>관심 키워드</h3>
    </div>
    <!-- //.titArea -->
    <!-- .srchArea -->
    <div class="srchArea">
        <div class="dlBox">
            <dt>상담 일시</dt>
            <dd class="w100">
                <div class="dateBox">
                    <input type="text" autocomplete="off" id="schStartDate" class="ipt_date" placeholder="시작일" autocomplete="off">
                    <div class="srchbox">
                        <input type="time" class="ipt_txt" placeholder="" id="schStartTime">
                    </div>
                    <span class="hyphen">-</span>
                    <input type="text" autocomplete="off" id="schEndDate" class="ipt_date" placeholder="종료일" autocomplete="off">
                    <div class="srchbox">
                        <input type="time" class="ipt_txt" placeholder="" id="schEndTime">
                    </div>
                </div>
            </dd>
        </div>
        <div class="dlBox">
            <dt>상담사 사번</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt ipt_wide" placeholder="" id="schAgentId" v-on:keyup.13="search()">
                </div>
            </dd>
            <dt>내선번호</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt ipt_wide" placeholder="" id="schAgentNumber" v-on:keyup.13="search()">
                </div>
            </dd>
        </div>
        <div class="dlBox">
            <dt>발화자</dt>
            <dd>
                <div class="selectbox">
                    <label for="schSpeaker">전체</label>
                    <select id="schSpeaker" >
                        <option value="" selected>전체</option>
                        <option value="A">상담사</option>
                        <option value="C">고객</option>
                    </select>
                </div>
            </dd>
            <dt>고객번호</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt ipt_wide" placeholder="" id="schCustomerNumber" v-on:keyup.13="search()">
                </div>
            </dd>
        </div>
        <div class="dlBox">
            <dt>키워드</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt ipt_wide" placeholder="" id="schWordCodeValue" v-on:keyup.13="search()">
                </div>
            </dd>
            <dt>소속팀</dt>
            <dd>
                <div class="selectbox">
                    <label for="schDepart">전체</label>
                    <select id="schDepart">
                        <option value="" selected>전체</option>
                    </select>
                </div>
            </dd>
        </div>
    </div>
    <!-- //.srchArea -->
    <div class="btnBox">
        <button type="submit" class="btnS_basic" v-on:click="search()"><img src="resources/images/ico_srch_w.png" alt="조회">조회</button>
        <button type="button" class="btnS_basic" v-on:click="downloadExcel()"><img src="resources/images/ico_download_w.png" alt="다운로드">다운로드</button>
    </div>
    <!-- .content -->
    <div class="content">
        <!-- .stn -->
        <div id="demo" class="stn">
            <demo-grid :key-arr="keyArr" :data="data" :header="header" :paging-data="pagingData"></demo-grid>
        </div>
        <!-- //.stn -->
        <!-- page_area -->
        <div class="page_area">
            <%@ include file="../common/paging.jsp" %>
        </div>
        <!-- //page_area -->
        <div id="modal"></div>
        <div id="keywordManagement"></div>
        <div id="configManagement"></div>
        <div id="changePasswordModal"></div>
    </div>
    <!-- //.content -->
</div>

<!-- component template -->
<script type="text/x-template" id="interested-keyword-table">
    <div>
        <table class="tbl_lst">
            <colgroup>
                <col width="60">
                <col width="7%">
                <col width="10%">
                <col width="6%">
                <col width="5%">
                <col width="10%">
                <col width="10%">
                <col>
                <col width="5%">
            </colgroup>
            <thead>
            <th v-for="column in header">
                {{column}}
            </th>
            </thead>
        </table>
        <div class="tblBoxScroll" style="max-height:470px;">
            <table class="tbl_lst">
                <colgroup>
                    <col width="60">
                    <col width="7%">
                    <col width="10%">
                    <col width="6%">
                    <col width="5%">
                    <col width="10%">
                    <col width="10%">
                    <col>
                    <col width="5%">
                </colgroup>
                <tbody>
                    <tr v-if="data.length === 0">
                        <td colspan="9">데이터 없음</td>
                    </tr>
                    <tr v-else v-for="(row, idx) in data">
                        <td>{{(pagingData['currentPage'] - 1) * pagingData['countPerPage'] + idx + 1}}</td>
                        <td v-for="key in keyArr" :class="{stt_txt: key === 'stt_sentence'}">
                            {{row[key]}}
                        </td>
                        <td v-on:click="sttTextModal(row['rec_meta_ser'])"><span class="lyr_view"><i class="material-icons">volume_up</i></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<script type="text/javascript">
    Vue.component('demo-grid', {
        template: '#interested-keyword-table',
        props: {
            data: Array,
            header: Array,
            keyArr: Array,
            pagingData: Object
        },
        methods: {
            sttTextModal: function(recMetaSer) {
                $.ajax({
                    type : "GET",
                    url : "sttText",
                    dataType : "text",
                    data: {recMetaSer: recMetaSer},
                    error : function(msg) {
                        console.log(msg);
                    },
                    success : function(data) {
                        $('#modal').html(data);
                    }
                });
            }
        }
    });

    contentVue = new Vue({
        el: '#content',
        data: {
            filterStartDate: '',
            filterEndDate: '',
            filterDepart: '',
            filterAgentId: '',
            filterAgentNumber: '',
            filterSpeaker: '',
            filterCustomerNumber: '',
            filterWordCodeValue: '',
            isNew: true,
            pagingData: {
                //초기값 currentPage=1, countPerPage=15
                totalPage: 0,
                currentPage: 1,
                countPerPage: 15,
                pageList: false
            },
            header: ['No', '소속팀', '상담사', '내선번호', '발화자', '고객번호', '상담일시', '원문', '청취'],
            keyArr: ['agent_part1', 'agent_info', 'agent_number', 'speaker2','customer_number', 'create_time', 'stt_sentence'],
            data: []
        },
        methods: {
            search: function() {
                this.filterStartDate = $('#schStartDate').val();
                this.filterEndDate = $('#schEndDate').val();
                this.filterStartTime = $('#schStartTime').val();
                this.filterEndTime = $('#schEndTime').val();
                this.filterDepart = $('#schDepart').val();
                this.filterAgentId = $('#schAgentId').val();
                this.filterAgentNumber = $('#schAgentNumber').val();
                this.filterSpeaker = $('#schSpeaker').val();
                this.filterCustomerNumber = $('#schCustomerNumber').val();
                this.filterWordCodeValue = $('#schWordCodeValue').val();

                if(!this.filterStartTime) {
                    this.filterStartTime = "00:00"
                }
                if(!this.filterEndTime) {
                    this.filterEndTime = "00:00"
                }

                this.getInterestedKeywordList(1);
                this.getInterestedKeywordCount();
            },
            getTeamList: function() {
                var self = this;
                var suffix = '/api/getTeamList';
                var url = getUrl(suffix);
                var data = {};

                $.get(url, data, function (result) {
                    if (result) {
                        result.forEach(function(item){
                            $('#schDepart').append('<option>' + item + '</option>');
                        });
                    }
                });
            },
            goPage: function(page){
                if(page <= 0){
                    page = 1;
                } else if(page > this.pagingData.totalPage) {
                    page = this.pagingData.totalPage;
                }
                this.pagingData.currentPage = page;
                this.getInterestedKeywordCount();
                this.getInterestedKeywordList(page);
            },
            getInterestedKeywordCount: function(count) {
                var countPerPage = this.pagingData.countPerPage;
                var currentPage = this.pagingData.currentPage;
                var totalPage = Math.ceil(count/countPerPage);
                var pageList = [];

                this.pagingData.totalPage = totalPage;

                var startIndex = Math.floor((currentPage-1)/10)*10; //올림
                for(var i=startIndex; i<totalPage; i++) {
                    pageList.push(i+1);

                    if(pageList.length >= 10) {
                        break;
                    }
                }
                this.pagingData.pageList = pageList;
            },
            getInterestedKeywordList: function(page) {
                var self = this;
                var suffix = '/api/getInterestedKeywordList';
                var url = getUrl(suffix);
                var data = {
                    schStartDate: self.filterStartDate,
                    schEndDate: self.filterEndDate,
                    schStartTime: self.filterStartTime,
                    schEndTime: self.filterEndTime,
                    schDepart: self.filterDepart,
                    schAgentId: self.filterAgentId,
                    schAgentNumber: self.filterAgentNumber,
                    schSpeaker: self.filterSpeaker,
                    schCustomerNumber: self.filterCustomerNumber,
                    schWordCodeValue: self.filterWordCodeValue,
                    page: page,
                    countPerPage: this.pagingData.countPerPage
                };

                $.get(url, data, function (result) {
                    if (result[0]['numFound'] > 0) {
                        console.log(result);
                        var numFound = result[0]['numFound'];
                        self.getInterestedKeywordCount(numFound);

                        result = result.splice(1);
                        result.forEach(function(item, idx){
                            if(item['stt_tlk_code'] === 'A') {
                                item['speaker2'] = '상담사'
                            } else if (item['stt_tlk_code'] === 'C') {
                                item['speaker2'] = '고객'
                            } else {
                                item['speaker2'] = ''
                            }

                            if(idx === result.length-1){
                                self.data = result;
                            }
                        });
                    } else {
                        self.data = [];
                    }
                });
            },
            downloadExcel : function() {
                var self = this;
                var a = document.createElement('a');
                var param = '/api/getInterestedKeywordExcel?schStartDate=' + self.filterStartDate + '&schEndDate=' + self.filterEndDate
                    + '&schStartTime=' + self.filterStartTime + '&schEndTime=' + self.filterEndTime
                    + '&schDepart=' + self.filterDepart + '&schWordCodeValue=' + self.filterWordCodeValue
                    + '&schAgentId=' + self.filterAgentId + '&schAgentNumber=' + self.filterAgentNumber + '&schSpeaker=' + self.filterSpeaker
                    + '&schCustomerNumber=' + self.filterCustomerNumber;
                var url = getUrl(param);

                a.href = url;
                a.click();
            },
            setDate: function () {
                var nowDate = new Date();
                var stDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 7);

                var startDate = convertDateToStr(stDate);
                var endDate = convertDateToStr(nowDate);

                $('#schStartDate').val(startDate);
                $('#schEndDate').val(endDate);
                $('#schStartTime').val('09:00');
                $('#schEndTime').val('18:00');
            }
        },
        mounted: function () {
            $.getScript('/resources/js/common.js');
            this.setDate();
            $('#schWordCodeValue').val('중소 벤처 기업');
            this.getTeamList();

            if("${searchVO.schWordCodeValue}" !== "") {
                var stDate = "${searchVO.schStartDate}";
                var eDate = "${searchVO.schEndDate}";
                var stTime = "${searchVO.schStartTime}";
                var eTime = "${searchVO.schEndTime}";
                var speaker = "${searchVO.schSpeaker}";
                var keyword = "${searchVO.schWordCodeValue}";

                $('#schStartDate').val(stDate);
                $('#schEndDate').val(eDate);
                $('#schStartTime').val(stTime);
                $('#schEndTime').val(eTime);
                $('#schSpeaker').val(speaker);
                $('#schWordCodeValue').val(keyword);

                var select_name = $('#schSpeaker').children('option:selected').text();
                $('#schSpeaker').siblings('label').text(select_name);
            }
            this.search();
        }
    });
</script>