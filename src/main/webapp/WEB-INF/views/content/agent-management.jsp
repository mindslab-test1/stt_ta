<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="content">
    <!-- .titArea -->
    <div class="titArea">
        <h3>상담사 관리</h3>
    </div>
    <!-- //.titArea -->

    <!-- .srchArea -->
    <div class="srchArea">
        <div class="dlBox">
            <dt>전화 IP</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" id="schAgentIp" class="ipt_txt ipt_wide" placeholder="" v-on:keyup.13="search()">
                </div>
            </dd>
            <dt>상담사 이름</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" class="ipt_txt ipt_wide" placeholder="" id="schAgentName" v-on:keyup.13="search()">
                </div>
            </dd>
        </div>
        <div class="dlBox">
            <dt>내선번호</dt>
            <dd>
                <div class="srchbox">
                    <input type="text" autocomplete="off" id="schAgentDn" class="ipt_txt ipt_wide" placeholder="" v-on:keyup.13="search()">
                </div>
            </dd>
            <dt>소속팀</dt>
            <dd>
                <div class="selectbox">
                    <label for="schDepart">전체</label>
                    <select id="schDepart">
                        <option value="" selected>전체</option>
                    </select>
                </div>
            </dd>
        </div>
    </div>
    <!-- //.srchArea -->
    <div class="btnBox">
        <button type="submit" class="btnS_basic" v-on:click="search()"><img src="resources/images/ico_srch_w.png" alt="조회">조회</button>
        <button type="submit" class="btnS_basic" v-on:click="openModal(true)"><img src="resources/images/ico_add_w.png" alt="추가">추가</button>
        <button type="button" class="btnS_basic" v-on:click="$('#deleteConfirmDialog').dialog('open')"><img src="resources/images/ico_check_delete_w.png" alt="삭제">삭제</button>
    </div>
    <!-- .content -->
    <div class="content">

        <!-- .stn -->
        <div id="demo" class="stn">
            <demo-grid :key-arr="keyArr" :data="data" :header="header" :paging-data="pagingData"></demo-grid>
        </div>
        <!-- //.stn -->
        <!-- page_area -->
        <div class="page_area">
            <%@ include file="../common/paging.jsp" %>
        </div>
        <!-- //page_area -->
        <div id="modal"></div>
        <div id="keywordManagement"></div>
        <div id="configManagement"></div>
        <div id="changePasswordModal"></div>
    </div>
    <!-- //.content -->

    <!-- modal page -->
    <div class="lyrWrap">
        <div class="lyr_bg"></div>
        <div id="lyr_add" class="lyrBox03">
            <div class="lyr_top">
                <h3 v-if="isNew">상담사 정보 추가</h3>
                <h3 v-if="!isNew">상담사 정보 수정</h3>
                <button type="button" class="btn_lyr_close" v-on:click="modalReset()">닫기</button>
            </div>
            <div class="lyr_mid">
                <div class="txtBox">
                    <table class="tbl_view">
                        <colgroup>
                            <col width="20%"><col width="30%"><col width="20%"><col width="30%">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th scope="row">전화 IP</th>
                            <td v-if="isNew"><input type="text" autocomplete="off" id="agentIp" class="ipt_txt ipt_wide" placeholder=""></td>
                            <td v-else>
                                <input type="text" autocomplete="off" id="agentIp" class="ipt_txt ipt_wide" placeholder="" readonly style="background:#ccd3df">
                            </td>
                            <th scope="row">내선번호</th>
                            <td v-if="isNew"><input type="text" autocomplete="off" id="agentDn" class="ipt_txt ipt_wide" placeholder=""></td>
                            <td v-else>
                                <input type="text" autocomplete="off" id="agentDn" class="ipt_txt ipt_wide" placeholder="" readonly style="background:#ccd3df">
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">사번</th>
                            <td><input type="text" autocomplete="off" id="agentId" class="ipt_txt ipt_wide" placeholder=""></td>
                            <th scope="row">이름</th>
                            <td><input type="text" autocomplete="off" id="agentName" class="ipt_txt ipt_wide" placeholder=""></td>
                        </tr>
                        <tr>
                            <th scope="row">소속</th>
                            <td>
                                <select id="agentDepart" class="">
                                    <option value="" selected>전체</option>
                                </select>
                            </td>
                            <th scope="row">재직여부</th>
                            <td>
                                <select id="agentAvailable" class="">
                                    <option selected>Y</option>
                                    <option>N</option>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="hidden" id="rowId">
                </div>
            </div>
            <div class="lyr_btm">
                <ul class="btn_lst">
                    <li><button type="button" v-on:click="$('#saveConfirmDialog').dialog('open')" class="btn_clr" >저장</button></li>
                    <li><button type="button" class="btn_clr btn_lyr_close" v-on:click="modalReset()">닫기</button></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- //.lyrWrap -->
    <!-- //modal page -->
</div>
<!-- component template -->
<script type="text/x-template" id="agent-management-table">
    <div>
        <table class="tbl_lst">
            <colgroup>
                <col width="40">
                <col width="60">
                <col width="10%">
                <col width="10%">
                <col width="20%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
            </colgroup>
            <thead>
            <th><input type="checkbox" id="cbh" v-on:click="chkBoxAllCheck()"><label for="cbh"></label></th>
            <th v-for="column in header">
                {{column}}
            </th>
            </thead>
        </table>
        <div class="tblBoxScroll" style="max-height:615px;">
            <table class="tbl_lst">
                <colgroup>
                    <col width="40">
                    <col width="60">
                    <col width="10%">
                    <col width="10%">
                    <col width="20%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                </colgroup>
                <tbody>
                    <tr v-if="data.length === 0">
                        <td colspan="10">데이터 없음</td>
                    </tr>
                    <tr v-else v-for="(row, idx) in data" v-on:click="updateAgent(row)" style="cursor: pointer;">
                        <td onclick="event.cancelBubble=true"><input type="checkbox" name="chkBoxes" v-bind:id="row['id']"><label v-bind:for="row['id']"></label></td>
                        <td>{{(pagingData['currentPage'] - 1) * pagingData['countPerPage'] + idx + 1}}</td>
                        <td v-for="key in keyArr">
                            {{row[key]}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>


<script type="text/javascript">

    Vue.component('demo-grid', {
        template: '#agent-management-table',
        props: {
            data: Array,
            header: Array,
            keyArr: Array,
            pagingData: Object
        },
        methods: {
            updateAgent: function(row) {
                contentVue.openModal(false);
                $('#rowId').val(row['id']);
                $('#agentIp').val(row['agent_ip']);
                $('#agentDn').val(row['agent_dn']);
                $('#agentName').val(row['agent_name']);
                $('#agentId').val(row['agent_id']);
                $('#agentDepart').val(row['agent_depart']);
                $('#agentAvailable').val(row['agent_available']);
            },
            chkBoxAllCheck: function() {
                var flag = $('input:checkbox[id="cbh"]').is(":checked");
                if( flag ){
                    $('input[name=chkBoxes]').prop("checked", true);
                } else{
                    $('input[name=chkBoxes]').prop("checked", false);
                }
            }
        }
    });

    contentVue = new Vue({
        el: '#content',
        data: {
            isNew: true,
            pagingData: {
                //초기값 currentPage=1, countPerPage=15
                totalPage: 0,
                currentPage: 1,
                countPerPage: 15,
                pageList: false
            },
            header: ['No', '전화 IP', '내선번호', '상담사 사번', '상담사 이름', '소속', '재직여부', '수정자 사번', '수정일시'],
            keyArr: ['agent_ip', 'agent_dn', 'agent_id', 'agent_name', 'agent_depart', 'agent_available', 'update_id', 'update_time'],
            data: []
        },
        methods: {
            search: function() {
                contentVue.getAgentList(1);
                contentVue.getAgentCount();
            },
            getTeamList: function() {
                var self = this;
                var suffix = '/api/getTeamList';
                var url = getUrl(suffix);
                var data = {};

                $.get(url, data, function (result) {
                    if (result) {
                        result.forEach(function(item){
                            $('#schDepart').append('<option>' + item + '</option>');
                            $('#agentDepart').append('<option>' + item + '</option>');
                        });
                    }
                });
            },
            goPage: function(page){
                if(page <= 0){
                    page = 1;
                } else if(page > this.pagingData.totalPage) {
                    page = this.pagingData.totalPage;
                }
                this.getAgentCount();
                this.getAgentList(page);
            },
            getAgentCount: function() {
                var self = this;
                var suffix = '/api/getAgentCount';
                var url = getUrl(suffix);
                var data = {
                    schDepart: $('#schDepart').val(),
                    schAgentIp: $('#schAgentIp').val(),
                    schAgentDn: $('#schAgentDn').val(),
                    schAgentName: $('#schAgentName').val()
                };
                $.get(url, data, function (result) {
                    var countPerPage = self.pagingData.countPerPage;
                    var currentPage = self.pagingData.currentPage;
                    var totalPage = Math.ceil(result/countPerPage);
                    var pageList = [];
                    self.pagingData.totalPage = totalPage;

                    var startIndex = Math.floor((currentPage-1)/10)*10; //올림
                    for(var i=startIndex; i<totalPage; i++) {
                        pageList.push(i+1);
                    }
                    self.pagingData.pageList = pageList;
                });
            },
            getAgentList: function(page) {
                var self = this;
                var suffix = '/api/getAgentList';
                var url = getUrl(suffix);
                var data = {
                    schDepart: $('#schDepart').val(),
                    schAgentIp: $('#schAgentIp').val(),
                    schAgentDn: $('#schAgentDn').val(),
                    schAgentName: $('#schAgentName').val(),
                    page: page,
                    countPerPage: this.pagingData.countPerPage
                };

                $.get(url, data, function (result) {
                    if (result) {
                        self.data = result;
                        self.pagingData.currentPage = page;
                    }
                });
            },
            deleteAgent: function() {
                var self = this;
                var suffix = '/api/deleteAgent';
                var url = getUrl(suffix);

                var agentIdList = [];
                $('input:checkbox[name="chkBoxes"]:checked').each(function(){
                    agentIdList.push($(this).attr('id'));
                }).promise().done(function() {
                    if (agentIdList.length <= 0) {
                        return false;
                    }
                    $.ajax({
                        url : url,
                        traditional : true,
                        data : {
                            agentIdList: agentIdList
                        },
                        type : "POST",
                        beforeSend : function(xhr) {
                            /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                            xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                        },
                    }).done(function(data){
                        if(data === 'SUCC') {
                            self.chkBoxReset();
                            self.getAgentCount();
                            self.getAgentList(1);
                            $('#deleteSuccessDialog').dialog('open');
                        } else {
                            console.log('error');
                            self.chkBoxReset();
                            $('#deleteFailDialog').dialog('open');
                        }
                    }).fail(function(){
                        console.log('error');
                        self.chkBoxReset();
                        $('#deleteFailDialog').dialog('open');
                    });
                });
            },
            openModal: function(flag) {
                this.isNew = flag;
                $('.lyrWrap').fadeIn(300);
                $('#lyr_add').show();
            },
            saveAgent: function() {
                var rowId = $('#rowId').val();
                var self = this;
                if(!rowId) {
                    var suffix = '/api/addAgent';
                } else {
                    var suffix = '/api/updateAgent';
                }
                var url = getUrl(suffix);

                var user = getUserData();
                var userId = user.userId;

                var data = {
                    id: $('#rowId').val(),
                    agentIp: $('#agentIp').val(),
                    agentName: $('#agentName').val(),
                    agentId: $('#agentId').val(),
                    agentDn: $('#agentDn').val(),
                    agentDepart: $('#agentDepart').val(),
                    agentAvailable: $('#agentAvailable').val(),
                    updateId: userId
                };

                $.ajax({
                    url : url,
                    data : data,
                    type : "POST",
                    beforeSend : function(xhr) {
                        /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                        xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                    },
                }).done(function(result){
                    if(result === "DUP"){
                        $('#duplicateDialog').dialog('open');
                    } else if(result === "SUCC") {
                        self.getAgentCount();
                        self.getAgentList(1);
                        $('#saveSuccessDialog').dialog('open');
                        self.modalReset();
                    } else {
                        console.log('error');
                        $('#saveFailDialog').dialog('open');
                    }
                }).fail(function(data){
                    console.log('error');
                    $('#saveFailDialog').dialog('open');
                });
            },
            chkBoxReset: function() {
                $('input:checkbox[name=chkBoxes]:checked').each(function(){
                    $(this).attr("checked", false);
                });
                $('#cbh').attr("checked", false);
            },
            modalReset: function() {
                $('#rowId').val('');
                $('#agentIp').val('');
                $('#agentDn').val('');
                $('#agentId').val('');
                $('#agentName').val('');
                $('#agentDepart option').removeAttr('selected');
                $('#agentAvailable option').removeAttr('selected');
                $('#agentDepart').find('option:first').attr('selected','selected');
                $('#agentAvailable').find('option:first').attr('selected','selected');
            },
            initDialog: function() {
                var duplicateDialogMsg = 'IP 또는 내선번호가 이미 존재합니다.';
                var duplicateDialogId = 'duplicateDialog';
                var duplicateDialogType = 'duplicate';
                this.setDialog(duplicateDialogId, duplicateDialogMsg, duplicateDialogType);

                var deleteConfirmDialogMsg = '삭제 하시겠습니까?';
                var deleteConfirmDialogId = 'deleteConfirmDialog';
                var deleteConfirmDialogType = 'deleteConfirm';
                this.setDialog(deleteConfirmDialogId, deleteConfirmDialogMsg, deleteConfirmDialogType);

                var saveConfirmDialogMsg = '저장 하시겠습니까?';
                var saveConfirmDialogId = 'saveConfirmDialog';
                var saveConfirmDialogType = 'saveConfirm';
                this.setDialog(saveConfirmDialogId, saveConfirmDialogMsg, saveConfirmDialogType);

                var saveSuccessDialogMsg = '저장 완료되었습니다.';
                var saveSuccessDialogId = 'saveSuccessDialog';
                var saveSuccessDialogType = 'success';
                this.setDialog(saveSuccessDialogId, saveSuccessDialogMsg, saveSuccessDialogType);

                var saveFailDialogMsg = '저장 실패되었습니다.';
                var saveFailDialogId = 'saveFailDialog';
                var saveFailDialogType = 'fail';
                this.setDialog(saveFailDialogId, saveFailDialogMsg, saveFailDialogType);

                var deleteSuccessDialogMsg = '삭제 완료되었습니다.';
                var deleteSuccessDialogId = 'deleteSuccessDialog';
                var deleteSuccessDialogType = 'success';
                this.setDialog(deleteSuccessDialogId, deleteSuccessDialogMsg, deleteSuccessDialogType);

                var deleteFailDialogMsg = '삭제 실패되었습니다.';
                var deleteFailDialogId = 'deleteFailDialog';
                var deleteFailDialogType = 'fail';
                this.setDialog(deleteFailDialogId, deleteFailDialogMsg, deleteFailDialogType);
            },
            setDialog: function(id, msg, type) {
                var buttons;
                var self = this;
                if (type === 'success' || type === 'fail') {
                    buttons = [
                        {
                            text: "OK",
                            click: function () {
                                $('.lyrWrap').fadeOut(300);
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if(type === 'saveConfirm') {
                    buttons= [
                        {
                            text: "OK",
                            click: function() {
                                self.saveAgent();
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if(type === 'deleteConfirm') {
                    buttons= [
                        {
                            text: "OK",
                            click: function() {
                                self.deleteAgent();
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if (type === 'duplicate') {
                    buttons = [
                        {
                            text: "OK",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ];
                }
                $('#'+id).html(msg);
                $('#'+id).dialog({
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    buttons: buttons
                });
            }
        },
        mounted: function () {
            $.getScript('/resources/js/common.js');
            this.getAgentCount();
            this.getAgentList(1);
            this.getTeamList();
            this.initDialog();
        }
    });
</script>