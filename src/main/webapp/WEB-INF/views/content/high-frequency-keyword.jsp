<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="content">
    <!-- .titArea -->
    <div class="titArea">
        <h3>고빈도 키워드</h3>
    </div>
    <!-- //.titArea -->


    <!-- .srchArea -->
    <div class="srchArea">
        <div class="dlBox">
            <dt>상담 일시</dt>
            <dd class="w100">
                <div class="dateBox">
                    <input type="text" autocomplete="off" id="schStartDate" class="ipt_date" placeholder="시작일" autocomplete="off">
                    <div class="srchbox">
                        <input type="time" class="ipt_txt" placeholder="" id="schStartTime">
                    </div>
                    <span class="hyphen">-</span>
                    <input type="text" autocomplete="off" id="schEndDate" class="ipt_date" placeholder="종료일" autocomplete="off">
                    <div class="srchbox">
                        <input type="time" class="ipt_txt" placeholder="" id="schEndTime">
                    </div>
                </div>
            </dd>
        </div>
        <div class="dlBox">
            <dt>발화자</dt>
            <dd class="w100">
                <div class="selectbox">
                    <label for="schSpeaker">전체</label>
                    <select id="schSpeaker" >
                        <option value="" selected>전체</option>
                        <option value="A">상담사</option>
                        <option value="C">고객</option>
                    </select>
                </div>
            </dd>
        </div>
    </div>


    <div class="btnBox">
        <button type="submit" class="btnS_basic" onclick="getSolrData()"><img src="resources/images/ico_srch_w.png" alt="조회">조회</button>
    </div>
    <!-- .content -->
    <div class="content">
        <!-- .stn -->
        <div class="stn" style="display: flex">
            <div id="wordCloud"></div>
            <div id="frequencyTable">
                <table-grid :key-arr="keyArr" :data="data" :header="header"></table-grid>
            </div>
        </div>
        <!-- //.stn -->
        <div id="keywordManagement"></div>
        <div id="configManagement"></div>
        <div id="changePasswordModal"></div>
    </div>
    <!-- //.content -->
</div>
<!-- component template -->
<script type="text/x-template" id="high-frequency-keyword-table">
    <div v-if="data.length !== 0">
        <table class="tbl_lst">
            <colgroup>
                <col width="20%">
                <col width="40%">
                <col width="40%">
            </colgroup>
            <thead>
            <th v-for="column in header">
                {{column}}
            </th>
            </thead>
        </table>
        <div class="tblBoxScroll" style="max-height: 500px;">
            <table class="tbl_lst">
                <colgroup>
                    <col width="20%">
                    <col width="40%">
                    <col width="40%">
                </colgroup>
                <tbody>
                <tr v-for="row in data" v-on:click="contentVue.searchKeyword(row)" style="cursor: pointer;">
                    <td v-for="key in keyArr">
                        {{row[key]}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<script type="text/javascript">
    var svg = null;
    var startDate;
    var endDate;
    var startTime;
    var endTime;
    var speaker;
    var tableData = [];

    Vue.component('table-grid', {
        template: '#high-frequency-keyword-table',
        props: {
            data: Array,
            header: Array,
            keyArr: Array
        },
        methods: {
        }
    });

    contentVue = new Vue({
        el: '#content',
        data: {
            header: ['No.', '키워드', 'Count'],
            keyArr: ['no', 'text', 'size'],
            data: []
        },
        methods: {
            searchKeyword: function(keyword) {
                var data = {
                    schStartDate: startDate,
                    schEndDate: endDate,
                    schStartTime: startTime,
                    schEndTime: endTime,
                    schSpeaker: speaker,
                    schWordCodeValue: keyword['text']
                }
                $('.snb ul.nav li').removeClass('active');
                $('#interestedKeyword').addClass('active');
                $.ajax({
                    type : "GET",
                    url : "interestedKeyword",
                    dataType : "text",
                    data: data,
                    error : function(msg) {
                        console.log(msg);
                    },
                    success : function(data) {
                        if(contentVue){
                            contentVue.$destroy();
                        }
                        $('.contents').html(data);
                    }
                });
            }
        },
        mounted: function(){
            init();
        }
    });

    function setWordCloud(words) {
        $("#wordCloud").empty();

        svg = d3.select("#wordCloud").append("svg")
            .attr("width", 1000)
            .attr("height", 500);

        //scale.linear: 선형적인 스케일로 표준화를 시킨다.
        //domain: 데이터의 범위, 입력 크기
        //range: 표시할 범위, 출력 크기
        //clamp: domain의 범위를 넘어간 값에 대하여 domain의 최대값으로 고정시킨다.
        var wordScale = d3.scale.linear().domain([1, 7000]).range([5, 150]).clamp(true);
        svg = d3.select("svg")
            .append("g")
            .attr("transform", "translate(" + 1000 / 2 + "," + 500 / 2 + ")");

        d3.layout.cloud().size([900, 500])
            .words(words)
            .rotate(function (d) {
                //return d.text.length > 5 ? 0 : 90;
                var row = {};
                row['no'] = d.no;
                row['text'] = d.text;
                row['size'] = d.size;
                tableData.push(row);
                return 0;
            })
            //스케일로 각 단어의 크기를 설정
            .fontSize(function (d) {
                return wordScale(d.size);
                //return d.size;
            })
            //클라우드 레이아웃을 초기화 > end이벤트 발생 > 연결된 함수 작동
            .on("end", draw)
            .start();
    }

    function draw(words) {
        contentVue.data = tableData;
        var cloud = svg.selectAll("text").data(words);
        //Entering words
        cloud.enter()
            .append("text")
            .style("font-family", "overwatch")
            .style("fill", function (d) {
                var ran = Math.floor(Math.random() * 10); // 0~9 랜덤 정수
                return (ran  === 0 ? "#fbc280" : "#405275");
            })
            .style("fill-opacity", .5)
            .attr("text-anchor", "middle")
            .attr('font-size', 1)
            .text(function (d) {
                return d.text;
            }).on("click", function(keyword){
            contentVue.searchKeyword(keyword);
        });
        cloud
            .transition()
            .duration(600)
            .style("font-size", function (d) {
                return d.size + "px";
            })
            .attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .style("fill-opacity", 1);
    }

    function init() {
        $.getScript('/resources/js/common.js');
        var nowDate = new Date();
        var stDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1);
        // var stDate = new Date(2020, 0, 1);
        startDate = convertDateToStr(stDate);
        endDate = convertDateToStr(stDate);

        $('#schStartDate').val(startDate);
        $('#schEndDate').val(endDate);
        $('#schStartTime').val('09:00');
        $('#schEndTime').val('18:00');

        $('#schSpeaker').val('C');
        var select_name = $('#schSpeaker').children('option:selected').text();
        $('#schSpeaker').siblings('label').text(select_name);

        getSolrData();
    }

    function getSolrData() {
        startDate = $('#schStartDate').val();
        endDate = $('#schEndDate').val();
        startTime = $('#schStartTime').val();
        endTime = $('#schEndTime').val();
        speaker = $('#schSpeaker').val();

        if(!startTime) {
            startTime = "00:00"
        }
        if(!endTime) {
            endTime = "00:00"
        }

        var self = this;
        var suffix = '/api/getSolrData';
        var url = getUrl(suffix);
        var data = {
            schStartDate: startDate,
            schEndDate: endDate,
            schStartTime: startTime,
            schEndTime: endTime,
            schSpeaker: speaker
        };

        $.get(url, data, function (result) {
            if (result) {
                tableData = [];
                setWordCloud(result);
            }
        });
    }
</script>