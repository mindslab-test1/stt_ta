<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="lyrWrap" id="changePassword">
    <div class="lyr_bg"></div>
    <div class="lyrBox03">
        <div class="lyr_top">
            <h3>비밀번호 변경</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
        <div class="lyr_mid">
            <div class="txtBox">
                <table class="tbl_view">
                    <colgroup>
                        <col width="20%"><col width="40%"><col width="40%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">사번</th>
                        <td><input type="text" autocomplete="off" id="username" class="ipt_txt ipt_wide" readonly></td>
                    </tr>
                    <tr>
                        <th scope="row">비밀번호</th>
                        <td><input type="password" id="password" class="ipt_txt ipt_wide" placeholder=""></td>
                    </tr>
                    <tr>
                        <th scope="row">이름</th>
                        <td><input type="text" autocomplete="off" id="name" class="ipt_txt ipt_wide" readonly></td>
                    </tr>
                    <tr>
                        <th scope="row">소속팀</th>
                        <td><input type="text" autocomplete="off" id="userDepart" class="ipt_txt ipt_wide" readonly></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="lyr_btm">
            <ul class="btn_lst">
                <li><button type="button" v-on:click="$('#saveConfirmDialog').dialog('open')" class="btn_clr btn_lyr_close" >저장</button></li>
                <li><button type="button" class="btn_clr btn_lyr_close">닫기</button></li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    var changePasswordVue;
    var userInfo;

    changePasswordVue = new Vue({
        el: '#changePassword',
        methods: {
            saveUser: function() {
                var self = this;
                var suffix = '/api/updateUser';
                var url = getUrl(suffix);
                var data = {
                    username: $('#username').val(),
                    name: $('#name').val(),
                    password: $('#password').val(),
                    userDepart: $('#userDepart').val(),
                    id: userInfo['id']
                };

                $.ajax({
                    url : url,
                    data : data,
                    type : "POST",
                    beforeSend : function(xhr) {
                        /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                        xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                    },
                }).done(function(result){
                    if(result === "SUCC") {
                        $('#saveSuccessDialog').dialog('open');
                    } else {
                        console.log('error');
                        $('#saveFailDialog').dialog('open');
                    }
                }).fail(function(data){
                    $('#failDialog').dialog('open');
                    console.log('error');
                });
            },
            setDialog: function(id, msg, type) {
                var buttons;
                var self = this;
                if (type === 'success' || type === 'fail') {
                    buttons = [
                        {
                            text: "OK",
                            click: function () {
                                $('#changePassword').fadeOut(300);
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if(type === 'saveConfirm') {
                    buttons= [
                        {
                            text: "OK",
                            click: function() {
                                self.saveUser();
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ];
                }
                $('#'+id).html(msg);
                $('#'+id).dialog({
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    buttons: buttons
                });
            },
            initDialog: function() {
                var saveConfirmDialogMsg = '저장 하시겠습니까?';
                var saveConfirmDialogId = 'saveConfirmDialog';
                var saveConfirmDialogType = 'saveConfirm';
                this.setDialog(saveConfirmDialogId, saveConfirmDialogMsg, saveConfirmDialogType);

                var saveSuccessDialogMsg = '저장 완료되었습니다.';
                var saveSuccessDialogId = 'saveSuccessDialog';
                var saveSuccessDialogType = 'success';
                this.setDialog(saveSuccessDialogId, saveSuccessDialogMsg, saveSuccessDialogType);

                var saveFailDialogMsg = '저장 실패되었습니다.';
                var saveFailDialogId = 'saveFailDialog';
                var saveFailDialogType = 'fail';
                this.setDialog(saveFailDialogId, saveFailDialogMsg, saveFailDialogType);
            }
        },
        mounted: function () {
            userInfo = getUserData();
            if(userInfo) {
                $('#username').val(userInfo['userId']);
                $('#name').val(userInfo['userName']);
                $('#userDepart').val(userInfo['userDepart']);
            }
            this.initDialog();
        }
    });
</script>

