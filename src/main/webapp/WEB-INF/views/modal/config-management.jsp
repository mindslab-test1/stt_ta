<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="configMng" id="configManagement">
    <div class="lyr_bg"></div>
    <div class="lyrBox">
        <div class="lyr_top">
            <h3>통화시간 설정</h3>
            <button class="btn_lyr_close">닫기</button>
        </div>
        <div class="lyr_btm">
            <div class="iptBox">
                <input type="text" autocomplete="off" class="ipt_txt" id="longcallConfig" style="text-align: right">
                <span class="small_txt">초</span>
            </div>
            <div class="btnBox sz_small">
                <button class="btnS_basic" v-on:click="$('#saveConfirmDialog').dialog('open')">저장</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var keywordManagementVue;

    keywordManagementVue = new Vue({
        el: '#configManagement',
        methods: {
            getLongcallConfig: function() {
                var self = this;
                var suffix = '/api/getLongcallConfig';
                var url = getUrl(suffix);
                $.get(url, {}, function (result) {
                    $('#longcallConfig').val(result);
                });
            },
            updateLongcallConfig: function(){
                var longcallConfig = $('#longcallConfig').val();

                var self = this;
                var suffix = '/api/updateLongcallConfig';
                var url = getUrl(suffix);
                var data = {
                    longcallConfig: longcallConfig
                };

                $.ajax({
                    url : url,
                    data : data,
                    type : "POST",
                    beforeSend : function(xhr) {
                        /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                        xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                    },
                }).done(function(result){
                    if(result) {
                        self.getLongcallConfig();
                        $('#longcallConfig').val('');
                        $('#saveSuccessDialog').dialog('open');
                    } else {
                        console.log('error');
                        $('#saveFailDialog').dialog('open');
                    }
                }).fail(function(data){
                    console.log('error');
                    $('#saveFailDialog').dialog('open');
                });
            },
            initDialog: function() {
                var saveConfirmDialogMsg = '저장 하시겠습니까?';
                var saveConfirmDialogId = 'saveConfirmDialog';
                var saveConfirmDialogType = 'saveConfirm';
                this.setDialog(saveConfirmDialogId, saveConfirmDialogMsg, saveConfirmDialogType);

                var saveSuccessDialogMsg = '저장 완료되었습니다.';
                var saveSuccessDialogId = 'saveSuccessDialog';
                var saveSuccessDialogType = 'success';
                this.setDialog(saveSuccessDialogId, saveSuccessDialogMsg, saveSuccessDialogType);

                var saveFailDialogMsg = '저장 실패되었습니다.';
                var saveFailDialogId = 'saveFailDialog';
                var saveFailDialogType = 'fail';
                this.setDialog(saveFailDialogId, saveFailDialogMsg, saveFailDialogType);
            },
            setDialog: function(id, msg, type) {
                var buttons;
                var self = this;
                if (type === 'success' || type === 'fail') {
                    buttons = [
                        {
                            text: "OK",
                            click: function () {
                                $('.configMng').fadeOut(300);
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if(type === 'saveConfirm') {
                    buttons= [
                        {
                            text: "OK",
                            click: function() {
                                self.updateLongcallConfig();
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ];
                }
                $('#'+id).html(msg);
                $('#'+id).dialog({
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    buttons: buttons
                });
            }
        },
        mounted: function () {
            this.getLongcallConfig();
            this.initDialog();
        }
    });

    $('.btn_lyr_close, .btn_lyr_cancel, .lyr_bg').on('click',function(){
        $('.configMng').fadeOut(300);
    });
</script>