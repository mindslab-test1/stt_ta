<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="resources/js/player.js"></script>

<script type="text/x-template" id="stt-text-template">
    <transition name="modal">
        <div class="lyrWrap">
            <div class="lyr_bg"></div>
            <div id="lyrview" class="lyrBox02">
                <div class="lyr_top">
                    <h3>상세보기</h3>
                    <button type="button" class="btn_lyr_close">닫기</button>
                </div>
                <div class="lyr_mid">
                    <div class="txtBox">
                        <table class="tbl_view">
                            <colgroup>
                                <col width="15%"><col><col width="15%"><col>
                            </colgroup>
                            <tbody v-if="sttMetaData">
                            <tr>
                                <th scope="row">상담 일자</th>
                                <td>{{sttMetaData['create_date']}}</td>
                                <th scope="row">상담 시간</th>
                                <td>{{sttMetaData['start_time']}} - {{sttMetaData['end_time']}}</td>
                            </tr>
                            <tr>
                                <th scope="row">상담사</th>
                                <td>{{sttMetaData['agent_info']}}</td>
                                <th scope="row">녹취 시간</th>
                                <td>{{sttMetaData['rec_time_str']}}</td>
                            </tr>
                            <tr>
                                <th scope="row">고객 번호</th>
                                <td>{{sttMetaData['customer_number']}}</td>
                                <th scope="row">평균 발화 속도</th>
                                <td>{{sttMetaData['avg_tlk_speed']}}</td>
                            </tr>
                            </tbody>
                            <tbody v-else>
                            <tr>
                                <th scope="row">상담 일자</th>
                                <td></td>
                                <th scope="row">상담 시간</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">상담사</th>
                                <td></td>
                                <th scope="row">녹취 시간</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">고객 번호</th>
                                <td></td>
                                <th scope="row">평균 발화 속도</th>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="fr_view">
                            <p>검색어</p>
                            <div class="ly_srchBox">
                                <input id="schKeyword" type="text" placeholder="" v-on:keyup.13="search()">
                            </div>

                        </div>
                        <div class="fl_view">
                            <p>상담콜</p>
                            <div class="btn_plr">
                                <button type="button" class="btnSpeedDown" v-on:click="changePlayingRate(false)">재생속도 감소</button>
                                <button type="button" class="btnSpeedUp" v-on:click="changePlayingRate(true)">재생속도 증가</button>
                                <span class="speedTxt" id="playingRate">1.0x</span>
                            </div>
                            <!-- .rightBox -->
                            <div class="rightBox">
                                <!-- .callBox -->
                                <div class="callBox">
                                    <!-- .call_btm -->
                                    <div class="call_player">
                                        <audio id="audio" preload="auto" controls ontimeupdate="onPlaying(this)">
                                            <source src="" type="audio/mpeg">
                                        </audio>
                                    </div>
                                    <!-- //.call_btm -->
                                    <!-- .call_mid -->
                                    <div class="call_mid">

                                    </div>
                                    <!-- //.call_mid -->
                                </div>

                                <!-- //.callBox -->
                            </div>
                            <!-- //.rightBox -->
                        </div>
                        <div class="fl_view">
                            <p>다운로드</p>
                            <!-- .rightBox -->
                            <div class="rightBox">
                                <%--<button type="submit" class="btn_srch" v-on:click="changePlayingRate(false)"><img src="resources/images/minus.gif" alt="배속재생"><span id="reduceRate">0.9x</span></button>--%>
                                <%--<button type="submit" class="btn_srch" v-on:click="changePlayingRate(true)"><img src="resources/images/plus.gif" alt="배속재생"><span id="increaseRate">1.1x</span></button>--%>

                                <button type="submit" class="btn_srch" v-on:click="downloadAudio()"><img src="resources/images/ico_download_bk.png" alt="음원다운">음원 파일</button>
                                <button type="submit" class="btn_srch" v-on:click="downloadExcel()"><img src="resources/images/ico_download_bk.png" alt="엑셀다운">STT 파일</button>
                            </div>
                            <!-- //.rightBox -->
                        </div>
                        <div class="fr_view">
                            <p>화자 구분</p>
                            <div class="ly_srchBox">
                                <div class="selectbox">
                                    <label for="schModalSpeaker">전체</label>
                                    <select id="schModalSpeaker" >
                                        <option value="" selected>전체</option>
                                        <option value="A">상담사</option>
                                        <option value="C">고객</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn_srch" v-on:click="search()"><img src="resources/images/ico_srch_bk.png" alt="조회">조회</button>
                            </div>
                        </div>

                        <div class="lyr_tb">
                            <table class="tbl_lst ly_tbl">
                                <colgroup>
                                    <col width="12%">
                                    <col width="12%">
                                    <col width="12%">
                                    <col width="12%">
                                    <col width="12%">
                                    <col>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">시작시간</th>
                                        <th scope="col">종료시간</th>
                                        <th scope="col">화자</th>
                                        <th scope="col">속도</th>
                                        <th scope="col">묵음시간</th>
                                        <th scope="col">STT 텍스트</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="tblBoxScroll" style="max-height:400px;" id="scrollElement">
                                <table class="tbl_lst ly_tbl">
                                    <colgroup>
                                        <col width="12%">
                                        <col width="12%">
                                        <col width="12%">
                                        <col width="12%">
                                        <col width="12%">
                                        <col>
                                    </colgroup>
                                    <tbody>
                                        <tr v-for="row in sttResult"
                                            :class="{highlight: row['findYn'] === 'Y'}"
                                            v-on:click="playSkip(row['stt_start_point'])" style="cursor: pointer;" name="sttRow">
                                            <td>{{row['stt_start_point']}}</td>
                                            <td>{{row['stt_end_point']}}</td>
                                            <td>{{row['speaker']}}</td>
                                            <td>{{row['stt_tlk_speed']}}</td>
                                            <td>{{row['silence_time']}}</td>
                                            <td class="stt_txt">{{row['stt_sentence']}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lyr_btm">
                    <ul class="btn_lst">
                        <li><button type="button" class="btn_clr btn_lyr_close">확인</button></li>
                    </ul>
                </div>
            </div>
        </div>
    </transition>
</script>

<div id="stt-text">
    <modal :stt-meta-data="sttMetaData" :stt-result="sttResult"></modal>
</div>

<script type="text/javascript">
    var recMetaSer = "${recMetaSer}";
    var tableVue = null;
    var preTime = 0;

    // register modal component
    Vue.component('modal', {
        template: '#stt-text-template',
        props: {
            sttMetaData: Object,
            sttResult: Array
        },
        methods: {
            playSkip: function(sec) {
                var audio = document.getElementById("audio");
                if(audio) {
                    audio.currentTime = sec;
                }
            },
            search: function() {
                tableVue.getSttResult();
            },
            downloadExcel : function() {
                tableVue.filterSpeaker = $('#schModalSpeaker').val();

                var a = document.createElement('a');
                var param = '/api/getSttExcel?recMetaSer=' + recMetaSer + '&schModalSpeaker=' + tableVue.filterSpeaker;
                var url = getUrl(param);

                a.href = url;
                a.click();
            },
            downloadAudio: function() {
                var a = document.createElement('a');
                var param = "/api/audioDownload?recMetaSer=" + recMetaSer;
                var url = getUrl(param);
                a.href = url;
                a.click();
            },
            changePlayingRate: function(type) {
                var audio = document.getElementById("audio");

                if(type) {
                    audio.playbackRate += 0.1;
                } else {
                    if(audio.playbackRate === 0.2){
                        return false;
                    }
                    audio.playbackRate -= 0.1;
                }
                $('#playingRate').html((audio.playbackRate).toFixed(1) + 'x');

            }
        }
    });

    // start app
    tableVue = new Vue({
        el: '#stt-text',
        data: {
            filterSpeaker: '',
            showModal: true,
            sttResult: [],
            sttMetaData: {}
        },
        methods: {
            findKeyword: function(result){
                var keyword = $('#schKeyword').val();
                if(keyword === "") {
                    this.sttResult = result;
                    return false;
                }
                var self = this;
                result.forEach(function(item, idx){
                   if(item['stt_sentence'].includes(keyword)) {
                       item['findYn'] = "Y";
                   }

                   if(idx === result.length - 1){
                       self.sttResult = result;
                   }
                });
            },
            getSttResult: function(){
                this.filterSpeaker = $('#schModalSpeaker').val();

                var self = this;
                var suffix = '/api/getSttResult';
                var url = getUrl(suffix);
                var data = {
                    recMetaSer: recMetaSer,
                    schModalSpeaker: self.filterSpeaker
                };
                $.get(url, data, function (result) {
                    self.findKeyword(result);
                });
            },
            gettSttMetaData: function(){
                var self = this;
                var suffix = '/api/getSttMetaData';
                var url = getUrl(suffix);
                var data = {
                    recMetaSer: recMetaSer
                };
                $.get(url, data, function (result) {
                    result['rec_time_str'] = addZero(Math.floor(result['rec_time']/60)) + ':' + addZero(result['rec_time']%60);
                    self.sttMetaData = result;
                });
            },
            setAudio: function(){
                var audio = document.getElementById("audio");
                var source = document.createElement("source");
                var param = "/api/getWav?recMetaSer=" + recMetaSer;
                var url = getUrl(param);

                source.src = url;
                source.type="audio/mpeg";
                audio.appendChild(source);
            },
            onPlayingChange: function(track) {
                var currentTime = Math.floor(track.currentTime);
                if (preTime === currentTime) {
                    return false;
                } else {
                    preTime = currentTime;
                    this.sttResult.forEach(function (item, idx) {
                        if (item['stt_start_point'] === currentTime && !$('tr[name=sttRow]').eq(idx).hasClass('playingHighlight')) {
                            var a = $('tr[name=sttRow]').eq(idx).offset().top;
                            var b = $('#scrollElement').offset().top;
                            var c = $('#scrollElement').scrollTop();
                            $('#scrollElement').animate({scrollTop: a - b * 2 + c + 300}, 200);
                            $('tr[name=sttRow]').eq(idx).addClass('playingHighlight')
                        }
                        if (item['stt_end_point'] === (currentTime - 1) && $('tr[name=sttRow]').eq(idx).hasClass('playingHighlight')) {
                            $('tr[name=sttRow]').eq(idx).removeClass('playingHighlight')
                        }

                    });
                }
            }
        },
        mounted: function () {
            $('.lyrWrap').fadeIn(300);
            $.getScript('/resources/js/common.js');

            this.getSttResult();
            this.gettSttMetaData();
            this.setAudio();
        }
    });

    $('.btn_lyr_close, .btn_lyr_cancel, .lyr_bg').on('click',function(){
        var playingAudio = document.getElementById("audio");
        if(playingAudio) {
            playingAudio.pause();
        }
        $('.lyrWrap').fadeOut(300);
    });

    function onPlaying(track) {
        tableVue.onPlayingChange(track);
    }
</script>