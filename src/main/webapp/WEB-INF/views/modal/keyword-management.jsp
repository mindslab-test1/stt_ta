<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="keywordSrch" id="keywordManagement">
    <div class="lyr_bg"></div>
    <div class="lyrBox">
        <div class="lyr_top">
            <h3>탐지 키워드 관리</h3>
            <button class="btn_lyr_close">닫기</button>
        </div>
        <div class="lyr_mid">
            <ul class="keyword_srch_lst">
                <li v-for="(row, idx) in data">
                    <div class="checkBox">
                        <input type="checkbox" name="ipt_check" v-bind:id="'checkbox' + idx" class="ipt_check" v-bind:value="row['word_code_value']">
                        <label v-bind:for="'checkbox' + idx">{{row['word_code_value']}}</label>
                        <!-- [D] <label>의 for값과 <input>의 ID 값을 동일하게 작성해야 함 -->
                    </div>
                </li>
            </ul>
        </div>
        <div class="lyr_btm">
            <div class="iptBox">
                <input type="text" autocomplete="off" class="ipt_txt" id="keyword">
            </div>
            <div class="btnBox sz_small">
                <button class="btnS_basic" v-on:click="$('#saveConfirmDialog').dialog('open')">추가</button>
                <button class="btnS_basic" v-on:click="$('#deleteConfirmDialog').dialog('open')">삭제</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var keywordManagementVue;

    keywordManagementVue = new Vue({
        el: '#keywordManagement',
        data: {
            data: []
        },
        methods: {
            getKeywordList: function() {
                var self = this;
                var suffix = '/api/getKeywordManagementList';
                var url = getUrl(suffix);
                var data = {};
                $.get(url, data, function (result) {
                    self.data = result;
                });
            },
            addKeyword: function(){
                var keyword = $('#keyword').val();
                var userId = getUserData()['userId'];

                if(keyword.trim().length <= 0) {
                    return false;
                } else {
                    keyword = keyword.trim();
                }

                var self = this;
                var suffix = '/api/addKeyword';
                var url = getUrl(suffix);
                var data = {
                    wordCodeValue: keyword,
                    upsertId: userId
                };

                $.ajax({
                    url : url,
                    data : data,
                    type : "POST",
                    beforeSend : function(xhr) {
                        /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                        xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                    },
                }).done(function(result){
                    if(result === "DUP") {
                        $('#duplicateDialog').dialog('open');
                    } else if(result === "SUCC") {
                        self.getKeywordList();
                        $('#keyword').val('');
                        $('#saveSuccessDialog').dialog('open');
                    } else {
                        console.log('error');
                        $('#saveFailDialog').dialog('open');
                    }
                }).fail(function(){
                    console.log('error');
                    $('#saveFailDialog').dialog('open');
                });
            },
            deleteKeyword: function(){
                var self = this;
                var suffix = '/api/deleteKeyword';
                var url = getUrl(suffix);

                var keywordList = [];
                $('input:checkbox[name="ipt_check"]:checked').each(function(){
                    keywordList.push($(this).attr('value'));
                }).promise().done(function() {
                    if (keywordList.length <= 0) {
                        return false;
                    }
                    $.ajax({
                        url : url,
                        traditional : true,
                        data : {
                            keywordList: keywordList
                        },
                        type : "POST",
                        beforeSend : function(xhr) {
                            /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                            xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                        },
                    }).done(function(data){
                        if(data) {
                            self.getKeywordList();
                            self.chkBoxReset();
                            $('#deleteSuccessDialog').dialog('open');
                        } else {
                            console.log('error');
                            self.chkBoxReset();
                            $('#deleteFailDialog').dialog('open');
                        }
                    }).fail(function(){
                        console.log('error');
                        self.chkBoxReset();
                        $('#deleteFailDialog').dialog('open');
                    });
                });
            },
            chkBoxReset: function() {
                $('input:checkbox[name=ipt_check]:checked').each(function(){
                    $(this).attr("checked", false);
                });
                $('#cbh').attr("checked", false);
            },
            initDialog: function() {
                var duplicateDialogMsg = '키워드가 이미 존재합니다.';
                var duplicateDialogId = 'duplicateDialog';
                var duplicateDialogType = 'duplicate';
                this.setDialog(duplicateDialogId, duplicateDialogMsg, duplicateDialogType);

                var deleteConfirmDialogMsg = '삭제 하시겠습니까?';
                var deleteConfirmDialogId = 'deleteConfirmDialog';
                var deleteConfirmDialogType = 'deleteConfirm';
                this.setDialog(deleteConfirmDialogId, deleteConfirmDialogMsg, deleteConfirmDialogType);

                var saveConfirmDialogMsg = '저장 하시겠습니까?';
                var saveConfirmDialogId = 'saveConfirmDialog';
                var saveConfirmDialogType = 'saveConfirm';
                this.setDialog(saveConfirmDialogId, saveConfirmDialogMsg, saveConfirmDialogType);

                var saveSuccessDialogMsg = '저장 완료되었습니다.';
                var saveSuccessDialogId = 'saveSuccessDialog';
                var saveSuccessDialogType = 'success';
                this.setDialog(saveSuccessDialogId, saveSuccessDialogMsg, saveSuccessDialogType);

                var saveFailDialogMsg = '저장 실패되었습니다.';
                var saveFailDialogId = 'saveFailDialog';
                var saveFailDialogType = 'fail';
                this.setDialog(saveFailDialogId, saveFailDialogMsg, saveFailDialogType);

                var deleteSuccessDialogMsg = '삭제 완료되었습니다.';
                var deleteSuccessDialogId = 'deleteSuccessDialog';
                var deleteSuccessDialogType = 'success';
                this.setDialog(deleteSuccessDialogId, deleteSuccessDialogMsg, deleteSuccessDialogType);

                var deleteFailDialogMsg = '삭제 실패되었습니다.';
                var deleteFailDialogId = 'deleteFailDialog';
                var deleteFailDialogType = 'fail';
                this.setDialog(deleteFailDialogId, deleteFailDialogMsg, deleteFailDialogType);
            },
            setDialog: function(id, msg, type) {
                var buttons;
                var self = this;
                if (type === 'success' || type === 'fail') {
                    buttons = [
                        {
                            text: "OK",
                            click: function () {
                                $('.lyrWrap').fadeOut(300);
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if(type === 'saveConfirm') {
                    buttons= [
                        {
                            text: "OK",
                            click: function() {
                                self.addKeyword();
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if(type === 'deleteConfirm') {
                    buttons= [
                        {
                            text: "OK",
                            click: function() {
                                self.deleteKeyword();
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ];
                } else if (type === 'duplicate') {
                    buttons = [
                        {
                            text: "OK",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ];
                }
                $('#'+id).html(msg);
                $('#'+id).dialog({
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    buttons: buttons
                });
            }
        },
        mounted: function () {
            this.getKeywordList();
            this.initDialog();
        }
    });

    $('.btn_lyr_close, .btn_lyr_cancel, .lyr_bg').on('click',function(){
        $('.keywordSrch').fadeOut(300);
    });
</script>