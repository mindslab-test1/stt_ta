<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/x-template" id="realtime-stt-template">
    <transition name="modal">
        <div class="lyrWrap">
            <div class="lyr_bg"></div>
            <div id="lyr_call" class="lyrBox04">
                <div class="lyr_top">
                    <h3>상담콜 실시간 모니터링 | {{sttMetaData['agent_info']}}</h3>
                    <button type="button" class="btn_lyr_close">닫기</button>
                </div>
                <div class="lyr_mid" id="scrollElement">
                    <!-- .call_mid -->
                    <div class="call_mid">
                        <!-- .talkLst -->
                        <ul class="talkLst">
                            <!-- .agent -->
                            <!-- [D] 실시간 반영 하면 li에 realTime 클래스 선언해 주세요. -->
                            <div v-for="row in sttResult">
                                <li v-if="row['stt_tlk_code'] == 'A'" class="agent">
                                    <span class="thumb"><i class="ico_thumb"></i></span>
                                    <span class="cont">
                                        <em class="txt">
                                            {{row['stt_sentence']}}
                                        </em>
                                    </span>
                                </li>
                                <li v-if="row['stt_tlk_code'] == 'C'" class="client">
                                    <span class="thumb"><i class="ico_thumb"></i></span>
                                    <span class="cont">
                                        <em class="txt">
                                            {{row['stt_sentence']}}
                                        </em>
                                    </span>
                                </li>
                            </div>
                        </ul>
                        <!-- //.talkLst -->
                    </div>
                    <!-- //.call_mid -->
                </div>
            </div>
        </div>
    </transition>
</script>

<div id="realtime-stt">
    <modal :stt-meta-data="sttMetaData" :stt-result="sttResult"></modal>
</div>

<script type="text/javascript">

    var recMetaSer = "${recMetaSer}";
    var realtimeVue = null;

    // register modal component
    Vue.component('modal', {
        template: '#realtime-stt-template',
        props: {
            sttMetaData: Object,
            sttResult: Array
        }
    });
    // start app
    realtimeVue = new Vue({
        el: '#realtime-stt',
        data: {
            showModal: true,
            sttResult: [],
            sttMetaData: {},
            ws: null
        },
        methods: {
            getSttResult: function(){
                var self = this;
                var suffix = '/api/getSttResult';
                var url = getUrl(suffix);
                var data = {
                    recMetaSer: recMetaSer
                };
                $.get(url, data, function (result) {
                    self.sttResult = result;
                    if(!self.ws) {
                        self.openWebSocket();
                    }
                });
            },
            gettSttMetaData: function(){
                var self = this;
                var suffix = '/api/getSttMetaData';
                var url = getUrl(suffix);
                var data = {
                    recMetaSer: recMetaSer
                };
                $.get(url, data, function (result) {
                    self.sttMetaData = result;
                });
            },
            openWebSocket: function() {
                var self = this;
                this.ws = new WebSocket('ws://211.253.149.199:8092/websocket');
                this.ws.onopen = function () {
                    var sendMsg = '{"TYPE":"subscribe", "CALL_ID" :' + recMetaSer + '}';
                    self.ws.send(sendMsg);
                };

                this.ws.onmessage = function (e) {
                    var rcvData = JSON.parse(e.data);
                    var row = {};
                    if(rcvData['CALL_ID'] === recMetaSer) {
                        row['stt_tlk_code'] = rcvData['TALKER'];
                        row['stt_sentence'] = rcvData['TEXT'];

                        self.sttResult.push(row);
                        setTimeout(function(){
                            self.scrollToBottom();
                        }, 100);
                    }
                };
            },
            scrollToBottom: function() {
                var scrollElement = document.getElementById("scrollElement");
                scrollElement.scrollTop = scrollElement.scrollHeight;
            }
        },
        mounted: function () {
            $('.lyrWrap').fadeIn(300);
            this.getSttResult();
            this.gettSttMetaData();
        },
        destroyed: function() {
            console.log('destroyed');
            if(this.ws) {
                this.ws.close();
            }
        }
    });

    $('.btn_lyr_close, .btn_lyr_cancel, .lyr_bg').on('click',function(){
        $('.lyrWrap').fadeOut(300);
        if(realtimeVue.ws) {
            realtimeVue.ws.close();
        }
    });
</script>