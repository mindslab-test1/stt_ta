// MINDsLab. UX/UI Team. YMJ. 20180825

$(document).ready(function() {
	
	
	// layer popup
	$('.btn_audio_play').on('click',function(){
		$('.audioBox').fadeIn(300);
		
		$('.audioBox audio').each(function(){
			var audio = document.getElementById('myAudio');
			audio.play();
		});	
	});	
	$('.btn_lyr_close, .btn_lyr_cancel, .lyr_bg, .btn_win_close').on('click',function(){
		$('.keywordSrch').fadeOut(300);
		$('.lyrWrap').fadeOut(300);		
		$('.audioBox').fadeOut();
		$('.audioBox audio').each(function(){
			var audio = document.getElementById('myAudio');
			audio.pause();
			audio.currentTime = 0;
		});
		$('#header .etcmenu .userBox dl').removeClass('active');
	});
	
	// select design 
    $('.selectbox').each(function(){
        $('.selectbox select').on('change',function(){
            var select_name = $(this).children('option:selected').text();
            $(this).siblings('label').text(select_name);
        });
        $('.selectbox select').on('click',function(){
            $(this).parent().addClass('active');
        });
        $('.selectbox select').on('blur',function(){
            $(this).parent().removeClass('active');
        });
        $('.selectbox select:disabled').each(function(){
            $(this).parent().addClass('disabled');
        });
    });    
    
	// header user
	$('#header .etcmenu .userBox dl dd > a').on('click',function(){
		$(this).parent().parent().addClass('active');
	});	
	$('.contents').on('click',function(){
		$('#header .etcmenu .userBox dl').removeClass('active');
	});
	
	// snb
	$('.snb ul.nav li a').on('click',function(){
		$('.snb ul.nav li').removeClass('active');
		
		$(this).parents().addClass('active');
	});
	$('.snb ul.sub_nav > li > a').on('click',function(){
		$('.snb ul.sub_nav li').removeClass('active');
		
		$(this).parent().addClass('active');
		$(this).parents().parents().parents().addClass('active');	
	});	
	$('.snb ul.third_nav > li > a').on('click',function(){
		$('.snb ul.third_nav > li').removeClass('active');
		
		$(this).parent().addClass('active');
		$(this).parents().parents().parents().addClass('active');	
	});
	
	// text count
	$('.txtareaBox .textArea').on('input keyup paste', function() {
		var content = $(this).val();
		$(this).height(((content.split('\n').length + 1) * 1.5) + 'px');
		$('.txt_count').html(content.length + '/100');
		
		var txtValLth = $(this).val().length;
		
		if ( txtValLth > 0) {
			$('.btn_change').removeClass('disabled');	
			$('.btn_change').removeAttr('disabled');
		} else {
			$('.btn_change').addClass('disabled');	
			$('.btn_change').attr('disabled');
			$('.resultArea').fadeOut(300);
		}
	});
	$('.txtareaBox .textArea').keyup();
});

// Change the selector if needed
var $table = $('table.scroll'),
	$bodyCells = $table.find('tbody tr:first').children(),
	colWidth;

// Adjust the width of thead cells when window resizes
$(window).resize(function() {
	// Get the tbody columns width array
	colWidth = $bodyCells.map(function() {
		return $(this).width();
	}).get();

	// Set the width of thead columns
	$table.find('thead tr').children().each(function(i, v) {
		$(v).width(colWidth[i]);
	});
}).resize(); // Trigger resize handler

// 191131 추가 - Maro Kim
// datapicker
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

var checkin = $('#schStartDate').datepicker({
	onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'abled' : '';
	}
}).on('changeDate', function(ev) {
	if (ev.date.valueOf() > checkout.date.valueOf()) {
		var newDate = new Date(ev.date)
		newDate.setDate(newDate.getDate() + 1);
		checkout.setValue(newDate);
	}
	checkin.hide();
	$('#schEndDate')[0].focus();
}).data('datepicker');

var checkout = $('#schEndDate').datepicker({
	onRender: function(date) {
		return date.valueOf() <= checkin.date.valueOf() ? 'abled' : '';
	}
}).on('changeDate', function(ev) {
	checkout.hide();
}).data('datepicker');

