create table if not exists TB_AGENT_INFO
(
    ID              int auto_increment
        primary key,
    AGENT_IP        varchar(20)             null,
    AGENT_DN        varchar(20)             null,
    AGENT_NAME      varchar(20) default ''  null,
    AGENT_ID        varchar(20) default ''  null,
    AGENT_DEPART    varchar(20)             null,
    AGENT_AVAILABLE char        default 'Y' null
)
    comment '상담원 정보 관리 Table' charset = utf8;

create table if not exists TB_AUTHORITY_INFO
(
    ID             int auto_increment
        primary key,
    USERNAME       varchar(15) charset utf8 null,
    AUTHORITY_NAME varchar(20)              null
);

create table if not exists TB_CATEGORY_INFO
(
    CATEGORY_TYPE        char         not null,
    CATEGORY_CODE        varchar(10)  not null,
    CATEGORY_NAME        varchar(100) not null,
    CATEGORY_PARENT_CODE varchar(10)  null,
    primary key (CATEGORY_TYPE, CATEGORY_CODE)
)
    charset = utf8;

create table if not exists TB_DETECT_KEYWORD_RESULT
(
    REC_META_SER    varchar(64)   not null,
    CREATE_DATE     date          null,
    CREATE_TIME     datetime      null,
    UPDATE_TIME     datetime      null,
    DETECTED_SEQ    int           not null,
    SPOKEN_TALKER   char          null,
    STT_SENTENCE    varchar(4000) null,
    WORD_CODE_VALUE varchar(50)   null,
    primary key (REC_META_SER, DETECTED_SEQ)
)
    comment 'Keyword 탐지 Call 정보 저장' charset = utf8;

create index CREATE_DATE
    on TB_DETECT_KEYWORD_RESULT (CREATE_DATE);

create table if not exists TB_KEYWORD_INFO
(
    WORD_CODE_VALUE varchar(50) not null
        primary key,
    UPSERT_ID       varchar(50) null,
    CREATE_TIME     datetime    null,
    UPDATE_TIME     datetime    null
)
    charset = utf8;

create table if not exists TB_STT_META
(
    REC_META_SER     varchar(64)             not null
        primary key,
    CREATE_DATE      date                    null,
    CREATE_TIME      datetime                null,
    UPDATE_TIME      datetime                null,
    AGENT_ID         varchar(20)             null,
    AGENT_NAME       varchar(20)             null,
    AGENT_NUMBER     varchar(10)             null,
    AGENT_PART1      varchar(20)             null,
    AGENT_PART2      varchar(20)             null,
    AGENT_PART3      varchar(20)             null,
    CUSTOMER_NUMBER  varchar(20)             null,
    CATEGORY1        varchar(64)             null,
    CATEGORY2        varchar(64)             null,
    CATEGORY3        varchar(64)             null,
    REC_DIRECTION    char                    null,
    REC_FILE_NAME    varchar(200)            null,
    REC_START_TIME   datetime                null,
    REC_END_TIME     datetime                null,
    REC_TIME         varchar(20) default '0' null,
    VOC_FLAG         char        default 'N' null,
    AVG_TLK_SPEED    varchar(8)  default '0' null,
    MAX_SILENCE_TIME varchar(8)  default '0' null,
    UPDATER_ID       varchar(20)             null
)
    comment 'Call의 Meta 정보를 저장하는 Table. Call 하나의 전반적인 데이터를 저장.' charset = utf8;

create index CREATE_DATE
    on TB_STT_META (CREATE_DATE);

create table if not exists TB_STT_RESULT
(
    REC_META_SER    varchar(64)   not null,
    CREATE_DATE     date          null,
    CREATE_TIME     datetime      null,
    UPDATE_TIME     datetime      null,
    STT_SEQ         int           not null,
    STT_START_POINT float(6, 2)   null,
    STT_END_POINT   float(6, 2)   null,
    STT_TLK_CODE    char          null,
    STT_TLK_SPEED   varchar(8)    null,
    SILENCE_TIME    varchar(8)    null,
    STT_SENTENCE    varchar(4000) null,
    primary key (REC_META_SER, STT_SEQ)
)
    comment 'Call 하나에 대한 STT Text Result 데이터를 저장. 문장별 발화속도 및 묵음 정보 저장.' charset = utf8;

create index CREATE_DATE
    on TB_STT_RESULT (CREATE_DATE);

create table if not exists TB_STT_TA_STATUS
(
    REC_META_SER    varchar(64)      not null
        primary key,
    CALL_STATE_CODE char default 'N' null,
    CREATE_TIME     datetime         null,
    UPDATE_TIME     datetime         null,
    KEYWORD_OCCUR   char default 'N' null,
    KEYWORD_TALKER  char default 'N' null,
    LONGCALL_OCCUR  char default 'N' null
)
    comment 'Call의 진행 상태와 특정 상태를 저장.' charset = utf8;

create table if not exists TB_STT_VOC_INFO
(
    REC_META_SER        varchar(64)      not null
        primary key,
    CREATE_TIME         datetime         null,
    UPDATE_TIME         datetime         null,
    CUSTOMER_NUMBER     varchar(20)      null,
    VOC_AREA            varchar(100)     null,
    BUSINESS_NAME       varchar(100)     null,
    POLICY_AREA         varchar(100)     null,
    BRANCH_NAME         varchar(100)     null,
    BUSINESS_OFFICE     varchar(100)     null,
    BUSINESS_BUREAU     varchar(100)     null,
    BUSINESS_DEPARTMENT varchar(100)     null,
    INQUIRY_DETAILS_CO  varchar(4000)    null,
    REPLY_CONTENTS1_CO  varchar(2000)    null,
    REPLY_CONTENTS2_CO  varchar(1000)    null,
    INQUIRY_DETAILS_SU  varchar(4000)    null,
    REPLY_CONTENTS1_SU  varchar(2000)    null,
    REPLY_CONTENTS2_SU  varchar(1000)    null,
    SUPERVISOR_CHECK    char default 'N' null
)
    charset = utf8;

create table if not exists TB_TEAM_INFO
(
    TEAM_CODE varchar(10)  not null,
    TEAM_NAME varchar(100) not null,
    primary key (TEAM_CODE, TEAM_NAME)
)
    charset = utf8;

create table if not exists TB_USER_INFO
(
    ID                      int auto_increment
        primary key,
    USERNAME                varchar(15) charset utf8  not null,
    PASSWORD                varchar(500) charset utf8 not null,
    NAME                    varchar(45) charset utf8  null,
    USER_DEPART             varchar(45) charset utf8  null,
    isAccountNonExpired     tinyint(1)                null,
    isAccountNonLocked      tinyint(1)                null,
    isCredentialsNonExpired tinyint(1)                null,
    isEnabled               tinyint(1)                null,
    CREATE_DATE             datetime                  null,
    constraint username_UNIQUE
        unique (USERNAME)
);

create table if not exists TB_VOC_AREA_INFO
(
    VOC_AREA_CODE varchar(10)  not null,
    VOC_AREA_NAME varchar(100) not null,
    primary key (VOC_AREA_CODE, VOC_AREA_NAME)
)
    charset = utf8;

create table if not exists TB_VOC_BUSINESS_INFO
(
    ID                  int auto_increment
        primary key,
    BUSINESS_NAME       varchar(100) null,
    BRANCH_NAME         varchar(100) null,
    BUSINESS_OFFICE     varchar(100) null,
    BUSINESS_BUREAU     varchar(100) null,
    BUSINESS_DEPARTMENT varchar(100) null,
    POLICY_AREA         varchar(100) null
)
    charset = utf8;


